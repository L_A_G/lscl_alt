
// Flow routing & accumulation
#define ROW 0
#define COLUMN 1

#define DIRECTION_START 0
#define DIRECTION_STOP 8
#define NO_DIRECTION -1
#define INDEGREES_COMPLETED -1

// LS
#define A0 22.1f
#define B0 0.09f

constant int CELL_OFFSETS[8][2] = {
	{-1,  0}, // N
	{-1,  1}, // NE
	{ 0,  1}, // E
	{ 1,  1}, // SE
	{ 1,  0}, // S
	{ 1, -1}, // SW
	{ 0, -1}, // W
	{-1, -1}  // NW
};

// Anisotropic flow algorithm e unit vectors, starts from north proceeding clockwise through the possible directions
constant float2 E[8] = {
	(float2)(0.0f, 			1.0f),			// N
	(float2)(M_SQRT1_2_F,	M_SQRT1_2_F),	// NE
	(float2)(1.0f, 			0.0f),			// E
	(float2)(M_SQRT1_2_F,	-M_SQRT1_2_F),	// SE
	(float2)(0.0f,			-1.0f), 		// S
	(float2)(-M_SQRT1_2_F, 	-M_SQRT1_2_F), 	// SW
	(float2)(-1.0f, 		0.0f),			// W
	(float2)(-M_SQRT1_2_F, 	M_SQRT1_2_F)	//NW
};

// Bitwise operations
inline unsigned char setBit(const uchar variable, const int bit) {
	// sets selected bit to 1
	return variable | 1 << bit;
}

inline unsigned char clearBit(const uchar variable, const int bit) {
	// sets selected bit to 0
	return variable & ~(1 << bit);
}

inline bool checkBit(const uchar variable, const int bit) {
	// checks what value a bit has
	return variable & (1 << bit);
}

inline float getContourLength(const bool diagonal) {
	// Contour lengths used in FD8
	return (diagonal) ? 0.354 : 0.5;
}

inline bool getIsDiagonal(const int direction) {
	// diagonal if 1, cardinal if 0
	return direction % 2;
}

inline bool onEdge(const uint row, const uint column, const uint total_rows, const uint total_columns) {
	return ((column == 0) || (column == total_columns - 1) || (row == 0) || (row == total_rows - 1));
}

inline int getOppositeDirection(const int direction) {
	// This function returns the opposite direction needed for translating directions into dependencies

	// N 0 NE 1 E 2 SE 3 S 4 SW 5 W 6 NW 7

	if ((direction < 0) || (direction > 7)) {
		return -1;
	}

	return (direction < 4) ? direction + 4 : direction - 4;
}

inline float computeSlopeRatio(const uint index, const uint total_columns, const float DEM_resolution, global const float *elevation_data) {
	// This function calculates the ratio of a slope from a 3x3 mask which is then used by slope or anisotropic flow algorithm
	// Assumes index is always surrounded by valid values
	
	// Create 3x3 sobel mask
	float upper_left	 = elevation_data[index - total_columns -  1];
	float upper_middle	 = elevation_data[index - total_columns +  0];
	float upper_right	 = elevation_data[index - total_columns +  1];

	float middle_left	 = elevation_data[index -  1];
	float middle_right	 = elevation_data[index +  1];

	float lower_left	 = elevation_data[index  + total_columns -  1];
	float lower_middle	 = elevation_data[index  + total_columns +  0];
	float lower_right	 = elevation_data[index  + total_columns +  1];
	
	// Calculate slope
	
	// Grid weights, since DEM resolution is same for horizontal and vertical axis 
	float distance = 8.0f * DEM_resolution;
	float dx = ((upper_left + middle_left + middle_left + lower_left)    - (upper_right + middle_right + middle_right + lower_right)) / distance;
	float dy = ((lower_left + lower_middle + lower_middle + lower_right) - (upper_left  + upper_middle + upper_middle + upper_right)) / distance;

	// rise = p² + q²
	// run = (p² + q²)^0.5
	// ratio = rise / run = (p² + q²)^0.5
	// return ratio
	return sqrt(dx * dx + dy * dy); 
}

kernel void computeAnisotropicNormalization(
	global const float *elevation,
	const uint total_rows,
	const uint total_columns,
	const float DEM_resolution,
	const float M,
	const float Q_0,
	global const float *vectors,
	global float *normalization) // Output
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
		
	if (onEdge(row, column, total_rows, total_columns)) {
		normalization[index] = 0;
		return;
	}
	
	float divisor = 0.0f;
	float up_elevation = elevation[index];
	// Gradient Vz = ratio
	float ratio = computeSlopeRatio(index, total_columns, DEM_resolution, elevation);
	float Q = Q_0 / ratio;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		uint down_row = row + CELL_OFFSETS[direction][ROW];
		uint down_column = column + CELL_OFFSETS[direction][COLUMN];
		uint down_index = down_row * total_columns + down_column;
		
		float down_elevation = elevation[down_index];
		if (down_elevation < up_elevation) {
			bool diagonal = getIsDiagonal(direction);
			float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
			float gradient = (up_elevation - down_elevation) * slope_length;
			
			// Vectors are assumed to have been normalized to 0 - 360 range, no data values are negative
			float vector_direction = radians(vectors[index]);
			float dot_product = 0;
			if (vector_direction >= 0.0f) {
				float2 u = (float2)(cos(vector_direction), sin(vector_direction));
				float2 e = E[direction];
		
				dot_product = fabs(dot(e, u));
			} else { // No vector so dot_product is ignored
				dot_product = 1.0f;
			}
			
			divisor += pow(gradient * pow(dot_product, Q), M);
		}		
	}
	
	// prevent divide by 0 which should only happen in pits
	if (divisor == 0.0f) {
		divisor = 1.0f;
	} 

	normalization[index] = divisor;
}

kernel void computeDependencies(
	global const uchar *flow_directions,
	const uint total_rows,
	const uint total_columns,
	global uchar *flow_dependencies) // Output
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
	uchar dependencies = 0;
	
	if (!onEdge(row, column, total_rows, total_columns)) {
		for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
			uint up_row = row + CELL_OFFSETS[direction][ROW];
			uint up_column = column + CELL_OFFSETS[direction][COLUMN];
			
			if (onEdge(up_row, up_column, total_rows, total_columns)) {
				continue;
			}
			
			uint up_index = up_row * total_columns + up_column;
			
			uchar upcell_flow_direction = flow_directions[up_index];
			int opposite_direction = getOppositeDirection(direction);
			
			if (checkBit(upcell_flow_direction, opposite_direction)) {
				dependencies = setBit(dependencies, direction);
			}
		}
	}
	
	flow_dependencies[index] = dependencies;
}

kernel void computeFD8Normalization(
	global const float *elevation_data,
	const uint total_rows,
	const uint total_columns,
	global float *normalization) // Output
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
		
	if (onEdge(row, column, total_rows, total_columns)) {
		normalization[index] = 0.0f;
		return;
	}
	
	float divisor = 0.0f;	
	float up_elevation = elevation_data[index];
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		uint down_row = row + CELL_OFFSETS[direction][ROW];
		uint down_column = column + CELL_OFFSETS[direction][COLUMN];
		uint down_index = down_row * total_columns + down_column;
		
		float down_elevation = elevation_data[down_index];
		if (down_elevation < up_elevation) {
			bool diagonal = getIsDiagonal(direction);
			float contour_length = getContourLength(diagonal);
			float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
			float gradient = (up_elevation - down_elevation) * slope_length;
			divisor += gradient * contour_length;
		}		
	}
	
	// prevent divide by 0 which should only happen in pits
	if (divisor == 0.0f) {
		divisor = 1.0f;
	} 

	normalization[index] = divisor;
}

kernel void computeFlowDirectionsD8(
	global const float *elevation_data,
	const uint total_rows,
	const uint total_columns,
	global uchar *flow_directions) // Output
{	
	// Get cell position in elevation_data
	uint column = get_global_id(0);
	uint row = get_global_id(1);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}

	uint index = row * total_columns + column;
	uchar directions = 0;
	
	if (!onEdge(row, column, total_rows, total_columns)) { 
		// Compute direction
		float max = 0.0f;
		int direction_of_max = NO_DIRECTION;
		float up_elevation = elevation_data[index];
		
		for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
			uint down_row = row + CELL_OFFSETS[direction][ROW];
			uint down_column = column + CELL_OFFSETS[direction][COLUMN];
			uint down_index = down_row * total_columns + down_column;
			
			float down_elevation = elevation_data[down_index];
			bool diagonal = getIsDiagonal(direction);
			float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
			float gradient = (up_elevation - down_elevation) * slope_length;
			
			if (gradient > max) {
				max = gradient;
				direction_of_max = direction;
			}
		}
		
		uint down_row = row + CELL_OFFSETS[direction_of_max][ROW];
		uint down_column = column + CELL_OFFSETS[direction_of_max][COLUMN];
		if (direction_of_max != NO_DIRECTION && !onEdge(down_row, down_column, total_rows, total_columns)) {
			directions = setBit(0, direction_of_max);
		} 
	}
	
	flow_directions[index] = directions;
}

kernel void computeFlowDirectionsFD8(
	global const float *elevation_data,
	const uint total_rows,
	const uint total_columns,
	global uchar *flow_directions) // Output
{
	
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
	uchar directions = 0;
	
	if (!onEdge(row, column, total_rows, total_columns)) {
		
		// Determine down_flow
		float up_elevation = elevation_data[index];
		for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
			uint down_row = row + CELL_OFFSETS[direction][ROW];
			uint down_column = column + CELL_OFFSETS[direction][COLUMN];
			
			if (onEdge(down_row, down_column, total_rows, total_columns)) {
				continue;
			}
			
			uint down_index = down_row * total_columns + down_column;
			float down_elevation = elevation_data[down_index];
			
			if (down_elevation < up_elevation) {
				directions = setBit(directions, direction);
			}
		}
	}
	
	flow_directions[index] = directions;
}

kernel void computeIndegree(
	global const uchar *dependencies,
	const uint total_cells,
	global int *indegrees) // Output
{
	uint index = get_global_id(0);
	
	if (index >= total_cells) {
		return;
	}
	
	int indegree = 0;
	uchar depend = dependencies[index];
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(depend, direction)) {
			indegree++;
		}
	}
	
	indegrees[index] = indegree;	
}

kernel void fillFloatBuffer(
	global float *buffer, // Output
	const uint elements,
	const float value)
{
	uint index = get_global_id(0);
	
	if (index < elements) {
		buffer[index] = value;
	}
}

kernel void fillFloatBufferWithEdge(
	global float *buffer, // Output
	const float value,
	const float edge_value,
	const uint total_rows,
	const uint total_columns)
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
	if (onEdge(row, column, total_rows, total_columns)) {
		buffer[index] = edge_value;
	} else {
		buffer[index] = value;
	}
}

kernel void fillUnsignedCharBuffer(
	global uchar *buffer, // Output
	const uint elements,
	const uchar value)
{
	uint index = get_global_id(0);
	
	if (index < elements) {
		buffer[index] = value;
	}
}

kernel void fillUnsignedCharBufferWithEdge(
	global uchar *buffer, // Output
	const uchar value,
	const uchar edge_value,
	const uint total_rows,
	const uint total_columns)
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
	if (onEdge(row, column, total_rows, total_columns)) {
		buffer[index] = edge_value;
	} else {
		buffer[index] = value;
	}
}

kernel void flowTransferAnisotropic(
	global const uchar *old_dependencies, 
	global const float *elevation,
	global const float *normalization,
	const uint total_rows,
	const uint total_columns,
	const float DEM_resolution,
	const float M,
	const float Q_0,
	global const float *vectors,
	global float *accumulation, // Desired Output
	global uchar *new_dependencies, // Necessary Output
	global bool *rerun)	// Necessary Output
{	
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
		
	uint index = row * total_columns + column;
	uchar old_depend = old_dependencies[index];
	float down_elevation = elevation[index];
	float flow_to_cell = 0.0f;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(old_depend, direction)) {
			uint up_row = row + CELL_OFFSETS[direction][ROW];
			uint up_column = column + CELL_OFFSETS[direction][COLUMN];
			uint up_index = up_row * total_columns + up_column;
			
			uchar up_depend = old_dependencies[up_index];
			
			if (up_depend == 0) {
				bool diagonal = getIsDiagonal(direction);
				float up_elevation = elevation[up_index];
				float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
				float gradient = (up_elevation - down_elevation) * slope_length;
			
				// Gradient Vz = ratio
				float ratio = computeSlopeRatio(up_index, total_columns, DEM_resolution, elevation);
				float Q = Q_0 / ratio;
			
				// Since the kernel is reading from upslope cells, those cells' vectors need to be used
				float vector_direction = radians(vectors[up_index]);
				
				// Vectors are assumed to have been normalized to 0 - 360 range, no data values are negative
				float dot_product = 0;
				if (vector_direction >= 0.0f) {
					float2 u = (float2)(cos(vector_direction), sin(vector_direction));
					float2 e = E[getOppositeDirection(direction)];
		
					dot_product = fabs(dot(e, u));
				} else { // No vector so dot_product is ignored
					dot_product = 1.0f;
				}	
				
				float flow_proportion = pow(gradient * pow(dot_product, Q), M) / normalization[up_index];
				flow_to_cell += accumulation[up_index] * flow_proportion;
				
				old_depend = clearBit(old_depend, direction);
				*rerun = true;
			}
		}
	}
	
	accumulation[index] += flow_to_cell;
	new_dependencies[index] = old_depend;
}

kernel void flowTransferReadD8(
	global const uchar *old_dependencies,
	const uint total_rows,
	const uint total_columns,
	global float *accumulation_data, // Desired Output
	global uchar *new_dependencies, // Necessary Output
	global bool *rerun) // Necessary Output
{
	// Edges are ignored because they should not have dependencies set by dependency kernel
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
	uchar old_depend = old_dependencies[index];	
	float flow_to_cell = 0.0f;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(old_depend, direction)) {
			uint up_row = row + CELL_OFFSETS[direction][ROW];
			uint up_column = column + CELL_OFFSETS[direction][COLUMN];
			
			uint up_index = up_row * total_columns + up_column;	
			uchar up_depend = old_dependencies[up_index];
			
			if (up_depend == 0) {
				flow_to_cell += accumulation_data[up_index];
				old_depend = clearBit(old_depend, direction);
				*rerun = true;
			}
		}
	}
	
	accumulation_data[index] += flow_to_cell;
	new_dependencies[index] = old_depend;
}



kernel void flowTransferReadFD8(
	global const uchar *old_dependencies,
	global const float *elevation_data,
	const uint total_rows,
	const uint total_columns,
	global const float *normalization, 
	global float *accumulation_data, // Desired Output
	global uchar *new_dependencies, // Necessary Output
	global bool *rerun) // Necessary Output
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
	uchar old_depend = old_dependencies[index];
	float down_elevation = elevation_data[index];
	float flow_to_cell = 0.0f;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(old_depend, direction)) {
			uint up_row = row + CELL_OFFSETS[direction][ROW];
			uint up_column = column + CELL_OFFSETS[direction][COLUMN];
			uint up_index = up_row * total_columns + up_column;
			
			uchar up_depend = old_dependencies[up_index];

			if (up_depend == 0) {
				bool diagonal = getIsDiagonal(direction);
				
				float up_elevation = elevation_data[up_index];
				float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
				float gradient = (up_elevation - down_elevation) * slope_length;
				float contour_length = getContourLength(diagonal);
				flow_to_cell += (accumulation_data[up_index] / normalization[up_index]) * gradient * contour_length;
				
				old_depend = clearBit(old_depend, direction);
				*rerun = true;
			}
		}
	}
	
	accumulation_data[index] += flow_to_cell;
	new_dependencies[index] = old_depend;
}

kernel void flowTransferReadFDD8(
	global const uchar *old_dependencies,
	global const float *elevation_data,
	const uint total_rows,
	const uint total_columns,
	const float threshold,
	global uchar *D8_dependencies, // Necessary Output
	global float *accumulation_data, 	// Desired Output
	global const float *normalization,
	global uchar *new_dependencies, // Necessary Output
	global bool *rerun) { // Necessary Output
	
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
	uchar old_depend = old_dependencies[index];
	float down_elevation = elevation_data[index];
	float flow_to_cell = 0.0f;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(old_depend, direction)) {
			uint up_row = row + CELL_OFFSETS[direction][ROW];
			uint up_column = column + CELL_OFFSETS[direction][COLUMN];
			uint up_index = up_row * total_columns + up_column;
			
			uchar up_depend = old_dependencies[up_index];
			
			// If that cell has no remaining dependencies (been calculated)
			if (up_depend == 0) {
				float up_accumulation = accumulation_data[up_index];
				
				// If the up cell's water level is above the D8 threshold check if this cell is the target of that flow
				if (up_accumulation > threshold) {
				
					// Gather flow according to D8		
					uchar d8_depend = D8_dependencies[index];
					
					if (checkBit(d8_depend, direction) != 0) {
						// If the cell is, transfer the flow levels over to this cell according to the D8 rules,
						flow_to_cell += up_accumulation;
						
						// and remove the D8 dependency
						d8_depend = clearBit(d8_depend, direction);
						D8_dependencies[index] = d8_depend;
					}
				} else { // If the water threshold levels were not enough
					
					// Gather flow according to FD8
					
					// Transfer the flow levels to this cell according to the FD8 rules and remove the dependency
					bool diagonal = getIsDiagonal(direction);
				
					float up_elevation = elevation_data[up_index];
					float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
					float gradient = (up_elevation - down_elevation) * slope_length;
					float contour_length = getContourLength(diagonal);
					flow_to_cell += (up_accumulation / normalization[up_index]) * gradient * contour_length;		
				}
				
				// Remove the FD8 dependencies of flow from the up cell in the current cell
				old_depend = clearBit(old_depend, direction);
				*rerun = true;
			
			} // up_dependencies
		} // checkBit
	} // for each direction
	
	accumulation_data[index] += flow_to_cell;
	new_dependencies[index] = old_depend;
}

kernel void flowTransferReadOrtegaRueda(
	global const float *old_flow,
	const uint total_rows,
	const uint total_columns,
	global const uchar *dependencies,
	global float *flow_accum, // Desired Output
	global float *new_flow, // Necessary Output
	global bool *rerun) // Necessary Output
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	uint index = row * total_columns + column;
	float flow = old_flow[index];
	
	if (flow <= 0.0f || onEdge(row, column, total_rows, total_columns)) {
		new_flow[index] = 0.0f;
		return;
	}
	
	float accum = 0.0f;
	uchar depend = dependencies[index];
	if (depend & 1) {
		accum += old_flow[(row - 1) * total_columns + column];
	}
	if (depend & 2) {
		accum += old_flow[(row - 1) * total_columns + (column + 1)];
	}
	if (depend & 4) {
		accum += old_flow[row * total_columns + (column + 1)];
	}
	if (depend & 8) {
		accum += old_flow[(row + 1) * total_columns + (column + 1)];
	}
	if (depend & 16) {
		accum += old_flow[(row + 1) * total_columns + column];
	}
	if (depend & 32) {
		accum += old_flow[(row + 1) * total_columns + (column - 1)];
	}
	if (depend & 64) {
		accum += old_flow[row * total_columns + (column - 1)];
	}
	if (depend & 128) {
		accum += old_flow[(row - 1) * total_columns + (column - 1)];
	}
	
	if (accum > 0) {
		flow_accum[index] += accum;
		*rerun = true;
	}
	
	new_flow[index] = accum;
}

kernel void flowTransferZhanQin(
	global const float *elevation_data,
	global const uchar *directions,
	global const uchar *dependencies,
	global const float *normalization,
	global int *indegrees, // Necessary Output
	const uint total_rows,
	const uint total_columns,
	global float *accumulation_data, // Desired Output
	global bool *rerun) // Necessary Output
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	if (row < total_rows && column < total_columns) {
		uint index = row * total_columns + column;
	
		int indegree = indegrees[index];
		if (indegree == 0) {
			indegrees[index] = INDEGREES_COMPLETED;
			float flow_to_cell = 0.0f;
			
			uchar depend = dependencies[index];
			
			float down_elevation = elevation_data[index];
			for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
				if (checkBit(depend, direction)) {
					uint up_row = row + CELL_OFFSETS[direction][ROW];
					uint up_column = column + CELL_OFFSETS[direction][COLUMN];
					uint up_index = up_row * total_columns + up_column;
				
					bool diagonal = getIsDiagonal(direction);
						
					float up_elevation = elevation_data[up_index];
					float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
					float gradient = (up_elevation - down_elevation) * slope_length;
					float contour_length = getContourLength(diagonal);
					flow_to_cell += (accumulation_data[up_index] / normalization[up_index]) * gradient * contour_length;
				}
			}
			
			accumulation_data[index] += flow_to_cell;
			uchar direct = directions[index];
			for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
				if (checkBit(direct, direction)) {
					uint down_row = row + CELL_OFFSETS[direction][ROW];
					uint down_column = column + CELL_OFFSETS[direction][COLUMN];
					uint down_index = down_row * total_columns + down_column;
				
					atomic_dec(&indegrees[down_index]);
				}
			}
			
			*rerun = true;
		}
	}
}

kernel void LS(
	global const float *accumulation,
	global const float *slopes,
	const uint total_rows,
	const uint total_columns,
	const float grid_resolution,
	const float m,
	const float n,
	global float *LS)	// Output
{
	// This kernel does not do anything with edges, use the fill float edge kernel to initialise LS buffer with edges
	// Edges cannot be computed due to missing values
	 
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	// Disregard elements outside boundries
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}

	// Disregard edges, set by fill float edge
	if (onEdge(row, column, total_rows, total_columns)) {
		return;
	}

	uint index = row * total_columns + column;
	
	float Ar = accumulation[index] * grid_resolution;
	float Br = slopes[index]; // No degrees to radians conversion needed as slopes is kept in radians
	
	// LS according to Mitasova
	LS[index] = (m + 1.0f) * pow(Ar / A0, m) * pow(sin(Br) / B0, n);
}

kernel void RUSLE(
	global const float *R,
	global const float *K,
	global const float *LS,
	global const float *C,
	const uint total_cells,
	const float no_data_value,
	global float *results) // Output
{
	uint index = get_global_id(0);
	if (index >= total_cells) {
		return;
	}
	
	// Direct comparison with float value works because the LS cell with the no_data_value should have got that exact value and nothing else
	if (R[index] == no_data_value || K[index] == no_data_value || LS[index] == no_data_value || C[index] == no_data_value) {
		results[index] = no_data_value;	
	} else {
		results[index] = R[index] * K[index] * LS[index] * C[index];
	}
}

kernel void slope(
	global const float *elevation,
	const uint total_rows,
	const uint total_columns,
	const float DEM_resolution,
	global float *slopes)	// Output
{
	uint row = get_global_id(1);
	uint column = get_global_id(0);
	
	if ((row >= total_rows) || (column >= total_columns)) {
		return;
	}
	
	if (onEdge(row, column, total_rows, total_columns)) {
		return;
	}

	uint index = row * total_columns + column;
	float ratio = computeSlopeRatio(index, total_columns, DEM_resolution, elevation); 
	float slope = atan(ratio);
	slopes[index] = slope;
}

kernel void testDimensionsKernel(
	global uint *local_size_rows,
	global uint *local_size_columns,
	global uint *num_groups_rows,
	global uint *num_groups_columns) // All are outputs
{
	uint x = get_global_id(1);
	uint y = get_global_id(0);
	
	if (x == 0 && y == 0) {
		*local_size_rows = get_local_size(1);
		*local_size_columns = get_local_size(0);
		*num_groups_rows = get_num_groups(1);
		*num_groups_columns = get_num_groups(0);
	}
}

kernel void topologicalAnisotropic(
	global const uint *sorted_cells,
	global const uchar *dependencies,
	global const float *elevation,
	global const float *normalization,
	const uint sort_start,
	const uint sort_end,
	const uint columns_per_row,
	const float DEM_resolution,
	const float M,
	const float Q_0,
	global const float *vectors,
	global float *accumulation)	// Output
{
	uint sort_index = get_global_id(0) + sort_start;
	if (sort_index >= sort_end) {
		return;
	}

	uint real_index = sorted_cells[sort_index];
	uchar depend = dependencies[real_index];
	float down_elevation = elevation[real_index];
	float flow_to_cell = 0.0f;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(depend, direction)) {
			uint up_row = (real_index / columns_per_row) + CELL_OFFSETS[direction][ROW];
			uint up_column = (real_index % columns_per_row) + CELL_OFFSETS[direction][COLUMN];
			uint up_index = up_row * columns_per_row + up_column;
		
			bool diagonal = getIsDiagonal(direction);
			float up_elevation = elevation[up_index];
			float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
			float gradient = (up_elevation - down_elevation) * slope_length;
			
			// Gradient Vz = ratio
			float ratio = computeSlopeRatio(up_index, columns_per_row, DEM_resolution, elevation);
			float Q = Q_0 / ratio;
			
			// Since the kernel is reading from upslope cells, those cells' vectors need to be used
			float vector_direction = radians(vectors[up_index]);
			
			// Vectors are assumed to have been normalized to 0 - 360 range, no data values are negative
			float dot_product = 0;
			if (vector_direction >= 0.0f) {
				float2 u = (float2)(cos(vector_direction), sin(vector_direction));
				float2 e = E[getOppositeDirection(direction)];
		
				dot_product = fabs(dot(e, u));
			} else { // No vector so dot_product is ignored
				dot_product = 1.0f;
			}
			
			float flow_proportion = pow(gradient * pow(dot_product, Q), M) / normalization[up_index];
			flow_to_cell += accumulation[up_index] * flow_proportion;
		}
	}
	
	accumulation[real_index] += flow_to_cell;	
}

kernel void topologicalD8(
	global const uint *sorted_cells,
	global const uchar *dependencies,
	const uint sort_start,
	const uint sort_end,
	const uint columns_per_row,
	global float *accumulation) // Output
{
	
	uint sort_index = get_global_id(0) + sort_start;
	if (sort_index >= sort_end) {
		return;
	}
	
	uint real_index = sorted_cells[sort_index];
	uchar depend = dependencies[real_index];
	float flow_to_cell = 0.0f;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(depend, direction)) {
			uint up_row = (real_index / columns_per_row) + CELL_OFFSETS[direction][ROW];
			uint up_column = (real_index % columns_per_row) + CELL_OFFSETS[direction][COLUMN];
			uint up_index = up_row * columns_per_row + up_column;
			
			flow_to_cell += accumulation[up_index];	
		}
	}
	
	accumulation[real_index] += flow_to_cell;
}

kernel void topologicalFD8(
	global const uint *sorted_cells,
	global const uchar *dependencies,
	global const float *elevation, 
	global const float *normalization,
	const uint sort_start,
	const uint sort_end,
	const uint columns_per_row,
	global float *accumulation) // Output
{ 
	uint sort_index = get_global_id(0) + sort_start;
	if (sort_index >= sort_end) {
		return;
	}
	
	uint real_index = sorted_cells[sort_index];
	uchar depend = dependencies[real_index];
	float down_elevation = elevation[real_index];
	float flow_to_cell = 0.0f;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(depend, direction)) {
			uint up_row = (real_index / columns_per_row) + CELL_OFFSETS[direction][ROW];
			uint up_column = (real_index % columns_per_row) + CELL_OFFSETS[direction][COLUMN];
			uint up_index = up_row * columns_per_row + up_column;
		
			bool diagonal = getIsDiagonal(direction);
			float up_elevation = elevation[up_index];
			float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
			float gradient = (up_elevation - down_elevation) * slope_length;
			float contour_length = getContourLength(diagonal);
			flow_to_cell += (accumulation[up_index] / normalization[up_index]) * gradient * contour_length;
		}
	}
	
	accumulation[real_index] += flow_to_cell;
}

kernel void topologicalFDD8(
	global const uint *sorted_cells,
	global const uchar *d8_dependencies,
	global const uchar *fd8_dependencies,
	global const float *elevation, 
	global const float *normalization,
	const uint sort_start,
	const uint sort_end,
	const uint columns_per_row,
	const float threshold,
	global float *accumulation) // Output
{
	uint sort_index = get_global_id(0) + sort_start;
	if (sort_index >= sort_end) {
		return;
	}
	
	uint real_index = sorted_cells[sort_index];
	uchar depend = fd8_dependencies[real_index];
	float down_elevation = elevation[real_index];
	float flow_to_cell = 0.0f;
	
	for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
		if (checkBit(depend, direction)) {
			uint up_row = (real_index / columns_per_row) + CELL_OFFSETS[direction][ROW];
			uint up_column = (real_index % columns_per_row) + CELL_OFFSETS[direction][COLUMN];
			uint up_index = up_row * columns_per_row + up_column;
		
			float up_accumulation = accumulation[up_index];
			if (up_accumulation > threshold) {
				uchar d8_depend = d8_dependencies[real_index];
				
				if (checkBit(d8_depend, direction)) {
					flow_to_cell += up_accumulation;
				}				
			} else {
				bool diagonal = getIsDiagonal(direction);
				float up_elevation = elevation[up_index];
				float slope_length = (diagonal) ? M_SQRT1_2_F : 1.0f;
				float gradient = (up_elevation - down_elevation) * slope_length;
				float contour_length = getContourLength(diagonal);
				flow_to_cell += (up_accumulation / normalization[up_index]) * gradient * contour_length;
			}
		}
	}
	
	accumulation[real_index] += flow_to_cell;
}
