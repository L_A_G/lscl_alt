Readme:
-------------------------------------------------------------------------------


Contents
-------------------------------------------------------------------------------
Description
Selecting a DEM
File Formats
RUSLE
Flow Accumulation
Choosing a Flow Routing Algorithm
LS-Factor
Slope
OpenCL
Memory Requirements
Debug Commands
Configuration File
Command Override Order


Description
-------------------------------------------------------------------------------
LSCL is a program that uses OpenCL to compute the LS-factor and RUSLE for a DEM.|
The program is capable of computing LS, RUSLE, Slope and Flow accumulation
using any device that supports the OpenCL 1.1 full profile. The program was 
originally designed to compute the LS-factor and later expanded to include the 
RUSLE computation. Computation of R, K, C and P factors are currently not 
supported and must be placed in data files and given to the program as input 
from the command line. The LS factor can also be given as input instead of 
being computed. The program is controlled by giving it commands separated by 
space. Commands are always in lower-case. If a command requires an additional
value (for example the input command) the = is used (input=DEM.asc). There is 
no limit to the amount of commands that can be given. The commands will be 
interpreted for every algorithm that are able to use them. That means that if
multiple flow algorithms and ls output have been specified, an LS-factor raster
for each flow routing algorithm will be created. The same applies to the other 
commands. If the same command is given more than once the last command is 
used. If the program reads an unknown command the program will stop and display
the invalid command.

Commands with a * needs additional parameters.
Commands with a ! overrides other parameters.
Commands with a @ means that the command parameter will be saved in a file.


Selecting a DEM
-------------------------------------------------------------------------------
Only one DEM can be processed per program execution. The input command tells
the program which DEM to use.

Related Commands:

input*		This command tells the program to use the specified DEM as input.			

Usage:
LSCL input=DEM.asc slope


File Formats
-------------------------------------------------------------------------------
The program has been developed to handle ESRIGrid files (ArcAscii) as both text
and binary files. The program has the capability to convert a file from one 
type to the other. The program automatically detects if an input file is an
ArcAscii or a binary file. The default output type is binary. All ArcAscii
files must have the extension .asc and all binary files must have the extension
.bin.

Related Commands:

ascii		This command tells the program to output all grids in text
			ArcAscii format instead of the default binary format.
			
convert!	This command tells the program to convert the file given as input
			to the opposite format of the input file.
				
Usage:
LSCL input=DEM.asc accumulation d8 ascii
LSCL input=DEM.asc convert


RUSLE
-------------------------------------------------------------------------------
The RUSLE command instructs the program to compute the RUSLE values for an
area. The area is the same as the area covered by the LS factor. All other
factors will be converted by the program to match the size and resolution of
the LS-factor. Null values are supported. LS can be computed by the program
by inputting a DEM or by inputting a precomputed file. Precomputed LS values
will override other commands. The P factor is currently not supported. The size
of the resulting raster is always determined by the LS raster. The output is
either the same size as the input DEM or the ls file depending on which is
used.

Related Commands:

rusle		tells the program to compute RUSLE but the command needs the R, K, 
			C, and LS factors as additional input.

ls!*		tells the program which input file to use as LS-factor source.

r*			tells the program which input file to use as R-factor source.

k*			tells the program which input file to use as K-factor source.

c*			tells the program which input file to use as C-factor source.

Usage: 
LSCL rusle input=DEM.bin r=dem_r.bin k=dem_k.bin c=dem_c.bin d8
LSCL rusle ls=dem_ls.bin r=dem_r.bin k=dem_k.bin c=dem_c.bin 


Flow Accumulation
-------------------------------------------------------------------------------
The Flow accumulation produces the accumulation values for a DEM for all
chosen flow routing algorithms. Flow accumulation is automatically enabled
when computation of the LS-factor is required. Flow accumulation requires
that one or more flow routing algorithms are selected. Accumulation can be
computed using two methods: Transfer or topological. Transfer is the default.
Topological preprocesses the DEM by sorting to achieve better execution speed
but requires more memory on the GPU. The sorting can be saved to disk for 
future use to rapidly speed up future accumulation computations. The program
will look for the presence of a .sort and .vector file having the same name as
the 

Related commands:
accumulaton		tells the program to compute flow Accumulation for all given flow
				routing algorithms and write out the accumulation values to the
				file system as ArcAscii grids or binary files.

topological		tells the program to use the topological method for all flow
				routing algorithms.

savesort*@		enables or disables the storage of the sort to disk. savesort=1
				enables and anything else disables.

forcesort		forces the program to resort the DEM and use the new result. 
				When forcesort is used, the sort will not be saved to disk.

Usage:
LSCL input=DEM.asc accumulation d8
LSCL input=DEM.asc accumulation d8 topological
LSCL input=DEM.asc accumulation topological forcesort
LSCL savesort=0


Choosing a Flow Routing Algorithm
-------------------------------------------------------------------------------
Four different flow routing algorithms are currently supported: D8, FD8 (MD8),
FDD8 (MDD8) and Anisotropic Flow Routing. Flow routing algorithms need to be
specified when accumulation is needed. The default threshold for FDD8 is 250.
The exponents M and Q0 are used by the Anisotropic Flow Routing algorithm. M is
the convergence factor and Q0 is the anisotropic impact factor. They are both 
set to 1 by default. The threshold and anisotropic exponents are saved to file.
The anisotropic flow routing algorithm requires additional input in the form of
vectors. Vectors should be angles preferably in the form 0-360 and the program
will normalize any angles to within that range. Vectors can have no data
values.

Related commands:

d8				tells the program to use the D8 flow routing algorithm.				

fd8				tells the program to use the FD8 flow routing algorithm.

fdd8			tells the program to use the FDD8 flow routing algorithm.

anisotropic		tells the program to use the Anisotropic flow routing 
				algorithm.

vectors*		tells the program to read in anisotropic vectors from the
				specified file. 

threshold*@		sets the accumulation value threshold when LSCL switches to
				single flow. 

a_m*@			sets the convergence factor how much the anisotropic flow
				should converge.

a_q0*@			sets the anisotropic factor how much the flow should be
				affected by the anisotropic vectors.

Usage:
LSCL input=DEM.bin accumulation d8 fd8 fdd8 
LSCL input=DEM.bin accumulation fdd8 threshold=345
LSCL input=DEM.bin ls anisotropic vectors=vectors.bin accumulation a_m=3 a_q0=2
LSCL a_m=2 a_q0=4 threshold=200

LS-Factor
-------------------------------------------------------------------------------
The LS command instructs the program to produce a LS-factor raster for the
input DEM. To compute the LS-factor, flow accumulation and slope is needed.
These cannot currently be given as input directly to the program and must be 
computed from an input DEM. Slope is set to compute automatically but the flow
routing algorithm to be used for the accumulation must be specified as input.  
The LS command and the LS input file both use the same command name but behave
differently. The LS equation features two exponents, M and N, which control the
impact of the flow accumulation and slope. Their default values are 0.6 and 1.3
respectively.

Related commands:

ls				tells the program to output the LS-factor raster to the file
				system.

ls_m*@			sets the M exponent which impacts the flow accumulation.

ls_n*@			sets the N exponent which impacts the slope.

Usage:
LSCL ls d8 fd8 input=DEM.bin
LSCL ls_m=0.9 ls_n=1.1
LSCL ls_m=0.5 ls_n=1.5 ls input=DEM.bin fdd8


Slope
-------------------------------------------------------------------------------
The only currently used slope algorithm is Horn's method which is similar to
the sobel operator. Slope will automatically be computed when the LS-factor is
needed. The program supports outputting the slope values to file. Slope will be
computed from the main input DEM.

Related commands:

slope			tells the program to output the slope raster to the file
				system.

Usage:

LSCL input=DEM.bin slope


OpenCL
-------------------------------------------------------------------------------
The program can currently only use a single OpenCL device to compute the 
program functions. The program by default uses the first OpenCL platform it can
find and the first OpenCL device for that platform to use. Another device and
platform can easily be selected. The program assigns each platform and device
a number which is used to set which platform and device should be used. The
OpenCL settings are saved to file. LSCL requires that at least OpenCL 1.1 is 
installed on the system. The OpenCL kernel code is by default placed in:
<program directory>/kernels/LSCL.cl

Related commands:

listplatforms!	lists all detected OpenCL platforms in numbered order.

listdevices!	lists all detected OpenCL devices in numbered order for the
				chosen platform.

platform*@		sets the OpenCL platform that will be used to the chosen
				platform. 

devices*@		sets the OpenCL device that will be used to the chosen device.

kernel*@		tells the program where to look for the OpenCL kernels which
				will compute the program functions. Allows the LSCL file to be
				placed elsewhere if desired.

Usage:
LSCL listplatforms
LSCL listdevices
LSCL platform=1
LSCL accumulation fd8 ls device=2
LSCL kernel=../OpenCL/LSCL.cl


Memory Requirements
-------------------------------------------------------------------------------
The following table lists the number of bytes required of device memory per
cell in the input DEM. There is currently no elegant memory check to see if
the processes fit in memory and the program will exit with an error message if
the device runs out of memory.

Flow Accumulation Transfer Algorithms:
Anisotropic		18	
D8				6
FD8				14
FDD8			15

Flow Accumulation Topological Algorithms:
Anisotropic		21	
D8				9
FD8				17
FDD8			18

Other Algorithms:
LS				12
RUSLE			16
Slope			8


Debug Commands
-------------------------------------------------------------------------------
Several commands were added to assist with debugging the program and gathering
interesting data. These commands include printing out the results between steps
and a small testcase for verifying that the algorithms perform correctly. The
debug commands are not necessary but are still included in the program in case
they are needed later. 

Related Commands

testcase!		performs a small testcase testing all flow routing algorithms,
				slope and LS for a very small testarea. The command prints out
				a lot of text output to the console.

ortega			enables the flow transfer accumulation method. Ortega and 
				Rueda's Flow Transfer algorithm for parallelizing D8. It acted
				as the basis for the more efficient transfer algorithms. The 
				Flow transfer method only implements D8, performs slower than 
				standard transfer and requires more memory. The method requires
				11 bytes of device RAM per cell.

zhan			enables the indegree accumulation method. Zhan and Qin's 
				indegree accumulation kernel is a more efficient method
				than the standard transfer methods at the cost of some extra 
				memory. The method was implemented and tested late in the 
				project primarily for performance comparisons. The method still
				performs slower and uses more RAM than the topological kernels.
				18 bytes of device RAM is needed per cell. Only FD8 is
				implemented.

directions		writes the flow directions for the used flow routing algorithms
				to the file system as a text file.

dependencies	writes the flow dependencies which are flow directions inverted
				to the file system as a text file.

normalization	writes the normalization values for FD8-based algorithms to
				the file system as an ArcAscii file.

profile			tells OpenCL to accurately measure the time the individual
				kernels take to complete. The output is written to console.

Usage:
LSCL testcase
LSCL accumulation ortega zhan profile input=DEM.bin
LSCL d8 directions dependencies input=DEM.bin
LSCL fd8 anisotropic vectors=dem_vectors.bin normalization input=DEM.bin


Configuration File
-------------------------------------------------------------------------------
The program creates a configuration file named cl.cfg in the same folder as 
the LSCL executable which stores selected OpenCL platform and device,
anisotropic exponents, LS exponents, FDD8 threshold, if topological sorts of
DEMs are stored in the file system and where the kernel file can be found. The
file is in text format and can be edited using a text editor. If the file 
becomes damaged it can be deleted safely and the LSCL program will recreate it
with default values during the next execution.


Command Override Order
-------------------------------------------------------------------------------
From highest to lowest priority:
-listplatforms
-listdevices
-convert
-testcase
-rusle using ls=
-the rest

