/*
 * LSCL.hpp
 *
 *  Created on: 3 Jun 2013
 *      Author: josten
 */

#ifndef LSCL_HPP_
#define LSCL_HPP_

#include <iostream>

#include "CL/cl.h"

void runOpenCLTestCase();

// ls.cpp
void LS(
	cl_float *accumulation,
	cl_float *slopes,
	const cl_float m,
	const cl_float n,
	cl_float *LS,
	std::string algorith);

// rusle.cpp
void freeRUSLE();

header* getLSFileHeader();

void RUSLE(
	cl_float* computed_LS,
	cl_float *RUSLE_results,
	std::string algorithm);

void setupRUSLE(
	std::string filename_R,
	std::string filename_K,
	std::string filename_LS,
	std::string filename_C);

// slope.cpp
void slope(
	cl_float *elevations,
	cl_float *slopes);

void writeSlopeToFileAsDegrees(
	std::string filename,
	cl_float *slopes);


// transfer.cpp
void flowTransferAnisotropic(
	cl_float *elevation_data,
	cl_float M,
	cl_float Q_0,
	cl_float *vector_data,
	cl_float *accumulation_data);

void flowTransferD8(
	cl_float *elevation_data,
	cl_float *accumulation_data);

void flowTransferFD8(
	cl_float *elevation_data,
	cl_float *accumulation_data);

void flowTransferFDD8(
	cl_float *elevation_data,
	cl_float threshold,
	cl_float *accumulation_data);

void ortegaRueda(
	cl_float *elevation_data,
	cl_float *accumulation_data);

void zhanQin(
	cl_float *elevation_data,
	cl_float *accumulation_data);


//topological.cpp
void topologicalAccumulation(
	cl_float *elevation_data,
	cl_float *anisotropic_accumulation_data,
	cl_float *d8_accumulation_data,
	cl_float *fd8_accumulation_data,
	cl_float *fdd8_accumulation_data,
	cl_float threshold,
	cl_float M,
	cl_float Q_0,
	cl_float *vector_data);

#endif /* LSCL_HPP_ */
