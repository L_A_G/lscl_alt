/*
 * build_program.cpp
 *
 *  Created on: 4 Jun 2013
 *      Author: josten
 */

#include <fstream>
#include <iostream>
#include <string>

#include "CL/cl.h"

#include "error.hpp"
#include "strings.hpp"

cl_program buildProgram(std::string filename, cl_context context, cl_device_id device) {
	// This function takes the OpenCL kernel source code file and compiles it to executable code

	const char *filename_chars = filename.c_str();
	std::ifstream file_source(filename_chars);
	std::string error_string = STR_READING_FROM + filename;
	if (!file_source.is_open()) {
		reportError(error_string);
	}

	std::string program_source(std::istreambuf_iterator<char>(file_source), (std::istreambuf_iterator<char>()));
	const char *program_chars = program_source.c_str();
	size_t program_length = program_source.length();

	// Create program from source
	cl_int errors[4] = {};
	cl_program program = clCreateProgramWithSource(context, 1, &program_chars, &program_length, &errors[0]);

	// Build Program
	errors[1] = clBuildProgram(program, 1, &device, NULL, NULL, NULL);

	// Get size of build log
	size_t build_log_size;
	errors[2] = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 0, NULL, &build_log_size);

	// Get build log
	char *build_log = (char*) alloca(build_log_size);
	errors[3] = clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, build_log_size, build_log, NULL);

	// Write out build log
	std::string build_log_string(build_log);
	std::ofstream build_log_file;
	build_log_file.open(STR_BUILD_LOG);
	build_log_file << build_log_string;
	build_log_file.close();

	// Alert user if some error occurred while building
	if (errors[1] != CL_SUCCESS) {
		std::cerr << "Error in OpenCL C source. See build log\n" << std::endl;
		std::cerr << build_log;
	}

	std::string source = "Build Log";
	checkForCLErrors(errors, 4, source);

	return program;
}
