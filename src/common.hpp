/*
 * common.hpp
 *
 *  Created on: 1 Aug 2013
 *      Author: josten
 */

#ifndef COMMON_KERNELS_HPP_
#define COMMON_KERNELS_HPP_

#include <iostream>

#include <CL/cl.h>

// OpenCL profiling event numbers
#define PROFILING_EVENTS			4
#define PROFILING_SETUP_START		0
#define PROFILING_SETUP_STOP 		1
#define PROFILING_EXECUTION_START	2
#define PROFILING_EXECUTION_STOP	3

void calculateProfilingTimes(cl_event profiling_events[4], std::string source);
bool onEdge(cl_uint row, cl_uint column);
void setAnisotropicNormalizationKernelArgs(cl_kernel normalization_kernel, cl_mem elevation_buffer, cl_float M, cl_float Q_0, cl_mem vector_buffer, cl_mem normalization_buffer, std::string source);
void setFD8NormalizationKernelArgs(cl_kernel normalization_kernel, cl_mem elevation_buffer, cl_mem normalization_buffer, std::string source);
void setFillFloatKernelArgs(cl_kernel fill_kernel, cl_mem float_buffer, cl_uint elements, cl_float pattern, std::string source);
void setFillFloatEdgeKernelArgs(cl_kernel fill_kernel, cl_mem float_buffer, cl_float pattern, std::string source);
void setFillUcharKernelArgs(cl_kernel fill_kernel, cl_mem uchar_buffer, cl_uint elements, cl_uchar pattern, std::string source);
void setDependencyKernelArgs(cl_kernel dependency_kernel, cl_mem direction_buffer, cl_mem dependency_buffer, std::string source);
void setDirectionKernelArgs(cl_kernel direction_kernel, cl_mem elevation_buffer, cl_mem direction_buffer, std::string source);

#endif /* COMMON_KERNELS_HPP_ */
