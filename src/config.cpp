/*
 * config.cpp
 *
 *  Created on: 6 Aug 2013
 *      Author: josten
 */

#include <fstream>
#include <iostream>
#include <string>

#include <CL/cl.h>

#include "strings.hpp"

const int NUMBER_CONFIGS = 9;
const std::string ANISO_M		= STR_CONFIG_ANISO_M;
const std::string ANISO_Q0		= STR_CONFIG_ANISO_Q0;
const std::string DEVICE		= STR_CONFIG_DEVICE;
const std::string KERNEL_PATH	= STR_CONFIG_KERNEL_PATH;
const std::string LS_MFACTOR	= STR_CONFIG_LS_MFACTOR;
const std::string LS_NFACTOR	= STR_CONFIG_LS_NFACTOR;
const std::string PLATFORM		= STR_CONFIG_PLATFORM;
const std::string SAVE_SORT		= STR_CONFIG_SAVE_SORT;
const std::string THRESHOLD		= STR_CONFIG_THRESHOLD;

cl_int readConfigFile(
	cl_uint *platform_number,
	cl_uint *device_number,
	cl_float *anisotropic_m,
	cl_float *anisotropic_q0,
	cl_float *ls_m,
	cl_float *ls_n,
	cl_float *fdd8_threshold,
	cl_bool *save_sort,
	std::string *kernel_location)
{
	char line[128];

	std::ifstream file;
	file.open(STR_SETTINGS_FILENAME);

	cl_int status = 0;

	// Keep track of which settings have been read
	bool read[NUMBER_CONFIGS];
	for (int config = 0; config < NUMBER_CONFIGS; config++) {
		read[config] = false;
	}

	// Read from file
	if (file.is_open()) {
		while (!file.eof()) {
			file.getline(line, 128);
			std::string line_string(line);

			if (line_string.compare(0, PLATFORM.length(), PLATFORM) == 0) {
				std::string value = line_string.substr(PLATFORM.length());
				*platform_number = atoi(value.c_str());
				read[0] = true;

			} else if (line_string.compare(0, DEVICE.length(), DEVICE) == 0) {
				std::string value = line_string.substr(DEVICE.length());
				*device_number = (cl_uint) atoi(value.c_str());
				read[1] = true;

			} else if (line_string.compare(0, LS_MFACTOR.length(), LS_MFACTOR) == 0) {
				std::string value = line_string.substr(LS_MFACTOR.length());
				*ls_m = (cl_float) atof(value.c_str());
				read[2] = true;

			} else if (line_string.compare(0, LS_NFACTOR.length(), LS_NFACTOR) == 0) {
				std::string value = line_string.substr(LS_NFACTOR.length());
				*ls_n = (cl_float) atof(value.c_str());
				read[3] = true;

			} else if (line_string.compare(0, THRESHOLD.length(), THRESHOLD) == 0) {
				std::string value = line_string.substr(THRESHOLD.length());
				*fdd8_threshold = (cl_float) atof(value.c_str());
				read[4] = true;
			} else if (line_string.compare(0, ANISO_M.length(), ANISO_M) == 0) {
				std::string value = line_string.substr(ANISO_M.length());
				*anisotropic_m = (cl_float) atof(value.c_str());
				read[5] = true;
			} else if (line_string.compare(0, ANISO_Q0.length(), ANISO_Q0) == 0) {
				std::string value = line_string.substr(ANISO_Q0.length());
				*anisotropic_q0 = (cl_float) atof(value.c_str());
				read[6] = true;
			} else if (line_string.compare(0, SAVE_SORT.length(), SAVE_SORT) == 0) {
				std::string value = line_string.substr(SAVE_SORT.length());
				*save_sort = (value == "TRUE");
				read[7] = true;
			} else if (line_string.compare(0, KERNEL_PATH.length(), KERNEL_PATH) == 0) {
				std::string value = line_string.substr(KERNEL_PATH.length());
				*kernel_location = value;
				read[8] = true;
			}
		}
	}

	// Were all settings found
	bool readAll = true;
	for (int config = 0; config < NUMBER_CONFIGS; config++) {
		readAll &= read[config];
	}

	if (!readAll) {
		status = -1;
	}

	// Did an error occur
	if (file.fail() && !file.eof()) {
		status = -1;
	}

	file.close();
	return status;
}

cl_int writeConfigFile(
	cl_uint platform_number,
	cl_uint device_number,
	cl_float anisotropic_m,
	cl_float anisotropic_q0,
	cl_float ls_m,
	cl_float ls_n,
	cl_float fdd8_threshold,
	cl_bool save_sort,
	std::string kernel_location)
{
	// This function assumes configuration file exists

	std::ofstream file;
	file.open(STR_SETTINGS_FILENAME);
	cl_int status = 0;

	if (file.is_open()) {
		file << STR_CONFIG_PLATFORM << platform_number 	<< std::endl;
		file << DEVICE 	 			<< device_number 	<< std::endl;
		file << ANISO_M 			<< anisotropic_m	<< std::endl;
		file << ANISO_Q0			<< anisotropic_q0 	<< std::endl;
		file << LS_MFACTOR  		<< ls_m				<< std::endl;
		file << LS_NFACTOR  		<< ls_n				<< std::endl;
		file << THRESHOLD			<< fdd8_threshold	<< std::endl;
		file << SAVE_SORT;
		if (save_sort) {
			file << "TRUE";
		} else {
			file << "FALSE";
		}
		file << std::endl;
		file << KERNEL_PATH 		<< kernel_location << std::endl;
	}

	if (file.fail()) {
		std::cerr << "Could not write to " << STR_SETTINGS_FILENAME << std::endl;
		status = -1;
	}

	file.close();
	return status;
}
