/*
 * config.hpp
 *
 *  Created on: 6 Aug 2013
 *      Author: josten
 */

#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#include <iostream>

#include <CL/cl.h>

cl_int readConfigFile(
	cl_uint *platform_number,
	cl_uint *device_number,
	cl_float *anisotropic_m,
	cl_float *anisotropic_q0,
	cl_float *ls_m,
	cl_float *ls_n,
	cl_float *fdd8_threshold,
	cl_bool *save_sort,
	std::string *kernel_location);

cl_int writeConfigFile(
	cl_uint platform_number,
	cl_uint device_number,
	cl_float anisotropic_m,
	cl_float anisotropic_q0,
	cl_float ls_m,
	cl_float ls_n,
	cl_float fdd8_threshold,
	cl_bool save_sort,
	std::string kernel_location);

#endif /* CONFIG_HPP_ */
