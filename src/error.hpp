/*
 * error.hpp
 *
 *  Created on: 1 Aug 2013
 *      Author: josten
 */

#ifndef ERROR_HPP_
#define ERROR_HPP_

#include <CL/cl.h>

void checkFileExtension(std::string path);
void checkForCLErrors(const cl_int *array, const cl_uint length, const std::string source);
void reportError(std::string error_message);

#endif /* ERROR_HPP_ */
