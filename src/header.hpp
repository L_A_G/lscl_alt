/*
 * header.h
 *
 *  Created on: 19 Jun 2013
 *      Author: josten
 */

#ifndef HEADER_H_
#define HEADER_H_

typedef struct {
	unsigned int rows;
	unsigned int columns;
	float xll_corner;
	float yll_corner;
	float resolution;
	float no_data_value;
} header;

#endif /* HEADER_H_ */
