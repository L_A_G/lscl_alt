/*
 * io.c
 *
 *  Created on: 8 Mar 2013
 *      Author: josten
 */

#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "error.hpp"
#include "header.hpp"
#include "io.hpp"
#include "strings.hpp"

extern header *head;

#define MAX_ANGLE 360.0f
#define INVALID_ANGLE -1.0f

// Used to name topological sort filenames and output files
extern bool performRUSLEFromFile;
extern std::string elevation_filename;
extern std::string ls_filename;

// Forward Declarations
std::string getFileNameWithoutExtension(std::string path);
std::string getFilePathWithoutFile(std::string path);
std::string getOutputFileName(std::string path);
int readArcASCII(const char *path, header *head, float **DEM);
int readBinary(const char *path, header *head, float **DEM);
int writeArcASCII(const char *path, header *head, float *DEM);
int writeBinary(const char *path, header *head, float *DEM);
int writeIntegerGrid(const char *path, int *grid);
int writeUnsignedGrid(const char *path, unsigned char *grid);

void convertDEMFileType(std::string path) {
	// Converts a DEM from Binary to ASCII or vice versa

	std::string input_extension = getFileExtension(path);
	std::string input_filename = getFileNameWithoutExtension(path);

	header input_header = {0};
	header *header_to_use = &input_header;
	float *dem;

	std::string output_extension;
	if (input_extension == STR_ASCII_EXTENSION) {
		output_extension = STR_BINARY_EXTENSION;
	} else if (input_extension == STR_BINARY_EXTENSION) {
		output_extension = STR_ASCII_EXTENSION;
	} else {
		// Unknown file type
		std::string error_message(STR_ERROR_EXTENSION + path);
		reportError(error_message);
	}

	std::string output_file (input_filename + "." + output_extension);
	readDEMFromFile(path.c_str(), &dem, header_to_use);
	writeDEMToFile(output_file.c_str(), dem, header_to_use, false);

	free(dem);
}

float* convertDEMData(const float *data, const header target, const header source) {

	// This function extracts a sub-area out of the source data into an array the scale and size of target
	// and returns a pointer to those elements

	// Assumptions
	// ll corner of source is always to the left and below the ll corner of target
	// ll corner increases towards north and east
	// target is of a equal or more detailed resolution than source
	// target DEM must fit inside source

	// Deallocation is handled by caller code

	// Check that the new grid fits inside old source, ie. there is enough data available
	cl_float longest_pos_of_source = source.xll_corner + source.columns * source.resolution;
	cl_float highest_pos_of_source = source.yll_corner + source.rows * source.resolution;
	cl_float longest_pos_of_target = target.xll_corner + target.columns * target.resolution;
	cl_float highest_pos_of_target = target.yll_corner + target.rows * target.resolution;

	if (target.xll_corner < source.xll_corner || target.yll_corner < source.yll_corner) {
		reportError("target's lower left corner is out of bounds of source");
	}

	if (longest_pos_of_source < longest_pos_of_target || highest_pos_of_source < highest_pos_of_target) {
		reportError("Target grid does not fit within source data");
	}

	cl_float *results = new float[target.rows * target.columns];

	// Establish starting point for source
	cl_float dx = target.xll_corner - source.xll_corner;
	cl_float dy = target.yll_corner - source.yll_corner;

	// Offsets are the starting position in the same units as xll_corner for the upper left corner of the grid
	// for which we wish to generate elements, inside the larger source DEM
	cl_float y_offset = (source.rows * source.resolution) - dy - (target.rows * target.resolution);
	cl_float x_offset = dx;

	// Start is the starting position converted to source grid units, used to fetch cells from the source data
	cl_uint x_start = (cl_uint)(x_offset / source.resolution);
	cl_uint y_start = (cl_uint)(y_offset / source.resolution);

	// The scaled matrix is traversed in steps. When these steps pass the edge of two cells in the source matrix,
	// two or more elements may have to be considered

	cl_float x_start_remainder = fmod(x_offset, source.resolution);
	cl_float y_start_remainder = fmod(y_offset, source.resolution);
	cl_float x_step = x_start_remainder;
	cl_float y_step = y_start_remainder;

	for (cl_uint row = 0; row < target.rows; row++) {

		y_step += target.resolution;

		// y_ratio tells how much weight two cells on a vertical axis have on the final value
		cl_float y_ratio;
		if (y_step < source.resolution) {
			y_ratio = 1.0f;
		} else {
			y_ratio = (target.resolution + source.resolution - y_step) / target.resolution;
			y_step -= source.resolution;
		}

		for (cl_uint column = 0; column < target.columns; column++) {
			x_step += target.resolution;

			// y_ratio tells how much weight two cells on a horizontal axis have on the final value
			// the ratio keeps track of how long a step has been taken into the new cell.
			// This is taken into account during computation
			cl_float x_ratio;
			if (x_step < source.resolution) {
				x_ratio = 1.0f;
			} else {
				x_ratio = (target.resolution + source.resolution - x_step) / target.resolution;
				x_step -= source.resolution;
			}

			// Compute the x and y position in the source grid to the respective current row and column
			cl_uint x_source_index = x_start + (cl_uint)(((float)column * target.resolution + x_start_remainder) / source.resolution);
			cl_uint y_source_index = y_start + (cl_uint)(((float)row    * target.resolution + y_start_remainder) / source.resolution);

			/*
			 * _______
			 * |a | b|
			 * -------
			 * |c | d|
			 * -------
			 * a,b,c,d create a small box extending from the current elements toward the lower right in the source grid
			 * the cell a is always the main source for the current conversion grid cell being examined and is always read
			 * b is read when the conversion cell passes the border between two horizontal cells in source
			 * c is read when the conversion cell passes the border between two vertical cells in source
			 * d is read when the conversion cell passes both the horizontal and the vertical border
			 */

			cl_float a = 0.0f;
			cl_float b = 0.0f;
			cl_float c = 0.0f;
			cl_float d = 0.0f;
			cl_bool x_fits_within_one_source_cell = (x_ratio == 1.0f);
			cl_bool y_fits_within_one_source_cell = (y_ratio == 1.0f);
			cl_bool next_x_is_within_grid = (x_source_index + 1 < source.columns);
			cl_bool next_y_is_within_grid = (y_source_index + 1 < source.rows);

			// a can be read immediately because the checks at the beginning of the function guarantee that we are within the source grid for this cell
			a = data[y_source_index * source.columns + x_source_index];

			// The rest have to be checked, if a fits inside a single source cell, b, c, and d is not required and set to 0
			// If they are required however, we must check that we do not read past the edge. Beyond that any value can be encountered,
			// and the starting function checks may not be able to ward against problems. In this case we simply write no_data_value to
			// avoid making this function unnecessarily complicated. In most cases b, c, and d should be set to 0 for the last column and row
			if (x_fits_within_one_source_cell) {
				b = 0.0f;
				d = 0.0f;
			} else {
				// next source column required
				if (next_x_is_within_grid) {
					b = data[y_source_index * source.columns + x_source_index + 1];
				} else {
					b = source.no_data_value;
					d = source.no_data_value;
				}
			}

			if (y_fits_within_one_source_cell) {
				c = 0.0f;
				d = 0.0f;
			} else {
				// next source row required
				if (next_y_is_within_grid) {
					c = data[(y_source_index + 1) * source.columns + x_source_index];
				} else {
					c = source.no_data_value;
					d = source.no_data_value;
				}
			}

			if (!x_fits_within_one_source_cell && !y_fits_within_one_source_cell) {
				if (next_x_is_within_grid && next_y_is_within_grid) {
					d = data[(y_source_index + 1) * source.columns + x_source_index + 1];
				}
			}

			// Compute the result using a weighted average
			float result = 0.0f;
			if (a == source.no_data_value || b == source.no_data_value || c == source.no_data_value || d == source.no_data_value) {
				result = target.no_data_value;
			} else {
				result = y_ratio * (x_ratio * a + (1.0f - x_ratio) * b) +
						(1.0f - y_ratio) * (x_ratio * c + (1.0f - x_ratio) * d);
			}

			cl_uint index = row * target.columns + column;
			results[index] = result;
		}

		x_step = fmod(x_offset, source.resolution);
	}

	return results;
}

std::string getFileExtension(std::string path) {
	// Attempts to determine the file extension
	// returns extension if success, empty string "" if fail
	size_t position = path.rfind(".", path.length());
	std::string extension = "";

	if (position != path.npos) {
		extension = path.substr(position + 1); // + 1 gets rid of .
	}

	return extension;
}

std::string getFileNameWithoutExtension(std::string path) {

	size_t start_of_file_name = path.find_last_of("/\\");
	if (start_of_file_name == path.npos) {
		// Found no "/" or "\"
		start_of_file_name = 0;
	} else {
		start_of_file_name++;
	}

	size_t start_of_extension = path.find_last_of(".");
	if (start_of_extension == path.npos) {
		start_of_extension = path.length();
	}

	size_t full_file_name_length = path.length() - start_of_file_name;
	size_t file_extension_length = path.length() - start_of_extension;
	size_t file_name_only_length = full_file_name_length - file_extension_length;

	std::string file_name = path.substr(start_of_file_name, file_name_only_length);
	return file_name;
}

std::string getFilePathWithoutFile(std::string path) {
	size_t position_last_slash = path.find_last_of("/\\");
	if (position_last_slash == path.npos) {
		// Found no "/" or "\"
		position_last_slash = 0;
	} else {
		position_last_slash++;
	}

	std::string file_path = path.substr(0, position_last_slash);
	return file_path;
}

std::string getOutputFileName(std::string path) {
	// This function appends the input file name without extensions to the beginning of the
	// output file name

	// Get input file name
	std::string source;
	if (performRUSLEFromFile) {
		if (ls_filename == "") {
			reportError("LS file doesn't exist");
		}

		source = getFileNameWithoutExtension(ls_filename);
	} else {
		if (elevation_filename == "") {
			reportError("Elevation file doesn't exist");
		}

		source = getFileNameWithoutExtension(elevation_filename);
	}

	std::string destination = source + "_" + path;
	return destination;
}

void normalizeVectors(float *vectors, header *head) {
	// Normalizes all angles to 0 - 360 degree range, no_data values are turned into negative values
	// The use of negative floats as no_data reduces arguments to anisotropic kernels

	unsigned int total_cells = head->rows * head->columns;

	for (unsigned int index = 0; index < total_cells; index++) {
		float angle = vectors[index];

		if (angle != head->no_data_value) {
			while (angle < 0.0f) {
				angle += MAX_ANGLE;
			}

			while (angle >= MAX_ANGLE) {
				angle -= MAX_ANGLE;
			}

			vectors[index] = angle;
		} else {
			vectors[index] = INVALID_ANGLE;
		}
	}
}

int readArcASCII(const char *path, header *head, float **DEM) {
	// Open File
	FILE *fp = fopen(path,"rt");
	if (fp == NULL) {
		printf("Could not open file %s\n",path);
		return -1;
	}

	// Read Header for ArcASCII grid
	char str[100];
	size_t read_elements = 0;
	read_elements += fscanf(fp, "%s %d\n", str, &head->columns);
	read_elements += fscanf(fp, "%s %d\n", str, &head->rows);
	read_elements += fscanf(fp, "%s %f\n", str, &head->xll_corner);
	read_elements += fscanf(fp, "%s %f\n", str, &head->yll_corner);
	read_elements += fscanf(fp, "%s %f\n", str, &head->resolution);
	read_elements += fscanf(fp, "%s %f\n", str, &head->no_data_value);
	if (ferror(fp)) {
		printf("Could not read file header\n");
		printf("Read %lu header elements\n", read_elements);
		return -2;
	}

	// Print Header
	printf("\nFile:\t\t%s\n", path);
	printf("Map size:\t%d x %d\n", head->columns, head->rows);
	printf("Corner:\t\t(%f,%f)\n", head->xll_corner, head->yll_corner);
	printf("Pixel size:\t%f\n", head->resolution);
	printf("Nodata value:\t%f\n\n", head->no_data_value);

	// Allocate memory for data
	size_t total_elements = head->rows * head->columns;
	float *memory = (float *)calloc(total_elements, sizeof(*memory));
	if (memory == NULL) {
		printf("Could not allocate memory for DEM\n");
		return -3;
	}

	printf("Reading file contents...\n");

	// Read data
	read_elements = 0;
	for (unsigned int row = 0; row < head->rows; row++) {
		for (unsigned int column = 0; column < head->columns; column++) {
			read_elements += fscanf(fp, "%f ", &memory[row * head->columns + column]);
		}
	}

	if (ferror(fp)) {
		printf("Could not read data\n");
		printf("Read %lu cells\n", read_elements);
		return -4;
	}

	fclose(fp);

	// return a pointer to the data
	*DEM = memory;
	return 0;
}

int readBinary(const char *path, header *head, float **DEM) {

	// Open File
	FILE *fp = fopen(path, "rb");
	if (fp == NULL) {
		printf("Could not open file %s\n", path);
		return -1;
	}

	// Read Header for ArcASCII grid
	unsigned int dem_size[2];
	float dem_float_info[4];

	size_t read_elements = 0.0;
	read_elements = fread((void*)&dem_size, sizeof(unsigned int), 2, fp);
	read_elements += fread((void*)&dem_float_info, sizeof(float), 4, fp);
	if (ferror(fp)) {
		printf("Could not read file header\n");
		printf("Read %lu header elements\n", read_elements);
		return -2;
	}

	head->columns = dem_size[0];
	head->rows = dem_size[1];
	head->xll_corner = dem_float_info[0];
	head->yll_corner = dem_float_info[1];
	head->resolution = dem_float_info[2];
	head->no_data_value = dem_float_info[3];

	// Print Header
	printf("\nFile:\t\t%s\n", path);
	printf("Map size:\t%d x %d\n", head->columns, head->rows);
	printf("Corner:\t\t(%f,%f)\n", head->xll_corner, head->yll_corner);
	printf("Pixel size:\t%f\n", head->resolution);
	printf("Nodata value:\t%f\n\n", head->no_data_value);

	// Allocate memory for data
	size_t total_elements = head->rows * head->columns;
	float *memory = (float *)calloc(total_elements, sizeof(*memory));
	if (memory == NULL) {
		printf("Could not allocate memory for DEM\n");
		return -3;
	}

	printf("Reading file contents...\n");

	// Read data
	read_elements = fread(memory, sizeof(*memory), total_elements, fp);
	if (ferror(fp)) {
		printf("Could not read data\n");
		printf("Read %lu cells\n", read_elements);
		return -4;
	}

	fclose(fp);

	// return a pointer to the data
	*DEM = memory;
	return 0;
}

void readDEMFromFile(std::string path, float **DEM, header *chosen_header) {
	// This function is a wrapper for readArcASCII/readBinary and supplies the functions with the appropriate header
	// as well as handles any errors return by them
	std::cout << STR_READING_FROM << path << std::endl;

	header *header_to_use = (chosen_header) ? chosen_header : head;
	std::string extension = getFileExtension(path);

	int could_not_read = -1;
	if (extension == STR_ASCII_EXTENSION) {
		could_not_read = readArcASCII(path.c_str(), header_to_use, DEM);
	} else if (extension == STR_BINARY_EXTENSION) {
		could_not_read = readBinary(path.c_str(), header_to_use, DEM);
	} else {
		// Unknown extension, Should be caught earlier
		std::string error_message(STR_ERROR_EXTENSION + path);
		reportError(error_message);
	}

	// Was data read correctly
	if (could_not_read) {
		std::string error_message(STR_ERROR_READING + path);
		reportError(error_message);
	}
}

bool readTopologicalSortFromFile(unsigned int **sort, std::vector<unsigned int> *vector) {
	// This function reads a topological sort from a file, binary only, file is assumed to be the sort for the DEM being calculated
	// The function will not modify arguments unless both sort and vector file is successfully found
	// Sort and vector files are assumed to reside in the same folder as the input_file
	// Sort file is assumed to be an array the size of the elevation file, consisting of unsigned integers which contain indices
	// Vector file is assumed to be an array of unsigned integers without header
	// Caller has to delete memory allocated by this function
	// Returns true if file was successfully found and read
	std::string error_message;

	if (elevation_filename == "" || sort == NULL || vector == NULL) {
		error_message = "Invalid Arguments";
		reportError(error_message);
	}

	if (!head) {
		error_message = "No elevation header available";
		reportError(error_message);
	}

	// Open files and check if they exist
	std::string sort_filename = getFileNameWithoutExtension(elevation_filename) + "." + STR_SORT_EXTENSION;
	std::string vector_filename = getFileNameWithoutExtension(elevation_filename) + "." + STR_VECTOR_EXTENSION;
	std::ifstream sort_file(sort_filename.c_str(), std::ios_base::binary);
	std::ifstream vector_file(vector_filename.c_str(), std::ios_base::binary);

	if (!sort_file.is_open() || !vector_file.is_open()) {
		return false;
	}

	// Read Indices file
	std::cout << STR_READING_FROM << sort_filename << std::endl;
	unsigned int sort_size_in_chars = head->rows * head->columns * sizeof(**sort);
	if (sort_size_in_chars == 0) {
		error_message = STR_ERROR_READING + sort_filename;
		reportError(error_message);
	}

	if (sort_size_in_chars % sizeof(**sort) != 0) {
		error_message = "Sort file not multiple of unsigned int";
		reportError(error_message);
	}

	char *data = new char[sort_size_in_chars];
	sort_file.read(data, sort_size_in_chars);
	if (sort_file.fail()) {
		error_message = STR_ERROR_READING + sort_filename;
		reportError(error_message);
	}

	sort_file.close();
	*sort = (unsigned int *) data;

	// Read vector file
	std::cout << STR_READING_FROM << vector_filename << std::endl;
	vector_file.seekg(0, std::ios_base::end);
	unsigned int vector_file_size = vector_file.tellg();
	vector_file.seekg(0, std::ios_base::beg);
	if (vector_file_size == 0) {
		error_message = STR_ERROR_READING + vector_filename;
		reportError(error_message);
	}

	if (vector_file_size % sizeof(unsigned int) != 0) {
		error_message = "Vector file not multiple of unsigned int";
		reportError(error_message);
	}

	char *vector_data_chars = new char[vector_file_size];
	vector_file.read(vector_data_chars, vector_file_size);
	if (vector_file.fail()) {
		error_message = STR_ERROR_READING + vector_filename;
		reportError(error_message);
	}

	unsigned int *vector_data = (unsigned int *) vector_data_chars;
	for (unsigned int iteration = 0; iteration < vector_file_size / sizeof(iteration); iteration++) {
		vector->push_back(vector_data[iteration]);
	}

	vector_file.close();
	delete[] vector_data_chars;

	return true;
}

int writeArcASCII(const char *path, header *head, float *DEM) {

	// Open File
	FILE *fp = fopen(path, "wt");
	if (fp == NULL) {
		printf("Could not open file %s\n", path);
		return -1;
	}

	// Write header
	fprintf(fp, "ncols\t\t%d\n", head->columns);
	fprintf(fp, "nrows\t\t%d\n", head->rows);
	fprintf(fp, "xllcorner\t%.3f\n", head->xll_corner);
	fprintf(fp, "yllcorner\t%.3f\n", head->yll_corner);
	fprintf(fp, "cellsize\t%g\n", head->resolution);
	fprintf(fp, "nodata_value\t%.1f\n", head->no_data_value);
	if (ferror(fp)) {
		printf("Could not write header to file\n");
		return -2;
	}

	// Write data
	for (unsigned int row = 0; row < head->rows; row++) {
		for (unsigned int column = 0; column < head->columns; column++) {
			unsigned int index = row * head->columns + column;
			fprintf(fp, "%.3f ", DEM[index]);
		}
		fprintf(fp, "\n");
	}

	if (ferror(fp)) {
		printf("Could not write data to file\n");
		return -3;
	}

	// Close file which forces fflush and the data is written to disk, the fflush can fail, resulting in no data being written
	int failed_to_close = fclose(fp);
	if (failed_to_close == EOF) {
		printf("Failed to close file, data might not been written successfully");
		return -4;
	}

	return 0;
}

void writeDEMToFile(std::string path, float *DEM, header *chosen_header, bool add_name) {
	// This function is a wrapper for writeArcASCII/writeBinary and supplies the functions with the appropriate header
	// as well as handles any errors return by them
	header *header_to_use = (chosen_header) ? chosen_header : head;

	std::string destination = "";
	// Create filename
	if (add_name) {
		destination = getOutputFileName(path);
	} else {
		destination = path;
	}

	std::cout << STR_WRITING_TO << destination << std::endl;

	int could_not_write = -1;
	std::string extension = getFileExtension(path);
	if (extension == STR_ASCII_EXTENSION) {
		could_not_write = writeArcASCII(destination.c_str(), header_to_use, DEM);
	} else if (extension == STR_BINARY_EXTENSION) {
		could_not_write = writeBinary(destination.c_str(), header_to_use, DEM);
	} else {
		// Unknown extension, Should be caught earlier
		std::string error_message(STR_ERROR_EXTENSION + path);
		reportError(error_message);
	}

	// Was data written correctly
	if (could_not_write) {
		std::string error_message(STR_ERROR_WRITING + destination);
		reportError(error_message);
	}
}

int writeBinary(const char *path, header *head, float *DEM) {

	// Open file
	FILE *fp = fopen(path, "wb");
	if (fp == NULL) {
		printf("Could not open file %s\n",path);
		return -1;
	}

	// Write header
	unsigned int dem_size[] = {head->columns, head->rows};
	float dem_float_info[] = {head->xll_corner, head->yll_corner, head->resolution, head->no_data_value};

	fwrite(&dem_size, sizeof(unsigned int), 2, fp);
	fwrite(&dem_float_info, sizeof(float), 4, fp);
	if (ferror(fp)) {
		printf("Could not write header to file\n");
		return -2;
	}

	// Write data
	size_t total_elements = head->rows * head->columns;
	fwrite(DEM, sizeof(float), total_elements, fp);
	if (ferror(fp)) {
		printf("Could not write data to file\n");
		return -3;
	}

	// Close file which forces fflush and the data is written to disk, the fflush can fail, resulting in no data being written
	int failed_to_close = fclose(fp);
	if (failed_to_close == EOF) {
		printf("Failed to close file, data might not been written successfully\n");
		return -4;
	}

	return 0;
}

int writeIntegerGrid(const char *path, int *grid) {
	FILE *fp = fopen(path, "wt");
	if (fp == NULL) {
		printf("Could not open file %s\n", path);
		return -1;
	}

	// append data
	for (unsigned int row = 0; row < head->rows; row++) {
		for (unsigned int column = 0; column < head->columns; column++) {
			unsigned int index = row * head->columns + column;
			fprintf(fp, "%d ", grid[index]);
		}
		fprintf(fp, "\n");
	}

	fclose(fp);
	return 0;
}

void writeIntegerGridCheck(std::string path, int *grid) {
	std::cout << STR_WRITING_TO << path << std::endl;
	int could_not_write = writeIntegerGrid(path.c_str(), grid);
	if (could_not_write) {
		std::string error_message(STR_ERROR_WRITING + path);
		reportError(error_message);
	}
}

void writeTopologicalSortToFile(unsigned int *sort, std::vector<unsigned int> vector) {
	// This function writes the topological sort to files, binary only
	std::string error_message;
	std::cout << "Saving topological sort" << std::endl;

	// Check function arguments
	if (elevation_filename == "" || sort == NULL || vector.size() == 0 ) {
		error_message = "Invalid arguments";
		reportError(error_message);
	}

	if (!head) {
		error_message = "No elevation header available";
		reportError(error_message);
	}

	// Write file containing indices
	std::string sort_filename = getFileNameWithoutExtension(elevation_filename) + "." + STR_SORT_EXTENSION;
	std::cout << STR_WRITING_TO << sort_filename << std::endl;

	// Open file
	std::ofstream sort_file(sort_filename.c_str(), std::ios_base::binary);
	if (!sort_file.is_open()) {
		error_message = STR_ERROR_OPENING + sort_filename;
		reportError(error_message);
	}

	// Write sorted indices
	char *index_data = (char *) sort;
	unsigned int sort_size_in_chars = head->rows * head->columns * sizeof(*sort);
	sort_file.write(index_data, sort_size_in_chars);

	if (sort_file.fail()) {
		error_message = STR_ERROR_WRITING + sort_filename;
		reportError(error_message);
	}

	sort_file.close();

	// Write vector file containing iterations
	std::string vector_filename = getFileNameWithoutExtension(elevation_filename) + "." + STR_VECTOR_EXTENSION;
	std::cout << STR_WRITING_TO << vector_filename << std::endl;

	std::ofstream vector_file(vector_filename.c_str(), std::ios_base::binary);
	if (!vector_file.is_open()) {
		error_message = STR_ERROR_OPENING + vector_filename;
		reportError(error_message);
	}

	// Create array from vector
	unsigned int *array = new unsigned int[vector.size()];
	for (unsigned int index = 0; index < vector.size(); index++) {
		array[index] = vector[index];
	}

	// Write vector of iteration sizes
	char *vector_data = (char *) array;
	unsigned int vector_size_in_chars = vector.size() * sizeof(*array);
	vector_file.write(vector_data, vector_size_in_chars);

	if (vector_file.fail()) {
		error_message = STR_ERROR_WRITING + vector_filename;
		reportError(error_message);
	}

	vector_file.close();
	delete[] array;
}

int writeUnsignedGrid(const char *path, unsigned char *grid) {
	FILE *fp = fopen(path, "wt");
	if (fp == NULL) {
		printf("Could not open file %s\n", path);
		return -1;
	}

	// append data
	for (unsigned int row = 0; row < head->rows; row++) {
		for (unsigned int column = 0; column < head->columns; column++) {
			unsigned int index = row * head->columns + column;
			fprintf(fp, "%hhu ", grid[index]);
		}
		fprintf(fp, "\n");
	}

	fclose(fp);
	return 0;
}

void writeUnsignedGridCheck(std::string path, unsigned char *grid) {
	std::cout << STR_WRITING_TO << path << std::endl;
	int could_not_write = writeUnsignedGrid(path.c_str(), grid);
	if (could_not_write) {
		std::string error_message(STR_ERROR_WRITING + path);
		reportError(error_message);
	}
}
