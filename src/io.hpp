/*
 * arc_parser.hpp
 *
 *  Created on: 19 Jun 2013
 *      Author: josten
 */

#ifndef ARC_PARSER_HPP_
#define ARC_PARSER_HPP_

#include "header.hpp"
#include <iostream>
#include <vector>

void convertDEMFileType(std::string path);
float* convertDEMData(const float *data, const header target, const header source);
std::string getFileExtension(std::string path);
void normalizeVectors(float *vectors, header *head);
void readDEMFromFile(std::string path, float **DEM, header *chosen_header = NULL);
bool readTopologicalSortFromFile(unsigned int **sort, std::vector<unsigned int> *vector);
void writeDEMToFile(std::string path, float *DEM, header *chosen_header = NULL, bool add_name = true);
void writeIntegerGridCheck(std::string path, int *grid);
void writeTopologicalSortToFile(unsigned int *sort, std::vector<unsigned int> iteration_sizes);
void writeUnsignedGridCheck(std::string path, unsigned char *grid);

#endif /* ARC_PARSER_HPP_ */
