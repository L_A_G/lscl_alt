/*
 * kernel_size.cpp
 *
 *  Created on: 6 Aug 2013
 *      Author: josten
 */

#include <iostream>

#include <CL/cl.h>

extern size_t max_global_size;
extern size_t max_local_size;

size_t computeTotalSizeWithPadding(size_t total_size) {

	// If perfect fit for work_group
	size_t remainder = total_size % max_global_size;
	if (remainder == 0) {
		return total_size;
	}

	size_t total_size_pad = total_size + max_global_size - remainder;

	// Error check
	if (total_size_pad % max_global_size != 0) {
		std::cerr << "TOTAL_SIZE ERROR: Pad " << total_size_pad % max_local_size << std::endl;
		exit(-1);
	}

	return total_size_pad;
}

void compute2DimKernelSizes(size_t global_size[], size_t *global_work_size, size_t *local_work_size) {
	size_t total_rows = global_size[0];
	size_t total_columns = global_size[1];

	// Set work sizes
	size_t total_rows_pad = total_rows;
	size_t rows_remainder = total_rows % max_local_size;
	if (rows_remainder != 0) {
		total_rows_pad += max_local_size - rows_remainder;
	}

	size_t total_columns_pad = total_columns;
	size_t columns_remainder = total_columns % max_local_size;
	if (columns_remainder != 0) {
		total_columns_pad += max_local_size - columns_remainder;
	}

	global_work_size[0] = total_columns_pad;
	global_work_size[1] = total_rows_pad;
	local_work_size[0] = max_local_size;
	local_work_size[1] = max_local_size;

	// Error check
	if (total_columns_pad % max_local_size != 0) {
		std::cerr << "COlUMN ERROR: Pad " << total_columns_pad % max_local_size << std::endl;
		exit(-1);
	}
	if (total_rows_pad % max_local_size != 0) {
		std::cerr << "ROW ERROR: Pad " << total_rows_pad % max_local_size << std::endl;
		exit(-1);
	}
}
