/*
 * kernel_size.hpp
 *
 *  Created on: 6 Aug 2013
 *      Author: josten
 */

#ifndef KERNEL_SIZE_HPP_
#define KERNEL_SIZE_HPP_

size_t computeTotalSizeWithPadding(size_t total_size);
void compute2DimKernelSizes(size_t global_size[], size_t *global_work_size, size_t *local_work_size);

#endif /* KERNEL_SIZE_HPP_ */
