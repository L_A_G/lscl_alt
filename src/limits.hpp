/*
 * limits.hpp
 *
 *  Created on: 30 Sep 2013
 *      Author: josten
 */

#ifndef LIMITS_HPP_
#define LIMITS_HPP_

#define ANISOTROPIC_M_MAX 25.0f
#define ANISOTROPIC_M_MIN 0.0f
#define ANISOTROPIC_Q0_MAX 25.0f
#define ANISOTROPIC_Q0_MIN 0.0f

#define FDD8_THRESHOLD_MAX 10000.0f
#define FDD8_THRESHOLD_MIN 1.0f

#define LS_M_MAX 25.0f
#define LS_M_MIN 0.1
#define LS_N_MAX 25.0f
#define LS_N_MIN 0.1

#endif /* LIMITS_HPP_ */
