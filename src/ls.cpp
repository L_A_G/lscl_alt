#include <ctime>
#include <string>

#include <CL/cl.h>

#include "common.hpp"
#include "error.hpp"
#include "header.hpp"
#include "kernel_size.hpp"
#include "limits.hpp"
#include "strings.hpp"

extern cl_bool performProfiling;
extern cl_command_queue queue;
extern cl_context context;
extern cl_program program;

extern header *head;
extern size_t max_global_size;
extern size_t max_local_size;

void LS(cl_float *accumulation, cl_float *slopes, const cl_float M, const cl_float N, cl_float *LS, std::string algorithm) {

	std::cout << std::endl << "Starting LS for " << algorithm << std::endl;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];
	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	// Check LS values
	if (M < LS_M_MIN || M > LS_M_MAX) {
		reportError("LS M factor is outside valid range");
	}

	if (N < LS_N_MIN || N > LS_N_MAX) {
		reportError("LS N factor is outside valid range");
	}

	// Measure time
	time_t start = time(0);

	cl_int errors[8];
	std::string source = "LS ";

	// Create kernels
	cl_kernel fill_float_edge_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE, &errors[0]);
	cl_kernel ls_kernel = clCreateKernel(program, STR_KERNEL_LS, &errors[1]);
	checkForCLErrors(errors, 2, source + STR_KERNELS);

	size_t total_size = head->rows * head->columns;

	// Create buffers
	size_t buffer_size = total_size * sizeof(cl_float);
	cl_mem accumulation_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, buffer_size, accumulation, &errors[0]);
	cl_mem slope_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, buffer_size, slopes, &errors[1]);
	cl_mem LS_buffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, buffer_size, NULL, &errors[2]);
	checkForCLErrors(errors, 3, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	// Initialise buffers
	setFillFloatEdgeKernelArgs(fill_float_edge_kernel, LS_buffer, 0.0f, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_float_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);
	profiling_events[PROFILING_SETUP_STOP] = profiling_events[PROFILING_SETUP_START];
	checkForCLErrors(errors, 1, source + STR_SETUP);

	// Set Arguments
	errors[0] = clSetKernelArg(ls_kernel, 0, sizeof(cl_mem), &accumulation_buffer);
	errors[1] = clSetKernelArg(ls_kernel, 1, sizeof(cl_mem), &slope_buffer);
	errors[2] = clSetKernelArg(ls_kernel, 2, sizeof(cl_uint), &head->rows);
	errors[3] = clSetKernelArg(ls_kernel, 3, sizeof(cl_uint), &head->columns);
	errors[4] = clSetKernelArg(ls_kernel, 4, sizeof(cl_float), &head->resolution);
	errors[5] = clSetKernelArg(ls_kernel, 5, sizeof(cl_float), &M);
	errors[6] = clSetKernelArg(ls_kernel, 6, sizeof(cl_float), &N);
	errors[7] = clSetKernelArg(ls_kernel, 7, sizeof(cl_mem), &LS_buffer);
	checkForCLErrors(errors, 8, source + STR_ARGUMENTS);

	// Compute LS and get results
	errors[0] = clEnqueueNDRangeKernel(queue, ls_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_EXECUTION_START]);
	errors[1] = clEnqueueReadBuffer(queue, LS_buffer, CL_TRUE, 0, buffer_size, LS, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[2] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 3, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "LS Factor: " << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(accumulation_buffer);
	errors[1] = clReleaseMemObject(slope_buffer);
	errors[2] = clReleaseMemObject(LS_buffer);
	checkForCLErrors(errors, 3, source + STR_CLEANUP + " " + STR_BUFFERS);

	errors[0] = clReleaseKernel(fill_float_edge_kernel);
	errors[1] = clReleaseKernel(ls_kernel);
	checkForCLErrors(errors, 2, source + STR_CLEANUP + " " + STR_KERNELS);
}
