/*
 * main.cpp
 *
 *  Created on: 3 Jun 2013
 *      Author: josten
 */

/*
 * PROGRAM ASSUMPTIONS
 *
 *	This program conforms to OpenCL 1.1 standard
 *	Some commands related to profiling have been deprecated in subsequent versions
 *
 * -All cells in input raster are squares in size
 * -Number of cells are less than than the maximum number of a unsigned int (4 294 967 295). That's a 16GB large binary DEM of floats
 * -The input elevations must contain valid data in every cell, NO_DATA_VALUE IN INPUT DEM IS NOT SUPPORTED!
 * -All other input DEMs with the exception of the topological sort should support no_data types
 *
 *  Commands for RUSLE
 *  if RUSLE is set and a LS file is given, that data will be used, all other computations will be ignored
 *  if only RUSLE is set, LS will be computed (not printed out)
 *  RUSLE still needs R K and C
 *
 *  Anisotropic Vectors are in degree form, not radians or percent
 *
 *  Order of commands' importance: (Higher Overrides Lower commands)
 *  testcase
 *  printPlatforms
 *  printDevices
 *  convert
 *  Rusle (LS from file)
 *  The rest
 *
 *  Input / Output
 *  Input File extensions supported are .asc (ASCII Grids) or .bin (Binary Grids). No other inputs are currently supported
 *  Both file types include header at start
 *	Program assumes output is binary unless changed by command
 *
 */

#include <ctime>
#include <fstream>
#include <iostream>
#include <sstream>

#include "config.hpp"
#include "error.hpp"
#include "header.hpp"
#include "io.hpp"
#include "limits.hpp"
#include "LSCL.hpp"
#include "setup.hpp"
#include "strings.hpp"

// Configuration defaults, used for initialization and when configuration file is unavailable
#define PLATFORM_DEFAULT 0
#define DEVICE_DEFAULT 0
#define LS_M_DEFAULT 0.6f
#define LS_N_DEFAULT 1.3f
#define THRESHOLD_DEFAULT 250.0f
#define ANISOTROPIC_M_DEFAULT 1.0f
#define ANISOTROPIC_Q0_DEFAULT 1.0f
#define SAVE_SORT_DEFAULT false
#define KERNEL_LOCATION_DEFAULT "../kernels/LSCL.cl"

enum Algorithm {
	FIRST_ALGORITHM = 0,
	ANISOTROPIC = 0,
	D8,
	FD8,
	FDD8,
	ORTEGA,
	ZHAN,
	LAST_ALGORITHM,
	NUMBER_OF_FLOW_ALGORITHMS = LAST_ALGORITHM
};

// Used as indexes for result buffers for respective algorithm
enum Buffer {
	FIRST_BUFFER = 0, // Allows for iteration
	ACCU_ANISOTROPIC = 0,
	ACCU_D8,
	ACCU_FD8,
	ACCU_FDD8,
	ACCU_ORTEGA,
	ACCU_ZHAN,
	SLOPE,
	LS_ANISOTROPIC,
	LS_D8,
	LS_FD8,
	LS_FDD8,
	LS_ORTEGA,
	LS_ZHAN,
	RUSLE_ANISOTROPIC,
	RUSLE_D8,
	RUSLE_FD8,
	RUSLE_FDD8,
	RUSLE_ORTEGA,
	RUSLE_ZHAN,
	//RUSLE_FILE,	 // RUSLE is calculated from file
	LAST_BUFFER, // Allows for iteration
	TOTAL_BUFFERS = LAST_BUFFER
};

// Each of these arrays can be indexed using Anisotropic-Zhan in Algorithm enum
const int ACCUMULATION_BUFFERS[NUMBER_OF_FLOW_ALGORITHMS] 		= {ACCU_ANISOTROPIC, ACCU_D8, ACCU_FD8, ACCU_FDD8, ACCU_ORTEGA, ACCU_ZHAN};
const int LS_BUFFERS[NUMBER_OF_FLOW_ALGORITHMS] 				= {LS_ANISOTROPIC, LS_D8, LS_FD8, LS_FDD8, LS_ORTEGA, LS_ZHAN};
const int RUSLE_BUFFERS[NUMBER_OF_FLOW_ALGORITHMS] 				= {RUSLE_ANISOTROPIC, RUSLE_D8, RUSLE_FD8, RUSLE_FDD8, RUSLE_ORTEGA, RUSLE_ZHAN};

const char *ACCUMULATION_FILENAMES[NUMBER_OF_FLOW_ALGORITHMS]	= {STR_ACCU_ANISO_OUT, STR_ACCU_D8_OUT, STR_ACCU_FD8_OUT, STR_ACCU_FDD8_OUT, STR_ACCU_ORTEGA_OUT, STR_ACCU_ZHAN_OUT};
const char *LS_FILENAMES[NUMBER_OF_FLOW_ALGORITHMS] 			= {STR_LS_ANISO_OUT, STR_LS_D8_OUT, STR_LS_FD8_OUT, STR_LS_FDD8_OUT, STR_LS_ORTEGA_OUT, STR_LS_ZHAN_OUT};
const char *RUSLE_FILENAMES[NUMBER_OF_FLOW_ALGORITHMS]			= {STR_RUSLE_ANISO_OUT, STR_RUSLE_D8_OUT, STR_RUSLE_FD8_OUT, STR_RUSLE_FDD8_OUT, STR_RUSLE_ORTEGA_OUT, STR_RUSLE_ZHAN_OUT};
const char *ALGORITHM_STRINGS[NUMBER_OF_FLOW_ALGORITHMS] 		= {STR_ANISOTROPIC, STR_D8, STR_FD8, STR_FDD8, STR_ORTEGA, STR_ZHAN};

// Global OpenCL Framework
cl_bool performProfiling = false;
cl_context context = NULL;
cl_command_queue queue = NULL;
cl_program program = NULL;
header *head = NULL;
std::string opencl_program_path = KERNEL_LOCATION_DEFAULT;

// Input files
std::string elevation_filename = "";
std::string r_filename = "";
std::string k_filename = "";
std::string ls_filename = "";
std::string c_filename = "";
std::string vector_filename = "";

// Global Preprocessor feedback switches
bool printNormalization = false;
bool printDependencies = false;
bool printDirections = false;
bool printIndegree = false;

// Main functionality
bool performConversion = false;
bool performTestCase = false;
bool performAnisotropic = false;
bool performD8 = false;
bool performFD8 = false;
bool performFDD8 = false;
bool performOrtega = false;
bool performZhan = false;
bool performLS = false;
bool performSlope = false;
bool performTopological = false;
bool performRUSLE = false;
bool performRUSLEFromFile = false;

// Local Algorithm Result feedback switches
bool printAccumulation = false;
bool printAscii = false;
bool printLS = false;
bool printSlope = false;

// OpenCL lists
bool printDevices = false;
bool printPlatforms = false;

void allocateAndCheck(cl_float **buffers, Buffer position, unsigned int size);
cl_float** allocateBuffers(cl_uint size);
void checkArguments(void);
void freeBuffers(cl_float **buffers);
void parseCommandLineArguments(int argv, char * argc[]);
void printBuffers(cl_float** buffers);
void printUsage(void);

// Configuration settings
cl_float fdd8_threshold = THRESHOLD_DEFAULT;
cl_float ls_m = LS_M_DEFAULT;
cl_float ls_n = LS_N_DEFAULT;

cl_float anisotropic_m = ANISOTROPIC_M_DEFAULT;
cl_float anisotropic_q0 = ANISOTROPIC_Q0_DEFAULT;
cl_bool save_sort = SAVE_SORT_DEFAULT;
cl_bool force_sort = false;

bool clComponentChanged = false;
cl_uint platform_number = 0;
cl_uint device_number = 0;

void allocateAndCheck(cl_float **buffers, Buffer position, unsigned int size) {
	// Allocates a buffer and checks it
	buffers[position] = new (std::nothrow) cl_float[size]; // force new to return 0 (NULL) on fail, avoiding the use of exception handling and check for errors the C way

	if (buffers[position] == NULL) {
		std::string error ("Could not allocate buffer nr " + position);
		reportError(error);
	}
}

cl_float** allocateBuffers(cl_uint size) {
	// This function will allocate all the buffers to the size of input file
	// Note: elevation is not a part of these buffers and is not handled here

	// Reads program settings and prints out buffers to file
	cl_float **buffers = new cl_float*[TOTAL_BUFFERS](); // () at end initializes each element to zero, making all unused pointers NULL
	cl_bool performAlgorithms[NUMBER_OF_FLOW_ALGORITHMS] = {performAnisotropic, performD8, performFD8, performFDD8, performOrtega, performZhan};

	if (performSlope) {
		allocateAndCheck(buffers, SLOPE, size);
	}

	for (int algorithm = 0; algorithm < NUMBER_OF_FLOW_ALGORITHMS; algorithm++) {
		if (performAlgorithms[algorithm]) {
			// Accumulation
			allocateAndCheck(buffers, (Buffer)ACCUMULATION_BUFFERS[algorithm], size);

			// LS
			if (performLS) {
				allocateAndCheck(buffers, (Buffer)LS_BUFFERS[algorithm], size);
			}

			// RUSLE
			if (performRUSLE) {
				allocateAndCheck(buffers, (Buffer)RUSLE_BUFFERS[algorithm], size);
			}
		}
	}

	/*// Performed elsewhere
	if (performAnisotropic) {
		allocateAndCheck(buffers, ANISOTROPIC_U, size);
	}
	*/

	return buffers;
}

void checkArguments(void) {
	// Is the combination of Command Line arguments allowed, returns true if yes

	// Check Kernel file location
	std::ifstream kernel_file (opencl_program_path.c_str());
	if (!kernel_file.is_open()) {
		std::cerr << "Could not open OpenCL kernel file at " << opencl_program_path << std::endl;
		std::cerr << "Reverting to default" << std::endl;
		opencl_program_path = KERNEL_LOCATION_DEFAULT;
	}
	kernel_file.close();

	// Just perform test case or print CL components?
	if (performTestCase || printDevices || printPlatforms) {
		return;
	}

	// Are we just setting OpenCL options without running anything?
	if (elevation_filename == "" && clComponentChanged) {
		exit(EXIT_SUCCESS);
	}

	// RUSLE requires necessary input files
	if (performRUSLE && (r_filename == "" || k_filename == "" || c_filename == "")) {
		reportError("Not enough data files for RUSLE");
	}

	// RUSLE requires LS in some form
	if (performRUSLE && !performRUSLEFromFile && ls_filename == "" && elevation_filename == "") {
		reportError("Cannot compute RUSLE without either LS file or computing LS using elevation file");
	}

	// Input file specified?
	if (!performRUSLEFromFile && elevation_filename == "") {
		reportError("No elevation file specified");
	}

	// Make sure the program has something to do
	if (!(performD8 || performFD8 || performFDD8 || performSlope || performOrtega || performZhan || performRUSLE || performConversion || performAnisotropic)) {
		reportError("No algorithm specified");
	}

	// LS cannot be performed without accumulation
	if ((performLS) && !(performD8 || performFD8 || performFDD8 || performOrtega || performZhan || performAnisotropic)) {
		reportError("LS specified but no flow algorithm given");
	}

	// Ortega and Zhan only supported for flow transfer
	if (performTopological && (performOrtega || performZhan)) {
		reportError("Ortega and Zhan cannot be used with topological");
	}

	// Has an algorithm been specified but no output to produce?
	if ((performD8 || performFD8 || performFDD8 || performOrtega || performZhan || performAnisotropic) && !(performLS || printDependencies || printDirections || printNormalization || printAccumulation)) {
		reportError("Flow algorithm specified but no use for flow algorithm");
	}

	// normalization are only computed with FD8, FDD8 and Zhan
	if (printNormalization && !(performFD8 || performFDD8 || performZhan || performAnisotropic)) {
		reportError("Normalization is only computed with FD8, FDD8 or Anisotropic");
	}

	// Only Zhan Algorithm uses Indegree
	if (printIndegree && !performZhan) {
		reportError("Indegrees are only computed for Zhan FD8");
	}

	if (performAnisotropic && vector_filename == "") {
		reportError("Anisotropic Flow Algorithm specified but no vector data specified");
	}

	// Check setting values
	std::stringstream range;
	if (performFDD8 && (fdd8_threshold < FDD8_THRESHOLD_MIN || fdd8_threshold > FDD8_THRESHOLD_MAX)) {
		range << " (" << FDD8_THRESHOLD_MIN << "-" << FDD8_THRESHOLD_MAX << ")";
		reportError("FDD8 Threshold is outside valid range" + range.str());
	}

	if (performLS && (ls_m < LS_M_MIN || ls_m > LS_M_MAX)) {
		range << " (" << LS_M_MIN << "-" << LS_M_MAX << ")";
		reportError("LS M factor is outside valid range" + range.str());
	}

	if (performLS && (ls_n < LS_N_MIN || ls_n > LS_N_MAX)) {
		range << " (" << LS_N_MIN << "-" << LS_N_MAX << ")";
		reportError("LS N factor is outside valid range" + range.str());
	}

	if (performAnisotropic && (anisotropic_m < ANISOTROPIC_M_MIN || anisotropic_m > ANISOTROPIC_M_MAX)) {
		range << " (" << ANISOTROPIC_M_MIN << "-" << ANISOTROPIC_M_MAX << ")";
		reportError("Anisotropic M factor is outside valid range" + range.str());
	}

	if (performAnisotropic && (anisotropic_q0 < ANISOTROPIC_Q0_MIN || anisotropic_q0 > ANISOTROPIC_Q0_MAX)) {
		range << " (" << ANISOTROPIC_Q0_MIN << "-" << ANISOTROPIC_Q0_MAX << ")";
		reportError("Anisotropic Q0 factor is outside valid range" + range.str());
	}

	// Check File extensions
	checkFileExtension(elevation_filename);
	checkFileExtension(r_filename);
	checkFileExtension(k_filename);
	checkFileExtension(ls_filename);
	checkFileExtension(c_filename);
	checkFileExtension(vector_filename);
}

void freeBuffers(cl_float **buffers) {
	// This function will deallocate all buffers allocated with allocateMemory()
	// Note elevation is not deallocated here, only result buffers for accumulation, slope and LS

	for (int algorithm = FIRST_BUFFER; algorithm < LAST_BUFFER; algorithm++) {
		if (buffers[algorithm] != NULL) {
			delete [] buffers[algorithm];
		}
	}

	delete [] buffers;
}

int main(int argv, char *argc[]) {

	// Start timer
	time_t main_start = time(NULL);

	// Read configuration file
	int read_error = readConfigFile(&platform_number, &device_number, &anisotropic_m, &anisotropic_q0, &ls_m, &ls_n, &fdd8_threshold, &save_sort, &opencl_program_path);
	if (read_error) {
		std::cout << "Could not read configuration file, creating it with default values" << std::endl;
		int write_error = writeConfigFile(PLATFORM_DEFAULT, DEVICE_DEFAULT, ANISOTROPIC_M_DEFAULT, ANISOTROPIC_Q0_DEFAULT, LS_M_DEFAULT, LS_N_DEFAULT, THRESHOLD_DEFAULT, SAVE_SORT_DEFAULT, KERNEL_LOCATION_DEFAULT);
		if (write_error) {
			std::cout << "Could not create configuration file. Default values are used unless set by command line" << std::endl;
			// Undoing possible read configuration errors
			platform_number = PLATFORM_DEFAULT;
			device_number = DEVICE_DEFAULT;
			ls_m = LS_M_DEFAULT;
			ls_n = LS_N_DEFAULT;
			fdd8_threshold = THRESHOLD_DEFAULT;
			anisotropic_m = ANISOTROPIC_M_DEFAULT;
			anisotropic_q0 = ANISOTROPIC_Q0_DEFAULT;
			opencl_program_path = KERNEL_LOCATION_DEFAULT;
		}
	}

	// Check command line
	parseCommandLineArguments(argv, argc);
	checkArguments();

	// List OpenCL options if user commands
	if (printPlatforms) {
		listPlatforms();
		releaseOpenCL();
		exit(EXIT_SUCCESS);
	}

	if (printDevices) {
		listDevices(platform_number);
		releaseOpenCL();
		exit(EXIT_SUCCESS);
	}

	// Convert file from one form to another
	if (performConversion) {
		convertDEMFileType(elevation_filename);
		exit(EXIT_SUCCESS);
	}

	// Set up everything in relation to OpenCL
	setupOpenCL(platform_number, device_number);

	// Run a small testcase then exit
	if (performTestCase) {
		runOpenCLTestCase();
		releaseOpenCL();
		exit(EXIT_SUCCESS);
	}

	// If perform RUSLE with LS from file, SPECIAL CASE
	if (performRUSLEFromFile) {
		setupRUSLE(r_filename, k_filename, ls_filename, c_filename);
		header *ls_header = getLSFileHeader();
		cl_uint ls_file_total_cells = ls_header->rows * ls_header->columns;
		cl_float *rusle_buffer = new cl_float[ls_file_total_cells];

		std::string source("LS_FILE");
		RUSLE(NULL, rusle_buffer, source);

		std::string extension = ".";
		extension.append((printAscii) ? STR_ASCII_EXTENSION : STR_BINARY_EXTENSION);
		std::string filename = STR_RUSLE_FILE_OUT + extension;
		writeDEMToFile(filename, rusle_buffer, ls_header, false);

		freeRUSLE();
		delete[] rusle_buffer;
		exit(EXIT_SUCCESS);
	}

	// Read in elevation data
	header file_header = {};
	head = &file_header;
	cl_float *elevation_data = NULL;
	readDEMFromFile(elevation_filename, &elevation_data);

	cl_uint total_elements = head->rows * head->columns;
	cl_float **buffers = allocateBuffers(total_elements);
	cl_bool performAlgorithms[NUMBER_OF_FLOW_ALGORITHMS] = {performAnisotropic, performD8, performFD8, performFDD8, performOrtega, performZhan};

	// Run algorithms

	if (performSlope) {
		slope(elevation_data, buffers[SLOPE]);
	}

	// Anisotropic flow algorithm requires vector (U) parameter from file
	cl_float *vector_data = NULL;
	if (performAnisotropic) {
		header vector_header = {};
		cl_float *vectors = NULL;

		readDEMFromFile(vector_filename, &vectors, &vector_header);
		vector_data = convertDEMData(vectors, *head, vector_header);
		normalizeVectors(vector_data, head);
	}

	// Flow Accumulation
	if (performTopological) {
		// Topological algorithms
		topologicalAccumulation(elevation_data, buffers[ANISOTROPIC], buffers[D8], buffers[FD8], buffers[FDD8], fdd8_threshold, anisotropic_m, anisotropic_q0, vector_data);
	} else {
		// Transfer algorithms
		if (performAnisotropic) {
			flowTransferAnisotropic(elevation_data, anisotropic_m, anisotropic_q0, vector_data, buffers[ANISOTROPIC]);
		}

		if (performD8) {
			flowTransferD8(elevation_data, buffers[D8]);
		}

		if (performFD8) {
			flowTransferFD8(elevation_data, buffers[FD8]);
		}

		if (performFDD8) {
			flowTransferFDD8(elevation_data, fdd8_threshold, buffers[FDD8]);
		}

		if (performOrtega) {
			ortegaRueda(elevation_data, buffers[ORTEGA]);
		}

		if (performZhan) {
			zhanQin(elevation_data, buffers[ZHAN]);
		}
	}

	// LS Factor
	if (performLS) {
		for (int algorithm = FIRST_ALGORITHM; algorithm < LAST_ALGORITHM; algorithm++) {
			if (performAlgorithms[algorithm]) {
				int current_accu = ACCUMULATION_BUFFERS[algorithm];
				int current_ls = LS_BUFFERS[current_accu];

				LS(buffers[current_accu], buffers[SLOPE], ls_m, ls_n, buffers[current_ls], ALGORITHM_STRINGS[current_accu]);
			}
		}
	}

	// RUSLE
	if (performRUSLE) {
		setupRUSLE(r_filename, k_filename, ls_filename, c_filename);

		for (int algorithm = FIRST_ALGORITHM; algorithm < LAST_ALGORITHM; algorithm++) {
			if (performAlgorithms[algorithm]) {
				int current_ls = LS_BUFFERS[algorithm];
				int current_rusle = RUSLE_BUFFERS[algorithm];

				RUSLE(buffers[current_ls], buffers[current_rusle], ALGORITHM_STRINGS[algorithm]);
			}
		}

		freeRUSLE();
	}

	// Write out data to files
	printBuffers(buffers);

	// Release resources
	freeBuffers(buffers);
	free(elevation_data);
	if (performAnisotropic) {
		free(vector_data);
	}
	releaseOpenCL();

	time_t main_stop = time(NULL);
	double main_total_time = (double)(main_stop - main_start);
	std::cout << std::endl << "Program completed successfully" << std::endl;
	std::cout << "Total program execution time: " << main_total_time << std::endl;
}

void parseCommandLineArguments(int argv, char *argc[]) {
	if (argv < 2) {
		printUsage();
	}

	// Configuration options
	cl_int new_platform_number = -1;
	cl_int new_device_number = -1;
	cl_float new_ls_m = -1.0f;
	cl_float new_ls_n = -1.0f;
	cl_float new_threshold = -1.0f;
	cl_float new_anisotropic_m = -1.0f;
	cl_float new_anisotropic_q0 = -1.0f;
	cl_bool new_save_sort = save_sort;
	std::string new_kernel_location = "";

	// Check each command
	for (int i = 1; i < argv; i++) {
		std::string argument(argc[i]);

		if (argument == "accumulation") {
			printAccumulation = true;
		} else

		if (argument == "anisotropic") {
			performAnisotropic = true;
		} else

		if (argument.compare(0, 4, "a_m=") == 0) {
			std::string value = argument.substr(4);
			new_anisotropic_m = (cl_float) atof(value.c_str());
		} else

		if (argument.compare(0, 5, "a_q0=") == 0) {
			std::string value = argument.substr(5);
			new_anisotropic_q0 = (cl_float) atof(value.c_str());
		} else

		if (argument == "ascii") {
			printAscii = true;
		} else

		if (argument.compare(0, 2, "c=") == 0) {
			c_filename = argument.substr(2);
		} else

		if (argument == "normalization") {
			printNormalization = true;
		} else

		if (argument == "convert") {
			performConversion = true;
		} else

		if (argument == "d8") {
			performD8 = true;
		} else

		if (argument.compare(0, 7, "device=") == 0) {
			std::string value = argument.substr(7);
			new_device_number = atoi(value.c_str());
		} else

		if (argument == "directions") {
			printDirections = true;
		} else

		if (argument == "dependencies") {
			printDependencies = true;
		} else

		if (argument == "fd8") {
			performFD8 = true;
		} else

		if (argument == "fdd8") {
			performFDD8 = true;
		} else

		if (argument == "forcesort") {
			force_sort = true;
		} else

		if (argument == "indegree") {
			printIndegree = true;
		} else

		if (argument.compare(0, 6, "input=") == 0) {
			elevation_filename = argument.substr(6);
		} else

		if (argument.compare(0, 2, "k=") == 0) {
			k_filename = argument.substr(2);
		} else

		if (argument.compare(0, 7, "kernel=") == 0) {
			std::string value = argument.substr(7);
			new_kernel_location = value;
		} else

		if (argument == "listplatforms") {
			printPlatforms = true;
		} else

		if (argument == "listdevices") {
			printDevices = true;
		} else

		if (argument == "ls") {
			performSlope = true;
			performLS = true;
			printLS = true;
		} else

		if (argument.compare(0, 3, "ls=") == 0) {
			ls_filename = argument.substr(3);
		} else

		if (argument.compare(0, 5, "ls_m=") == 0) {
			std::string value = argument.substr(5);
			new_ls_m = (cl_float) atof(value.c_str());
		} else

		if (argument.compare(0, 5, "ls_n=") == 0) {
			std::string value = argument.substr(5);
			new_ls_n = (cl_float) atof(value.c_str());
		} else

		if (argument == "ortega") {
			performOrtega = true;
		} else

		if (argument.compare(0, 9, "platform=") == 0) {
			std::string value = argument.substr(9);
			new_platform_number = atoi(value.c_str());
		} else

		if (argument == "profile") {
			performProfiling = true;
		} else

		if (argument.compare(0, 2, "r=") == 0) {
			r_filename = argument.substr(2);
		} else

		if (argument == "rusle") {
			performRUSLE = true;
			performLS = true;
			performSlope = true;
		} else

		if (argument.compare(0, 9, "savesort=") == 0) {
			std::string value = argument.substr(9);
			int save_sort_argument = atoi(value.c_str());
			new_save_sort = (save_sort_argument == 1);
		} else

		if (argument == "slope") {
			performSlope = true;
			printSlope = true;
		} else

		if (argument == "testcase") {
			performTestCase = true;
		} else

		if (argument.compare(0, 10, "threshold=") == 0) {
			std::string value = argument.substr(10);
			new_threshold = (cl_float) atof(value.c_str());
		} else

		if (argument.compare(0, 8, "vectors=") == 0) {
			vector_filename = argument.substr(8);
		} else

		if (argument == "topological") {
			performTopological = true;
		} else

		if (argument == "zhan") {
			performZhan = true;
		} else

		// Could not recognise command
		{
			std::cerr << "Unknown command: " << argument << std::endl;
			printUsage();
		}
	}

	// Have numerical values changed?
	bool changed_values = false;
	if (new_platform_number >= 0) {
		platform_number = (cl_uint) new_platform_number;
		changed_values = true;
	}

	if (new_device_number >= 0) {
		device_number = (cl_uint) new_device_number;
		changed_values = true;
	}

	if (new_ls_m >= 0.0f) {
		ls_m = new_ls_m;
		changed_values = true;
	}

	if (new_ls_n >= 0.0f) {
		ls_n = new_ls_n;
		changed_values = true;
	}

	if (new_threshold >= 0.0f) {
		fdd8_threshold = new_threshold;
		changed_values = true;
	}

	if (new_anisotropic_m >= 0.0f) {
		anisotropic_m = new_anisotropic_m;
		changed_values = true;
	}

	if (new_anisotropic_q0 >= 0.0f) {
		anisotropic_q0 = new_anisotropic_q0;
		changed_values = true;
	}

	bool new_saved_sort_setting = false;
	if (new_save_sort != save_sort) {
		new_saved_sort_setting = true;
		save_sort = new_save_sort;

		changed_values = true;
	}

	if (new_kernel_location != "") {
		opencl_program_path = new_kernel_location;
		changed_values = true;
	}

	// If so, save them
	if (changed_values) {
		clComponentChanged = true;
		int write_error = writeConfigFile(platform_number, device_number, anisotropic_m, anisotropic_q0, ls_m, ls_n, fdd8_threshold, save_sort, opencl_program_path);
		if (write_error) {
			reportError("Could not write to configuration file");
		} else {
			if (new_platform_number >= 0) {
				std::cout << "Saved Platform " << platform_number << std::endl;
			}

			if (new_device_number >= 0) {
				std::cout << "Saved Device " << device_number << std::endl;
			}

			if (new_ls_m >= 0.0f) {
				std::cout << "Saved LS M Factor " << ls_m << std::endl;
			}

			if (new_ls_n >= 0.0f) {
				std::cout << "Saved LS N Factor " << ls_n << std::endl;
			}

			if (new_threshold >= 0.0f) {
				std::cout << "Saved Threshold " << fdd8_threshold << std::endl;
			}

			if (new_anisotropic_m >= 0.0f) {
				std::cout << "Saved Anisotropic M Factor " << anisotropic_m << std::endl;
			}

			if (new_anisotropic_q0 >= 0.0f) {
				std::cout << "Saved Anisotropic Q0 Factor " << anisotropic_q0 << std::endl;
			}

			if (new_saved_sort_setting) {
				std::cout << "Saved Sort setting " << ((save_sort) ? "TRUE" : "FALSE") << std::endl;
			}

			if (new_kernel_location != "") {
				std::cout << "Saved Kernels location " << opencl_program_path << std::endl;
 			}
		}
	}

	if (performRUSLE && ls_filename != "") {
		performRUSLEFromFile = true;
	}

	// RUSLE from file overrides all other algorithms and print commands
	if (performRUSLEFromFile || performConversion) {
		printDirections = false;
		printDependencies = false;
		printNormalization = false;
		printIndegree = false;

		performAnisotropic = false;
		performD8 = false;
		performFD8 = false;
		performFDD8 = false;
		performOrtega = false;
		performZhan = false;
		performLS = false;
		performSlope = false;
		performTopological = false;

		printAccumulation = false;
		printSlope = false;
		printLS = false;
	}

	// Convert overrides all that and RUSLE
	if (performConversion) {
		performRUSLE = false;
		performRUSLEFromFile = false;
	}
}

void printUsage(void) {
	std::cout << "\nLSCL - LS Factor OpenCL\n" << "Commands should be separated by space and are case-sensitive\n\n";

	std::cout << "File selection:\n";
	std::cout << "input=<file_name>\tThe program will read ARC/ASCII data from this file.\n";

	std::cout << "\nFlow Algorithms:\n";

	std::cout << "anisotropic\t\tAnisotropic will be used by the other commands.\n";
	std::cout << "a_m=<value>\t\tSet Anisotropic M exponent. Default 1.0\n";
	std::cout << "a_q0=<value>\t\tSet Anisotropic Q0 exponent. Default 1.0\n";
	std::cout << "vectors=<file_name>\tVector Directions will be read from this file\n";

	std::cout << "\nd8\t\t\tD8 will be used by the other commands.\n";

	std::cout << "\nfd8\t\t\tFD8 will be used by the other commands.\n";

	std::cout << "\nfdd8\t\t\tFDD8 will be used by the other commands.\n";
	std::cout << "threshold=<value>\tSet Threshold for switch from FD8 to D8. \n\t\t\tDefault: 250.0\n";

	std::cout << "\nTopological Sorting supported for D8, FD8, FDD8 & Anisotropic Flow Algorithms\n";
	std::cout << "topological \t\tEnable topological sorting\n";
	std::cout << "savesort=<value>\tTopological sort will be saved to disk Default: off\n\t\t\tsavesort=1 turns on saving\n";
	std::cout << "forcesort\t\tDEM will topologically sorted and ignore files\n";

	std::cout << "\nRUSLE\n";
	std::cout << "ls\t\t\tLS Factor will be written to file.\n";
	std::cout << "ls_m=<value>\t\tSet LS M exponent. Default: 0.6\n";
	std::cout << "ls_n=<value>\t\tSet LS N exponent. Default: 1.3\n";

	std::cout << "\nrusle\t\t\tRUSLE will be written to file.\n";
	std::cout << "c=<file_name>\t\tC Factor will be read from this file.\n";
	std::cout << "k=<file_name>\t\tK Factor will be read from this file.\n";
	std::cout << "ls=<file_name>\t\tLS Factor will be read from this file.\n";
	std::cout << "r=<file_name>\t\tR Factor will be read from this file.\n";

	std::cout << "\nOpenCL Settings:\n";
	std::cout << "device=<value>\t\tSet device used by program. Default: 0\n";
	std::cout << "listdevices\t\tThe program will show a numbered list of available \n\t\t\tOpenCL devices belonging to selected platform.\n";
	std::cout << "listplatforms\t\tThe program will show a numbered list of available \n\t\t\tOpenCL platforms.\n";
	std::cout << "platform=<value>\tSet platform used by program. Default 0\n";

	std::cout << "\nOther Options:\n";
	std::cout << "accumulation\t\tAccumulation will be written to file.\n";
	std::cout << "ascii\t\t\tOutput files will be in ArcAscii format.\n";
	std::cout << "normalization\t\tNormalization will be written to file.\n";
	std::cout << "convert\t\t\tInput file will be converted from ascii to binary \n\t\t\tor vice versa.\n";
	std::cout << "dependencies\t\tDependencies will be written to file.\n";
	std::cout << "directions\t\tDirections will be written to file.\n";
	std::cout << "indegree\t\tIndegrees used by Zhan/Qin will be written to file.\n";
	std::cout << "ortega\t\t\tOrtega/Rueda Algorithm D8 will be used by the \n\t\t\tother commands.\n";
	std::cout << "profile\t\t\tAll kernels will be profiled with MS precision.\n";
	std::cout << "slope\t\t\tSlope will be calculated and written to file.\n";
	std::cout << "testcase\t\tRun a small testcase. Overrides other options.\n";
	std::cout << "zhan\t\t\tZhan/Qin Algorithm FD8 will be used by the \n\t\t\tother commands.\n";

	exit(EXIT_SUCCESS);
}

void printBuffers(cl_float** buffers) {

	// Reads program settings and prints out buffers to file
	cl_bool performAlgorithms[NUMBER_OF_FLOW_ALGORITHMS] = {performAnisotropic, performD8, performFD8, performFDD8, performOrtega, performZhan};

	// extension controls if data will be written as ASCII or binary
	std::string extension = ".";
	extension.append((printAscii) ? STR_ASCII_EXTENSION : STR_BINARY_EXTENSION);
	std::string filename;

	// Slope
	if (printSlope) {
		filename = STR_SLOPE_OUT + extension;
		writeSlopeToFileAsDegrees(filename, buffers[SLOPE]);
	}

	// Flow Accumulation Related
	for (int algorithm = 0; algorithm < NUMBER_OF_FLOW_ALGORITHMS; algorithm++) {
		if (performAlgorithms[algorithm]) {
			// Accumulation

			if (printAccumulation) {
				filename = ACCUMULATION_FILENAMES[algorithm] + extension;
				writeDEMToFile(filename, buffers[ACCUMULATION_BUFFERS[algorithm]]);
			}

			// LS
			if (printLS) {
				filename = LS_FILENAMES[algorithm] + extension;
				writeDEMToFile(filename, buffers[LS_BUFFERS[algorithm]]);
			}

			// RUSLE
			if (performRUSLE) {
				filename = RUSLE_FILENAMES[algorithm] + extension;
				writeDEMToFile(filename, buffers[RUSLE_BUFFERS[algorithm]]);
			}
		}
	}
}
