/*
 * print.cpp
 *
 *  Created on: 4 Jun 2013
 *      Author: josten
 */

#include <cstdio>

#include "common.hpp"

void printUnsignedCharGrid(unsigned char *unsigned_char_grid, unsigned int total_rows, unsigned int total_columns) {
	// Prints Flow directions
	printf("\n");

	for (unsigned int row = 0; row < total_rows; row++) {
		for (unsigned int column = 0; column < total_columns; column++) {
			unsigned int index = row * total_columns + column;

			printf("%u ", (unsigned int) unsigned_char_grid[index]);
		}
		printf("\n");
	}
}

void printFloatGrid(float *float_grid, unsigned int total_rows, unsigned int total_columns) {
	// Prints Flow Accumulations
	printf("\n");

	for (unsigned int row = 0; row < total_rows; row++) {
		for (unsigned int column = 0; column < total_columns; column++) {
			unsigned int index = row * total_columns + column;
			printf("%2.1f ", float_grid[index]);
		}
		printf("\n");
	}
}

void printFloatGridWithoutEdge(float *float_grid, unsigned int total_rows, unsigned int total_columns) {
	printf("\n");

	for (unsigned int row = 0; row < total_rows; row++) {
		for (unsigned int column = 0; column < total_columns; column++) {
			if (onEdge(row, column)) {
				continue;
			}

			unsigned int index = row * total_columns + column;
			printf("%2.1f ", float_grid[index]);
		}
		printf("\n");
	}
}
