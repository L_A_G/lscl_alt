/*
 * print.hpp
 *
 *  Created on: 3 Aug 2013
 *      Author: josten
 */

#ifndef PRINT_HPP_
#define PRINT_HPP_

void printUnsignedCharGrid(unsigned char *unsigned_char_grid, unsigned int total_rows, unsigned int total_columns);
void printFloatGrid(float *float_grid, unsigned int total_rows, unsigned int total_columns);
void printFloatGridWithoutEdge(float *float_grid, unsigned int total_rows, unsigned int total_columns);

#endif /* PRINT_HPP_ */
