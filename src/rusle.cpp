/*
 * rusle.cpp
 *
 *  Created on: 30 Aug 2013
 *      Author: josten
 */
#include <ctime>
#include <iostream>
#include <string>
#include <CL/cl.h>

#include "common.hpp"
#include "error.hpp"
#include "header.hpp"
#include "io.hpp"
#include "kernel_size.hpp"
#include "setup.hpp"
#include "strings.hpp"

extern header *head;

extern cl_bool performProfiling;
extern cl_program program;
extern cl_command_queue queue;
extern cl_context context;

extern size_t max_global_size;
extern size_t max_local_size;

// File-scope globals
cl_float *converted_R = NULL;
cl_float *converted_K = NULL;
cl_float *converted_C = NULL;
cl_float *file_LS = NULL;
header file_LS_header = {0};

header* getLSFileHeader() {
	// This function should only be executed after setupRUSLE has been executed
	// it is used to tell main how big buffer to allocate for LS file and how to write the data out to file

	return &file_LS_header;
}

void RUSLE(cl_float* computed_LS, cl_float *RUSLE_results, std::string algorithm) {
	// This function computes RUSLE given input files,
	// LS can be given as either a pre-computed ArcASCII file made using this program or a new computation

	// This function assumes R, K, LS, C and Results are of equal size
	// The RUSLE values are placed in result
	std::cout << std::endl << "Starting RUSLE for " << algorithm << std::endl;

	if (computed_LS == NULL && file_LS == NULL) {
		reportError("No LS data for RUSLE");
	}

	// Use LS from computation or file?
	cl_float *LS_to_use = (file_LS) ? file_LS : computed_LS;
	header header_to_use = (file_LS) ? file_LS_header : *head;
	cl_float LS_no_data_value = header_to_use.no_data_value;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Measure time
	time_t start = time(0);

	// Set work group sizes
	cl_uint total_size = header_to_use.rows * header_to_use.columns;

	size_t global_work_size_1dim[1] = {computeTotalSizeWithPadding(total_size)};
	size_t local_work_size_1dim[1] = {max_global_size};

	cl_int errors[7];
	std::string source("RUSLE ");

	// Create kernels
	cl_kernel rusle_kernel = clCreateKernel(program, STR_KERNEL_RUSLE, &errors[0]);
	cl_kernel fill_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT, &errors[1]);
	checkForCLErrors(errors, 2, source + STR_KERNELS);

	// Create memory objects
	size_t float_buffer_size = total_size * sizeof(cl_float);
	cl_mem R_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, converted_R, &errors[0]);
	cl_mem K_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, converted_K, &errors[1]);
	cl_mem LS_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, LS_to_use, &errors[2]);
	cl_mem C_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, converted_C, &errors[3]);
	cl_mem result_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[4]);
	checkForCLErrors(errors, 5, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	cl_float zero_pattern = 0.0f;
	setFillFloatKernelArgs(fill_kernel, result_buffer, total_size, zero_pattern, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);
	profiling_events[PROFILING_SETUP_STOP] = profiling_events[PROFILING_SETUP_START];
	checkForCLErrors(errors, 1, source + STR_SETUP);

	// Calculate RUSLE
	errors[0] = clSetKernelArg(rusle_kernel, 0, sizeof(cl_mem), &R_buffer);
	errors[1] = clSetKernelArg(rusle_kernel, 1, sizeof(cl_mem), &K_buffer);
	errors[2] = clSetKernelArg(rusle_kernel, 2, sizeof(cl_mem), &LS_buffer);
	errors[3] = clSetKernelArg(rusle_kernel, 3, sizeof(cl_mem), &C_buffer);
	errors[4] = clSetKernelArg(rusle_kernel, 4, sizeof(cl_uint), &total_size);
	errors[5] = clSetKernelArg(rusle_kernel, 5, sizeof(cl_float), &LS_no_data_value);
	errors[6] = clSetKernelArg(rusle_kernel, 6, sizeof(cl_mem), &result_buffer);
	checkForCLErrors(errors, 7, source + STR_ARGUMENTS);

	errors[0] = clEnqueueNDRangeKernel(queue, rusle_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, &profiling_events[PROFILING_EXECUTION_START]);
	errors[1] = clEnqueueReadBuffer(queue, result_buffer, CL_TRUE, 0, float_buffer_size, RUSLE_results, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[2] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 3, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "RUSLE: " << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(result_buffer);
	errors[1] = clReleaseMemObject(C_buffer);
	errors[2] = clReleaseMemObject(LS_buffer);
	errors[3] = clReleaseMemObject(K_buffer);
	errors[4] = clReleaseMemObject(R_buffer);
	checkForCLErrors(errors, 5, source + STR_CLEANUP + " " + STR_BUFFERS);

	errors[0] = clReleaseKernel(rusle_kernel);
	errors[1] = clReleaseKernel(fill_kernel);
	checkForCLErrors(errors, 2, source + STR_CLEANUP + " " + STR_KERNELS);

}

void setupRUSLE(std::string filename_R, std::string filename_K, std::string filename_LS, std::string filename_C) {
	// This function reads the input files and converts them to appropriate format for GPU

	cl_float *R;
	cl_float *K;
	cl_float *C;
	header R_header;
	header K_header;
	header C_header;
	header LS_header;

	// Read in LS
	cl_bool from_file = (filename_LS != "");
	if (from_file) {
		readDEMFromFile(filename_LS, &file_LS, &LS_header);
		file_LS_header = LS_header;
	} else {
		LS_header = *head;
	}

	// Read in the rest of the factors
	readDEMFromFile(filename_R, &R, &R_header);
	readDEMFromFile(filename_K, &K, &K_header);
	readDEMFromFile(filename_C, &C, &C_header);

	// Convert each factor to same size as LS, these are file-scope globals
	converted_R = convertDEMData(R, LS_header, R_header);
	converted_K = convertDEMData(K, LS_header, K_header);
	converted_C = convertDEMData(C, LS_header, C_header);

	free(R);
	free(K);
	free(C);
}

void freeRUSLE() {
	if (file_LS != NULL) {
		free(file_LS);
	}

	delete [] converted_R;
	delete [] converted_K;
	delete [] converted_C;

	converted_R = NULL;
	converted_K = NULL;
	converted_C = NULL;
	file_LS = NULL;
	file_LS_header.rows = 0;
	file_LS_header.columns = 0;
	file_LS_header.xll_corner = 0;
	file_LS_header.yll_corner = 0;
	file_LS_header.resolution = 0;
	file_LS_header.no_data_value = 0;
}
