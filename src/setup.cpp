/*
 * setup.cpp
 *
 *  Created on: 31 Jul 2013
 *      Author: josten
 */

#include <cmath>
#include <iostream>

#include "CL/cl.h"

#include "error.hpp"
#include "setup.hpp"

extern cl_bool performProfiling;
extern cl_command_queue queue;
extern cl_context context;
extern cl_program program;
extern std::string opencl_program_path;

size_t max_local_size = 0;
size_t max_global_size = 0;

cl_uint total_platforms = 0;
cl_platform_id* platforms = NULL;

const char* deviceTypeToString(cl_device_type device);
void displayDeviceInfo(cl_device_id device_id);
void displayPlatformInfo(cl_platform_id id, cl_platform_info name, std::string str);
cl_uint getNumberOfDevicesForPlatforms(cl_platform_id platform);
void readInPlatforms(void);

const char* deviceTypeToString(cl_device_type device) {

	const char * type;

	switch (device) {
	case CL_DEVICE_TYPE_CPU:
		type = "CPU";
		break;
	case CL_DEVICE_TYPE_GPU:
		type = "GPU";
		break;
	case CL_DEVICE_TYPE_ACCELERATOR:
		type = "ACCELERATOR";
		break;
	case CL_DEVICE_TYPE_DEFAULT:
		type = "Default";
		break;
	default:
		type = "Default";
		std::cerr << "Could not determine type of device" << std::endl;
		break;
	}

	return type;
}

void displayDeviceInfo(cl_device_id device_id) {

	std::string source = "displayDeviceInfo ";
	cl_int errors[6] = {};

	// Name
	size_t paramValueSize;
	errors[0] = clGetDeviceInfo(device_id, CL_DEVICE_NAME, 0, NULL, &paramValueSize);
	char *name = (char*) alloca(sizeof(char) * paramValueSize);
	errors[1] = clGetDeviceInfo(device_id, CL_DEVICE_NAME, paramValueSize, name, NULL);

	std::cout << "\tDevice Name: " << name << std::endl;

	// Type
	cl_device_type type;
	errors[2] = clGetDeviceInfo(device_id, CL_DEVICE_TYPE, sizeof(cl_device_type), &type, NULL);

	std::cout << "\tDevice Type: " << deviceTypeToString(type) << std::endl;

	// Global Memory
	cl_ulong memSize;
	errors[3] = clGetDeviceInfo(device_id, CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(memSize), &memSize, NULL);

	std::cout << "\tMemory size: " << memSize << std::endl;

	// Compute Units
	cl_uint compute_units;
	errors[4] = clGetDeviceInfo(device_id, CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(compute_units), &compute_units, NULL);
	std::cout << "\tCompute units: " << compute_units << std::endl;

	// Compiler exists
	cl_bool compiler_exists;
	errors[5] = clGetDeviceInfo(device_id, CL_DEVICE_COMPILER_AVAILABLE, sizeof(compiler_exists), &compiler_exists, NULL);

	checkForCLErrors(errors, 6, source);

	if (compiler_exists) {
		std::cout << "\tCompiler is available" << std::endl;
	} else {
		std::cout << "\tCompiles is not available" << std::endl;
	}
}

void displayPlatformInfo(cl_platform_id id, cl_platform_info name, std::string str) {

	std::string source = "displayPlatformInfo ";
	cl_int errors[2] = {};

	size_t paramValueSize;
	errors[0] = clGetPlatformInfo(id, name, 0, NULL, &paramValueSize);
	char *info = (char *)alloca(sizeof(char) * paramValueSize);
	errors[1] = clGetPlatformInfo(id, name, paramValueSize, info, NULL);
	checkForCLErrors(errors, 2, source);
	std::cout << str.c_str() << ":\t" << info << std::endl;
}

cl_uint getNumberOfDevicesForPlatforms(cl_platform_id platform) {
	cl_int errors[1] = {};
	std::string source = "getNumberOfDevicesForPlatforms";

	cl_uint num_devices = 0;
	errors[0] = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, NULL, &num_devices);
	checkForCLErrors(errors, 1, source);

	return num_devices;
}

void listDevices(cl_uint platform_num) {

	std::string source = "listDevices";
	cl_int errors[1] = {};

	// Have the platforms been read in already?
	if (platforms == NULL) {
		readInPlatforms();
	}

	if (platform_num >= total_platforms) {
		reportError("Platform doesn't exist");
	}

	// Get the devices for chosen platform
	cl_platform_id platform = platforms[platform_num];
	cl_uint total_devices = getNumberOfDevicesForPlatforms(platform);
	cl_device_id *devices = (cl_device_id*) alloca(total_devices * sizeof(cl_device_id));

	errors[0] = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, total_devices, devices, NULL);
	checkForCLErrors(errors, 1, source);

	// Iterate through them
	std::cout << "\tNumber of devices for platform: " << total_devices << std::endl;
	std::cout << "These " << total_devices << " devices were discovered for platform " << platform_num << std::endl;
	for (cl_uint device_num = 0; device_num < total_devices; device_num++) {
		std::cout << device_num << ":" << std::endl;
		cl_device_id device = devices[device_num];

		displayDeviceInfo(device);
	}
}

void listPlatforms(void) {

	// Are platforms read in
	if (platforms == NULL) {
		readInPlatforms();
	}

	// Print the list of platforms displaying associated information
	std::cout << "These " << total_platforms << " platforms were discovered." << std::endl;

	for (cl_uint platform = 0; platform < total_platforms; platform++) {

		cl_platform_id ID = platforms[platform];
		std::cout << platform << ":" << std::endl;

		displayPlatformInfo(ID, CL_PLATFORM_PROFILE, "CL_PLATFORM_PROFILE");
		displayPlatformInfo(ID, CL_PLATFORM_VERSION, "CL_PLATFORM_VERSION");
		displayPlatformInfo(ID, CL_PLATFORM_VENDOR, "CL_PLATFORM_VENDOR");
		displayPlatformInfo(ID, CL_PLATFORM_EXTENSIONS, "CL_PLATFORM_EXTENSIONS");
		std::cout << "Number of devices for platform: " << getNumberOfDevicesForPlatforms(ID) << std::endl;

		std::cout << std::endl;
	}
}

void readInPlatforms(void) {

	std::string source = "readInPlatforms";
	cl_int errors[2];

	if (platforms != NULL) {
		delete [] platforms;
	}

	// Query the total number of platforms
	errors[0] = clGetPlatformIDs(0, NULL, &total_platforms);
	if (total_platforms == 0) {
		reportError("Could not find any OpenCL platforms");
	}

	// Next, allocate memory for the installed platforms, and query to get the list.
	platforms = new cl_platform_id[total_platforms];
	errors[1] = clGetPlatformIDs(total_platforms, platforms, NULL);
	checkForCLErrors(errors, 2, source);
}

void releaseOpenCL(void) {
	if (program != NULL) {
		clReleaseProgram(program);
	}

	if (queue != NULL) {
		clReleaseCommandQueue(queue);
	}

	if (context != NULL) {
		clReleaseContext(context);
	}

	if (platforms != NULL) {
		delete [] platforms;
	}
}

void setupOpenCL(const cl_uint platform_id, const cl_uint device_id) {

	std::string source = "setupOpenCL";
	cl_int errors[8];

	cl_uint platform_choice = platform_id;
	cl_uint device_choice = device_id;

	cl_platform_id platform;
	// Are platforms read in?
	if (platforms == NULL) {
		readInPlatforms();
	}

	// is the platform number larger than number of platforms?
	if (platform_choice >= total_platforms) {
		std::cout << "Platform id doesn't exist, reverting to first platform" << std::endl;
		platform_choice = 0;
	}

	// Pick selected platform
	platform = platforms[platform_choice];

	// is the device number larger than number of devices for the chosen platform?
	cl_uint num_devices = getNumberOfDevicesForPlatforms(platform);
	if (device_choice >= num_devices) {
		std::cout << "Device id doesn't exist, reverting to first device" << std::endl;
		device_choice = 0;
	}

	// Pick selected device
	cl_device_id *devices = (cl_device_id *)alloca(sizeof(cl_device_id) * num_devices);
	errors[0] = clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, num_devices, devices, NULL);
	cl_device_id device = devices[device_choice];

	// Get number of work-items per work_group
	size_t max_work_group_size;
	errors[1] = clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &max_work_group_size, NULL);

	max_local_size = (size_t) sqrt(max_work_group_size);
	max_global_size = max_work_group_size;

	// Print out platform and device name
	size_t paramValueSize;
	errors[2] = clGetPlatformInfo(platform, CL_PLATFORM_NAME, 0, NULL, &paramValueSize);
	char *platform_name = (char *)alloca(paramValueSize);
	errors[3] = clGetPlatformInfo(platform, CL_PLATFORM_NAME, paramValueSize, platform_name, NULL);
	std::cout << "Platform:\t" << platform_name << std::endl;

	errors[4] = clGetDeviceInfo(device, CL_DEVICE_NAME, 0, NULL, &paramValueSize);
	char *device_name = (char*) alloca(sizeof(char) * paramValueSize);
	errors[5] = clGetDeviceInfo(device, CL_DEVICE_NAME, paramValueSize, device_name, NULL);
	std::cout << "Device:  \t" << device_name << std::endl;

	// Create context
	cl_context_properties properties[] = {CL_CONTEXT_PLATFORM, (cl_context_properties)platform, 0};
	context = clCreateContext(properties, 1, &device, NULL, NULL, &errors[6]);

	// Create command queue
	cl_command_queue_properties queue_properties = 0;
	if (performProfiling) {
		queue_properties = CL_QUEUE_PROFILING_ENABLE;
	}
	queue = clCreateCommandQueue(context, device, queue_properties, &errors[7]);
	checkForCLErrors(errors, 8, source);

	// Create and compile OpenCL program
	program = buildProgram(opencl_program_path, context, device);
}
