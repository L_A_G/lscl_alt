/*
 * setup.hpp
 *
 *  Created on: 3 Aug 2013
 *      Author: josten
 */

#ifndef SETUP_HPP_
#define SETUP_HPP_

// build_program.cpp
cl_program buildProgram(std::string filename, cl_context context, cl_device_id device);

// setup.cpp
void listDevices(cl_uint platform_num);
void listPlatforms(void);
void setupOpenCL(const cl_uint platform_id, const cl_uint device_id);
void releaseOpenCL(void);

#endif /* SETUP_HPP_ */
