/*
 * slope.cpp
 *
 *  Created on: 1 Aug 2013
 *      Author: josten
 */

#define _USE_MATH_DEFINES
#include <cmath>
#include <ctime>
#include <cstdio>
#include <iostream>
#include <string>
#include "CL/cl.h"

#include "common.hpp"
#include "error.hpp"
#include "header.hpp"
#include "io.hpp"
#include "kernel_size.hpp"
#include "strings.hpp"

// OpenCL Setup
extern cl_bool performProfiling;
extern cl_command_queue queue;
extern cl_context context;
extern cl_program program;
extern header *head;
extern size_t max_local_size;
extern size_t max_global_size;

// Slope will not convert to degrees unless asked to output
void convertSlopesToDegrees(cl_float *radians, cl_float *degrees);
bool onEdge(cl_uint row, cl_uint column);

void slope(cl_float *elevations, cl_float *slopes) {

	std::cout << std::endl << "Starting Slope..." << std::endl;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Set work group sizes
	size_t total_size = head->rows * head->columns;

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];
	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	// Measure time
	time_t start = time(0);

	cl_int errors[6];
	std::string source = "Slope ";

	// Create kernel
	cl_kernel fill_float_edge_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE, &errors[0]);
	cl_kernel slope_kernel = clCreateKernel(program, STR_KERNEL_SLOPE, &errors[1]);
	checkForCLErrors(errors, 2, source + STR_KERNELS);

	// Create buffers
	size_t buffer_size = total_size * sizeof(cl_float);
	cl_mem elevation_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, buffer_size, elevations, &errors[0]);
	cl_mem slope_buffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, buffer_size, NULL, &errors[1]);
	checkForCLErrors(errors, 2, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	// Initialise buffer
	setFillFloatEdgeKernelArgs(fill_float_edge_kernel, slope_buffer, 0.0f, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_float_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);
	checkForCLErrors(errors, 1, source + STR_SETUP);
	profiling_events[PROFILING_SETUP_STOP] = profiling_events[PROFILING_SETUP_START];	// Needed for time computation function

	// Set Slope Arguments
	errors[0] = clSetKernelArg(slope_kernel, 0, sizeof(cl_mem), &elevation_buffer);
	errors[1] = clSetKernelArg(slope_kernel, 1, sizeof(cl_uint), &head->rows);
	errors[2] = clSetKernelArg(slope_kernel, 2, sizeof(cl_uint), &head->columns);
	errors[3] = clSetKernelArg(slope_kernel, 3, sizeof(cl_float), &head->resolution);
	errors[4] = clSetKernelArg(slope_kernel, 4, sizeof(cl_mem), &slope_buffer);
	checkForCLErrors(errors, 5, source + STR_ARGUMENTS);

	// Compute Slope and read back
	errors[0] = clEnqueueNDRangeKernel(queue, slope_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_EXECUTION_START]);
	errors[1] = clEnqueueReadBuffer(queue, slope_buffer, CL_TRUE, 0, buffer_size, slopes, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[2] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 3, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "Slope: " << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(elevation_buffer);
	errors[1] = clReleaseMemObject(slope_buffer);
	errors[2] = clReleaseKernel(fill_float_edge_kernel);
	errors[3] = clReleaseKernel(slope_kernel);
	checkForCLErrors(errors, 4, source + STR_CLEANUP);
}

void convertSlopesToDegrees(cl_float *radians, cl_float *degrees) {
	cl_float radians_to_degrees = 180.0f / (cl_float)M_PI;

	for (cl_uint row = 0; row < head->rows; row++) {
		for (cl_uint column = 0; column < head->columns; column++) {

			cl_uint index = row * head->columns + column;

			if (onEdge(row, column)) {
				degrees[index] = head->no_data_value;
			} else {
				degrees[index] = radians[index] * radians_to_degrees;
			}
		}
	}
}

void writeSlopeToFileAsDegrees(std::string filename, cl_float *slopes) {

	cl_float *degrees = new cl_float[head->rows * head->columns];
	convertSlopesToDegrees(slopes, degrees);
	writeDEMToFile(filename, degrees);
	delete [] degrees;
}
