/*
 * strings.hpp
 *
 * Created on: 31 Jul 2013
 *   Author: josten
 */

#ifndef STRINGS_HPP_
#define STRINGS_HPP_

// Kernels
#define STR_KERNEL_ANISO_ACCU			"flowTransferAnisotropic"
#define STR_KERNEL_ANISO_NORMALIZATION	"computeAnisotropicNormalization"
#define STR_KERNEL_ANISO_TOPO			"topologicalAnisotropic"
#define STR_KERNEL_D8_ACCU 				"flowTransferReadD8"
#define STR_KERNEL_D8_DIRS 				"computeFlowDirectionsD8"
#define STR_KERNEL_D8_TOPO 				"topologicalD8"
#define STR_KERNEL_DEPEND 				"computeDependencies"
#define STR_KERNEL_FD8_ACCU 			"flowTransferReadFD8"
#define STR_KERNEL_FD8_NORMALIZATION 	"computeFD8Normalization"
#define STR_KERNEL_FD8_DIRS 			"computeFlowDirectionsFD8"
#define STR_KERNEL_FD8_TOPO 			"topologicalFD8"
#define STR_KERNEL_FDD8_ACCU 			"flowTransferReadFDD8"
#define STR_KERNEL_FDD8_TOPO 			"topologicalFDD8"
#define STR_KERNEL_FILL_FLOAT 			"fillFloatBuffer"
#define STR_KERNEL_FILL_FLOAT_EDGE 		"fillFloatBufferWithEdge"
#define STR_KERNEL_FILL_UCHAR 			"fillUnsignedCharBuffer"
#define STR_KERNEL_INDEGREE 			"computeIndegree"
#define STR_KERNEL_LS 					"LS"
#define STR_KERNEL_ORTEGA 				"flowTransferReadOrtegaRueda"
#define STR_KERNEL_RUSLE 				"RUSLE"
#define STR_KERNEL_SLOPE 				"slope"
#define STR_KERNEL_ZHANQIN 				"flowTransferZhanQin"

// OpenCL
#define STR_BUILD_LOG					"LSCL-BuildLog.txt"

// Config file
#define STR_SETTINGS_FILENAME 			"cl.cfg"

// Config file Strings
#define STR_CONFIG_ANISO_M				"ANISOTROPIC_M "
#define STR_CONFIG_ANISO_Q0				"ANISOTROPIC_Q0 "
#define STR_CONFIG_DEVICE				"DEVICE "
#define STR_CONFIG_KERNEL_PATH			"KERNEL_PATH "
#define STR_CONFIG_LS_MFACTOR			"LS_M "
#define STR_CONFIG_LS_NFACTOR			"LS_N "
#define STR_CONFIG_PLATFORM 			"PLATFORM "
#define STR_CONFIG_SAVE_SORT			"SAVE_SORT "
#define STR_CONFIG_THRESHOLD			"FDD8_THRESHOLD "

// Input / Output files
#define STR_ASCII_EXTENSION				"asc"
#define STR_BINARY_EXTENSION			"bin"
#define STR_ERROR_EXTENSION				"Could not read file type for "
#define STR_ERROR_READING 				"Could not read from "
#define STR_ERROR_OPENING				"Could not open "
#define STR_ERROR_WRITING 				"Could not write to "
#define STR_READING_FROM				"Reading from "
#define STR_SORT_EXTENSION				"sort"
#define STR_VECTOR_EXTENSION			"vector"
#define STR_WRITING_TO					"Writing to "

#define STR_ANISO_TRANSFER_DIR_OUT		"Dir_Anisotropic.txt"
#define STR_D8_TRANSFER_DIR_OUT 		"Dir_D8.txt"
#define STR_FD8_TRANSFER_DIR_OUT 		"Dir_FD8.txt"
#define STR_FDD8_D8_TRANSFER_DIR_OUT 	"Dir_FDD8_D8.txt"
#define STR_FDD8_FD8_TRANSFER_DIR_OUT 	"Dir_FDD8_FD8.txt"
#define STR_ORTEGA_DIR_OUT 				"Dir_Ortega.txt"
#define STR_ZHAN_DIR_OUT 				"Dir_Zhan.txt"

#define STR_ANISO_TRANSFER_DEP_OUT		"Dep_Anisotropic.txt"
#define STR_D8_TRANSFER_DEP_OUT 		"Dep_D8.txt"
#define STR_FD8_TRANSFER_DEP_OUT		"Dep_FD8.txt"
#define STR_FDD8_D8_TRANSFER_DEP_OUT 	"Dep_FDD8_D8.txt"
#define STR_FDD8_FD8_TRANSFER_DEP_OUT 	"Dep_FDD8_FD8.txt"
#define STR_ORTEGA_DEP_OUT 				"Dep_Ortega.txt"
#define STR_ZHAN_DEP_OUT 				"Dep_Zhan.txt"

#define STR_ZHAN_INDEGREE_OUT 			"Indegree_Zhan.txt"

#define STR_ANISO_TRANSFER_NORM_OUT		"Normalization_Anisotropic.asc"
#define STR_FD8_TRANSFER_NORM_OUT	 	"Normalization_FD8.asc"
#define STR_FDD8_TRANSFER_NORM_OUT	 	"Normalization_FDD8.asc"
#define STR_ZHAN_NORM_OUT 				"Normalization_Zhan.asc"

// These files can be both in binary or ASCII output form
#define STR_ACCU_ANISO_OUT				"Accu_Anisotropic"
#define STR_ACCU_D8_OUT 				"Accu_D8"
#define STR_ACCU_FD8_OUT 				"Accu_FD8"
#define STR_ACCU_FDD8_OUT 				"Accu_FDD8"
#define STR_ACCU_ORTEGA_OUT 			"Accu_Ortega"
#define STR_ACCU_ZHAN_OUT 				"Accu_Zhan"

#define STR_LS_ANISO_OUT				"LS_Anisotropic"
#define STR_LS_D8_OUT 					"LS_D8"
#define STR_LS_FD8_OUT 					"LS_FD8"
#define STR_LS_FDD8_OUT 				"LS_FDD8"
#define STR_LS_ORTEGA_OUT 				"LS_Ortega"
#define STR_LS_ZHAN_OUT 				"LS_Zhan"

#define STR_RUSLE_ANISO_OUT				"RUSLE_Anisotropic"
#define STR_RUSLE_D8_OUT 				"RUSLE_D8"
#define STR_RUSLE_FD8_OUT 				"RUSLE_FD8"
#define STR_RUSLE_FDD8_OUT 				"RUSLE_FDD8"
#define STR_RUSLE_ORTEGA_OUT 			"RUSLE_Ortega"
#define STR_RUSLE_ZHAN_OUT 				"RUSLE_Zhan"
#define STR_RUSLE_FILE_OUT 				"RUSLE"

#define STR_SLOPE_OUT 					"Slope"

// Feedback
#define STR_ANISOTROPIC					"Anisotropic"
#define STR_D8							"D8"
#define STR_FD8							"FD8"
#define STR_FDD8						"FDD8"
#define STR_ORTEGA						"Ortega"
#define STR_ZHAN						"Zhan"

#define STR_ARGUMENTS 					"Arguments"
#define STR_BUFFERS 					"Buffers"
#define STR_CLEANUP 					"Cleanup"
#define STR_NORMALIZATION 				"Normalization"
#define STR_DIRDEP 						"Directions and Dependencies"
#define STR_KERNELS 					"Kernels"
#define STR_LOOP 						"Loop"
#define STR_PROFILING					"Profiling"
#define STR_RESULTS 					"Results"
#define STR_SETUP 						"Setup"
#define STR_SORT 						"Topological Sort"
#define STR_SORTANISOTROPIC				"Topological Anisotropic"
#define STR_SORTD8 						"Topological D8"
#define STR_SORTFD8 					"Topological FD8"
#define STR_SORTFDD8 					"Topological FDD8"

#endif /* STRINGS_HPP_ */
