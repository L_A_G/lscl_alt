/*
 * testcase.cpp
 *
 *  Created on: 3 Jun 2013
 *      Author: josten
 */

#include <fstream>
#include <iostream>
#include <limits>
#include <vector>

#include "CL/cl.h"

#include "header.hpp"
#include "io.hpp"
#include "LSCL.hpp"
#include "print.hpp"
#include "topological.hpp"

extern header *head;
extern cl_bool force_sort;

static const unsigned int TOTAL_ROWS = 6;
static const unsigned int TOTAL_COLUMNS = 6;
static const unsigned int INPUT_SIZE = 36;

// Contains pit at 3,1
/*float map[6][6] = {
	{20., 19., 18., 17., 19., 19.},
	{19., 19., 19., 16., 18., 18.},
	{17., 18., 18., 17., 17., 18.},
	{16., 17., 17., 16., 16., 17.},
	{16., 15., 16., 16., 15., 16.},
	{14., 15., 15., 14., 15., 15.}
};*/

const float map[TOTAL_ROWS][TOTAL_COLUMNS] = {
	{20., 19., 18., 17., 19., 19.},
	{19., 19., 19., 17., 18., 18.},
	{17., 18., 18., 17., 17., 18.},
	{16., 17., 17., 16., 16., 17.},
	{16., 15., 16., 16., 15., 16.},
	{14., 15., 15., 14., 15., 15.}
};

const unsigned char FD8_dirs[TOTAL_ROWS][TOTAL_COLUMNS] = {
	{0,  0,  0,  0,   0, 0},
	{0, 24, 60,  0, 112, 0},
	{0, 24, 62, 24,  48, 0},
	{0, 24, 60,  8,  16, 0},
	{0,  0, 64,  4,   0, 0},
	{0,  0,  0,  0,   0, 0}
};

const unsigned char FD8_deps[TOTAL_ROWS][TOTAL_COLUMNS] = {
	{0, 0,   0,   0,   0, 0},
	{0, 0,   0, 100,   0, 0},
	{0, 3, 129, 194,   1, 0},
	{0, 3, 129, 195, 129, 0},
	{0, 7, 129, 128, 193, 0},
	{0, 0,   0,   0,   0, 0}
};


void runOpenCLTestCase(void) {

	std::cout << "Performing small test case" << std::endl;

	// Create buffer for elevation test-case
	cl_float elevations[INPUT_SIZE];
	cl_float d8_accumulation[INPUT_SIZE];
	cl_float fd8_accumulation[INPUT_SIZE];
	cl_float fdd8_accumulation[INPUT_SIZE];
	cl_float ortega_accumulation[INPUT_SIZE];
	cl_float zhan_accumulation[INPUT_SIZE];
	cl_float slopes[INPUT_SIZE];
	cl_float d8_ls[INPUT_SIZE];
	cl_float fd8_ls[INPUT_SIZE];
	cl_float fdd8_ls[INPUT_SIZE];
	cl_float ort_ls[INPUT_SIZE];
	cl_float zhan_ls[INPUT_SIZE];
	cl_uint sort[INPUT_SIZE];
	cl_uchar fd8_dirs[INPUT_SIZE];
	cl_uchar fd8_deps[INPUT_SIZE];
	cl_float d8_topo_accu[INPUT_SIZE];
	cl_float fd8_topo_accu[INPUT_SIZE];
	cl_float fdd8_topo_accu[INPUT_SIZE];
	cl_float aniso1_topo_accu[INPUT_SIZE];
	cl_float anisoinf_topo_accu[INPUT_SIZE];
	cl_float vectors[INPUT_SIZE];

	for (unsigned int row = 0; row < TOTAL_ROWS; row++) {
		for (unsigned int column = 0; column < TOTAL_COLUMNS; column++) {
			int index = row * TOTAL_COLUMNS + column;
			elevations[index] = map[row][column];
			fd8_dirs[index] = FD8_dirs[row][column];
			fd8_deps[index] = FD8_deps[row][column];
			vectors[index] = 0.0f;
		}
	}

	// Create header
	header test_header;
	test_header.rows = TOTAL_ROWS;
 	test_header.columns = TOTAL_COLUMNS;
 	test_header.xll_corner = 0.0f;
 	test_header.yll_corner = 0.0f;
 	test_header.resolution = 1.0f;
 	test_header.no_data_value = -9999.0f;
 	head = &test_header;

 	cl_float threshold = 1.5f;
 	cl_float M = 1.0f;
 	cl_float Q0 = 0.0f;

	flowTransferD8(elevations, d8_accumulation);
	printFloatGridWithoutEdge(d8_accumulation, TOTAL_ROWS, TOTAL_COLUMNS);

	ortegaRueda(elevations, ortega_accumulation);
	printFloatGridWithoutEdge(ortega_accumulation, TOTAL_ROWS, TOTAL_COLUMNS);

	flowTransferFD8(elevations, fd8_accumulation);
	printFloatGridWithoutEdge(fd8_accumulation, TOTAL_ROWS, TOTAL_COLUMNS);

	zhanQin(elevations, zhan_accumulation);
	printFloatGridWithoutEdge(zhan_accumulation, TOTAL_ROWS, TOTAL_COLUMNS);

	flowTransferFDD8(elevations, threshold, fdd8_accumulation);
	printFloatGridWithoutEdge(fdd8_accumulation, TOTAL_ROWS, TOTAL_COLUMNS);

	M = std::numeric_limits<float>::max();
	M = 10.0f;
	flowTransferAnisotropic(elevations, M, Q0, vectors, anisoinf_topo_accu);
	std::cout << "M: " << M << " Q0: " << Q0 << " (D8)" << std::endl;
	printFloatGridWithoutEdge(anisoinf_topo_accu, TOTAL_ROWS, TOTAL_COLUMNS);
	M = 1.0f;
	flowTransferAnisotropic(elevations, M, Q0, vectors, aniso1_topo_accu);
	std::cout << "M: " << M << " Q0: " << Q0 << " (FD8)" << std::endl;
	printFloatGridWithoutEdge(aniso1_topo_accu, TOTAL_ROWS, TOTAL_COLUMNS);

	slope(elevations, slopes);
	printFloatGridWithoutEdge(slopes, TOTAL_ROWS, TOTAL_COLUMNS);

	cl_float m = 0.6f;
	cl_float n = 1.3f;

	LS(d8_accumulation, slopes, m, n, d8_ls, "D8");
	std::cout << "D8 LS";
	printFloatGridWithoutEdge(d8_ls, TOTAL_ROWS, TOTAL_COLUMNS);

	LS(ortega_accumulation, slopes, m, n, ort_ls, "Ortega");
	std::cout << "Ortega LS";
	printFloatGridWithoutEdge(ort_ls, TOTAL_ROWS, TOTAL_COLUMNS);

	LS(fd8_accumulation, slopes, m, n, fd8_ls, "FD8");
	std::cout << "FD8 LS";
	printFloatGridWithoutEdge(fd8_ls, TOTAL_ROWS, TOTAL_COLUMNS);

	LS(zhan_accumulation, slopes, m, n, zhan_ls, "Zhan");
	std::cout << "Zhan LS";
	printFloatGridWithoutEdge(zhan_ls, TOTAL_ROWS, TOTAL_COLUMNS);

	LS(fdd8_accumulation, slopes, m, n, fdd8_ls, "FDD8");
	std::cout << "FDD8 LS";
	printFloatGridWithoutEdge(fdd8_ls, TOTAL_ROWS, TOTAL_COLUMNS);

	force_sort = true;
	std::cout << "Topological Sort" << std::endl;
	std::vector<cl_uint> iteration_sizes;
	topologicalElevationSort(fd8_dirs, fd8_deps, sort, &iteration_sizes);
	size_t iterations = iteration_sizes.size();
	std::cout << "Computation iterations: " << iterations << std::endl;
	for (size_t iteration = 0; iteration < iterations; iteration++) {
		printf("%u ", iteration_sizes[iteration]);
	}

	std::cout << std::endl << "Topological order" << std::endl;
	cl_uint start = 0;
	cl_uint stop = 0;

	for (cl_uint iteration = 0; iteration < iterations; iteration++) {
		cl_uint iteration_size = iteration_sizes[iteration];
		start = stop;
		stop += iteration_size;

		for (cl_uint cell = start; cell < stop; cell++) {
			printf("%u ", sort[cell]);
		}

		printf("\n");
	}

	std::cout << "Topological Accumulation" << std::endl;
	topologicalAccumulation(elevations, aniso1_topo_accu, NULL, NULL, NULL, threshold, M, Q0, vectors);
	std::cout << "M: " << M << " Q0: " << Q0 << " (FD8)" << std::endl;
	printFloatGridWithoutEdge(aniso1_topo_accu, TOTAL_ROWS, TOTAL_COLUMNS);
	M = 10.0f;
	topologicalAccumulation(elevations, anisoinf_topo_accu, d8_topo_accu, fd8_topo_accu, fdd8_topo_accu, threshold, M, Q0, vectors);
	std::cout << "M: " << M << " Q0: " << Q0 << " (D8)" << std::endl;
	std::cout << "Anisotropic Accumulation M=1.0 Q0=0.0"<< std::endl;
	printFloatGridWithoutEdge(anisoinf_topo_accu, TOTAL_ROWS, TOTAL_COLUMNS);
	std::cout << "Topological D8";
	printFloatGridWithoutEdge(d8_topo_accu, TOTAL_ROWS, TOTAL_COLUMNS);
	std::cout << "Topological FD8";
	printFloatGridWithoutEdge(fd8_topo_accu, TOTAL_ROWS, TOTAL_COLUMNS);
	std::cout << "Topological FDD8";
	printFloatGridWithoutEdge(fdd8_topo_accu, TOTAL_ROWS, TOTAL_COLUMNS);
}
