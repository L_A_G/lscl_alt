/*
 * topological.cpp
 *
 *  Created on: 19 Aug 2013
 *      Author: josten
 */
#include <ctime>
#include <iostream>
#include <string>
#include <vector>
#include "CL/cl.h"

#include "common.hpp"
#include "error.hpp"
#include "header.hpp"
#include "io.hpp"
#include "kernel_size.hpp"
#include "limits.hpp"
#include "strings.hpp"
#include "topological.hpp"

#define DIRECTION_START 0
#define DIRECTION_STOP  8

const cl_float ONE_PATTERN_FLOAT = 1.0f;

// OpenCL setup
extern cl_bool performProfiling;
extern cl_command_queue queue;
extern cl_context context;
extern cl_program program;
extern header *head;

extern size_t max_local_size;
extern size_t max_global_size;

extern cl_bool save_sort;
extern cl_bool force_sort;

// File-scope Variables
size_t total_size = 0;
size_t global_work_size_1dim[1] = {0};
size_t local_work_size_1dim[1] = {0};

#define ROW 0
#define COLUMN 1
const int CELL_OFFSETS[8][2] = {
		{-1,  0}, // N
		{-1,  1}, // NE
		{ 0,  1}, // E
		{ 1,  1}, // SE
		{ 1,  0}, // S
		{ 1, -1}, // SW
		{ 0, -1}, // W
		{-1, -1}, // NW
};

// Forward declarations
inline bool checkBit(unsigned char variable, int bit);
inline unsigned char clearBit(unsigned char variable, int bit);
void setConstantTopologicalAnisotropicKernelArgs(cl_kernel topological_anisotropic_kernel, cl_mem sorted_cells, cl_mem dependency_buffer, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_float M, cl_float Q_0, cl_mem vector_buffer, cl_mem accumulation_buffer, std::string source);
void setConstantTopologicalD8KernelArgs(cl_kernel topological_d8_kernel, cl_mem sorted_cells, cl_mem dependency_buffer, cl_mem accumulation_buffer, std::string source);
void setConstantTopologicalFD8KernelArgs(cl_kernel topological_fd8_kernel, cl_mem sorted_cells, cl_mem dependency_buffer, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_mem accumulation_buffer, std::string source);
void setConstantTopologicalFDD8KernelArgs(cl_kernel topological_fd8_kernel, cl_mem sorted_cells, cl_mem d8_dependency_buffer, cl_mem fd8_dependency_buffer, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_float threshold, cl_mem accumulation_buffer, std::string source);
void topologicalAnisotropic(cl_mem sorted_buffer, std::vector<cl_uint> iteration_sizes, cl_mem dependency_buffer, cl_mem elevation_buffer, cl_mem normalization_buffer,  cl_float M, cl_float Q_0, cl_mem vector_buffer, cl_mem accumulation_buffer, cl_event* profiling_events);
void topologicalD8(cl_mem sorted_buffer, std::vector<cl_uint> iteration_sizes, cl_mem dependency_buffer, cl_mem accumulation_buffer, cl_event *profiling_events);
void topologicalFD8(cl_mem sorted_buffer, std::vector<cl_uint> iteration_sizes, cl_mem dependency_buffer, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_mem accumulation_buffer, cl_event *profiling_events);
void topologicalFDD8(cl_mem sorted_buffer, std::vector<cl_uint> iteration_sizes, cl_mem d8_dependency_buffer, cl_mem fd8_dependency_buffer, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_mem accumulation_buffer, cl_float threshold, cl_event *profiling_events);

inline bool checkBit(unsigned char variable, int bit) {
	// checks what value a bit has
	return ((variable & (1 << bit)) != 0);
}

inline unsigned char clearBit(unsigned char variable, int bit) {
	// sets selected bit to 0
	return variable &= ~(1 << bit);
}

void setConstantTopologicalAnisotropicKernelArgs(
	cl_kernel topological_anisotropic_kernel,
	cl_mem sorted_cells,
	cl_mem dependency_buffer,
	cl_mem elevation_buffer,
	cl_mem normalization_buffer,
	cl_float M,
	cl_float Q_0,
	cl_mem vector_buffer,
	cl_mem accumulation_buffer,
	std::string source)
{
	std::string message (source);
	message.append(" Topological Anisotropic arg ");

	cl_int errors[10] = {};
	errors[0] = clSetKernelArg(topological_anisotropic_kernel, 0, sizeof(cl_mem), &sorted_cells);
	errors[1] = clSetKernelArg(topological_anisotropic_kernel, 1, sizeof(cl_mem), &dependency_buffer);
	errors[2] = clSetKernelArg(topological_anisotropic_kernel, 2, sizeof(cl_mem), &elevation_buffer);
	errors[3] = clSetKernelArg(topological_anisotropic_kernel, 3, sizeof(cl_mem), &normalization_buffer);
	errors[4] = clSetKernelArg(topological_anisotropic_kernel, 6, sizeof(cl_uint), &head->columns);
	errors[5] = clSetKernelArg(topological_anisotropic_kernel, 7, sizeof(cl_float), &head->resolution);
	errors[6] = clSetKernelArg(topological_anisotropic_kernel, 8, sizeof(cl_float), &M);
	errors[7] = clSetKernelArg(topological_anisotropic_kernel, 9, sizeof(cl_float), &Q_0);
	errors[8] = clSetKernelArg(topological_anisotropic_kernel, 10, sizeof(cl_mem), &vector_buffer);
	errors[9] = clSetKernelArg(topological_anisotropic_kernel, 11, sizeof(cl_mem), &accumulation_buffer);
	checkForCLErrors(errors, 10, message);
}

void setConstantTopologicalD8KernelArgs(
	cl_kernel topological_d8_kernel,
	cl_mem sorted_cells,
	cl_mem dependency_buffer,
	cl_mem accumulation_buffer,
	std::string source)
{
	std::string out (source);
	out.append(" Topological D8 Arg ");

	cl_int errors[4] = {};
	errors[0] = clSetKernelArg(topological_d8_kernel, 0, sizeof(cl_mem), &sorted_cells);
	errors[1] = clSetKernelArg(topological_d8_kernel, 1, sizeof(cl_mem), &dependency_buffer);
	errors[2] = clSetKernelArg(topological_d8_kernel, 4, sizeof(cl_uint), &head->columns);
	errors[3] = clSetKernelArg(topological_d8_kernel, 5, sizeof(cl_mem), &accumulation_buffer);
	checkForCLErrors(errors, 4, out);
}

void setConstantTopologicalFD8KernelArgs(
	cl_kernel topological_fd8_kernel,
	cl_mem sorted_cells,
	cl_mem dependency_buffer,
	cl_mem elevation_buffer,
	cl_mem normalization_buffer,
	cl_mem accumulation_buffer,
	std::string source)
{
	std::string out (source);
	out.append(" Topological FD8 Arg ");

	cl_int errors[6] = {};
	errors[0] = clSetKernelArg(topological_fd8_kernel, 0, sizeof(cl_mem), &sorted_cells);
	errors[1] = clSetKernelArg(topological_fd8_kernel, 1, sizeof(cl_mem), &dependency_buffer);
	errors[2] = clSetKernelArg(topological_fd8_kernel, 2, sizeof(cl_mem), &elevation_buffer);
	errors[3] = clSetKernelArg(topological_fd8_kernel, 3, sizeof(cl_mem), &normalization_buffer);
	errors[4] = clSetKernelArg(topological_fd8_kernel, 6, sizeof(cl_uint), &head->columns);
	errors[5] = clSetKernelArg(topological_fd8_kernel, 7, sizeof(cl_mem), &accumulation_buffer);
	checkForCLErrors(errors, 6, out);
}

void setConstantTopologicalFDD8KernelArgs(
	cl_kernel topological_fd8_kernel,
	cl_mem sorted_cells,
	cl_mem d8_dependency_buffer,
	cl_mem fd8_dependency_buffer,
	cl_mem elevation_buffer,
	cl_mem normalization_buffer,
	cl_float threshold,
	cl_mem accumulation_buffer,
	std::string source)
{
	std::string out (source);
	out.append(" Topological FD8 arg ");

	cl_int errors[8] = {};
	errors[0] = clSetKernelArg(topological_fd8_kernel, 0, sizeof(cl_mem), &sorted_cells);
	errors[1] = clSetKernelArg(topological_fd8_kernel, 1, sizeof(cl_mem), &d8_dependency_buffer);
	errors[2] = clSetKernelArg(topological_fd8_kernel, 2, sizeof(cl_mem), &fd8_dependency_buffer);
	errors[3] = clSetKernelArg(topological_fd8_kernel, 3, sizeof(cl_mem), &elevation_buffer);
	errors[4] = clSetKernelArg(topological_fd8_kernel, 4, sizeof(cl_mem), &normalization_buffer);
	errors[5] = clSetKernelArg(topological_fd8_kernel, 7, sizeof(cl_uint), &head->columns);
	errors[6] = clSetKernelArg(topological_fd8_kernel, 8, sizeof(cl_float), &threshold);
	errors[7] = clSetKernelArg(topological_fd8_kernel, 9, sizeof(cl_mem), &accumulation_buffer);
	checkForCLErrors(errors, 8, source);
}

void topologicalAnisotropic(
	cl_mem sorted_buffer,
	std::vector<cl_uint> iteration_sizes,
	cl_mem dependency_buffer,
	cl_mem elevation_buffer,
	cl_mem normalization_buffer,
	cl_float M,
	cl_float Q_0,
	cl_mem vector_buffer,
	cl_mem accumulation_buffer,
	cl_event* profiling_events)
{
	std::cout << std::endl << "Starting Anisotropic..." << std::endl;

	cl_int errors[3] = {};
	std::string source ("Topological Anisotropic ");

	// Create Kernels
	cl_kernel flow_kernel = clCreateKernel(program, STR_KERNEL_ANISO_TOPO, &errors[0]);
	setConstantTopologicalAnisotropicKernelArgs(flow_kernel, sorted_buffer, dependency_buffer, elevation_buffer, normalization_buffer, M, Q_0, vector_buffer, accumulation_buffer, source);
	errors[1] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 2, source + STR_KERNELS);

	cl_uint start = 0;
	cl_uint stop = 0;

	for (cl_uint iteration = 0; iteration < iteration_sizes.size(); iteration++) {

		cl_uint iteration_size = iteration_sizes[iteration];
		start = stop;
		stop += iteration_size;

		errors[0] = clSetKernelArg(flow_kernel, 4, sizeof(cl_uint), &start);
		errors[1] = clSetKernelArg(flow_kernel, 5, sizeof(cl_uint), &stop);

		// compute sizes
		size_t kernel_size_this_iteration = computeTotalSizeWithPadding(iteration_size);
		errors[2] = clEnqueueNDRangeKernel(queue, flow_kernel, 1, NULL, &kernel_size_this_iteration, local_work_size_1dim, 0, NULL, NULL);
		checkForCLErrors(errors, 3, source + STR_LOOP);
	}

	errors[0] = clReleaseKernel(flow_kernel);
	checkForCLErrors(errors, 1, source + STR_CLEANUP);
}

void topologicalD8(
	cl_mem sorted_buffer,
	const std::vector<cl_uint> iteration_sizes,
	cl_mem dependency_buffer,
	cl_mem accumulation_buffer,
	cl_event *profiling_events)
{

	std::cout << std::endl << "Starting " << STR_D8 << "..." << std::endl;

	cl_int errors[3] = {};
	std::string source ("Topological D8 ");

	// Create Kernels
	cl_kernel flow_kernel = clCreateKernel(program, STR_KERNEL_D8_TOPO, &errors[0]);
	setConstantTopologicalD8KernelArgs(flow_kernel, sorted_buffer, dependency_buffer, accumulation_buffer, source);
	errors[1] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 2, source + STR_KERNELS);

	cl_uint start = 0;
	cl_uint stop = 0;

	for (cl_uint iteration = 0; iteration < iteration_sizes.size(); iteration++) {

		cl_uint iteration_size = iteration_sizes[iteration];
		start = stop;
		stop += iteration_size;

		errors[0] = clSetKernelArg(flow_kernel, 2, sizeof(cl_uint), &start);
		errors[1] = clSetKernelArg(flow_kernel, 3, sizeof(cl_uint), &stop);

		// compute sizes
		size_t kernel_size_this_iteration = computeTotalSizeWithPadding(iteration_size);
		errors[2] = clEnqueueNDRangeKernel(queue, flow_kernel, 1, NULL, &kernel_size_this_iteration, local_work_size_1dim, 0, NULL, NULL);
		checkForCLErrors(errors, 3, source + STR_LOOP);
	}

	errors[0] = clReleaseKernel(flow_kernel);
	checkForCLErrors(errors, 1, source + STR_CLEANUP);
}

void topologicalFD8(
	cl_mem sorted_buffer,
	const std::vector<cl_uint> iteration_sizes,
	cl_mem dependency_buffer,
	cl_mem elevation_buffer,
	cl_mem normalization_buffer,
	cl_mem accumulation_buffer,
	cl_event *profiling_events)
{
	std::cout << std::endl << "Starting FD8..." << std::endl;

	cl_int errors[3] = {};
	std::string source ("Topological FD8 ");

	// Create Kernels
	cl_kernel flow_kernel = clCreateKernel(program, STR_KERNEL_FD8_TOPO, &errors[0]);
	setConstantTopologicalFD8KernelArgs(flow_kernel, sorted_buffer, dependency_buffer, elevation_buffer, normalization_buffer, accumulation_buffer, source);
	errors[1] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 2, source + STR_KERNELS);

	cl_uint start = 0;
	cl_uint stop = 0;

	for (cl_uint iteration = 0; iteration < iteration_sizes.size(); iteration++) {

		cl_uint iteration_size = iteration_sizes[iteration];
		start = stop;
		stop += iteration_size;

		errors[0] = clSetKernelArg(flow_kernel, 4, sizeof(cl_uint), &start);
		errors[1] = clSetKernelArg(flow_kernel, 5, sizeof(cl_uint), &stop);

		// compute sizes
		size_t kernel_size_this_iteration = computeTotalSizeWithPadding(iteration_size);
		errors[2] = clEnqueueNDRangeKernel(queue, flow_kernel, 1, NULL, &kernel_size_this_iteration, local_work_size_1dim, 0, NULL, NULL);
		checkForCLErrors(errors, 3, source + STR_LOOP);
	}

	errors[0] = clReleaseKernel(flow_kernel);
	checkForCLErrors(errors, 1, source + STR_CLEANUP);
}

void topologicalFDD8(
	cl_mem sorted_buffer,
	const std::vector<cl_uint> iteration_sizes,
	cl_mem d8_dependency_buffer,
	cl_mem fd8_dependency_buffer,
	cl_mem elevation_buffer,
	cl_mem normalization_buffer,
	cl_mem accumulation_buffer,
	cl_float threshold,
	cl_event *profiling_events)
{
	std::cout << std::endl << "Starting FDD8..." << std::endl;

	cl_int errors[3] = {};
	std::string source ("Topological FDD8 ");

	// Create Kernels
	cl_kernel flow_kernel = clCreateKernel(program, STR_KERNEL_FDD8_TOPO, &errors[0]);
	setConstantTopologicalFDD8KernelArgs(flow_kernel, sorted_buffer, d8_dependency_buffer, fd8_dependency_buffer, elevation_buffer, normalization_buffer, threshold, accumulation_buffer, source);
	errors[1] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 2, source + STR_KERNELS);

	cl_uint start = 0;
	cl_uint stop = 0;

	for (cl_uint iteration = 0; iteration < iteration_sizes.size(); iteration++) {

		cl_uint iteration_size = iteration_sizes[iteration];
		start = stop;
		stop += iteration_size;

		errors[0] = clSetKernelArg(flow_kernel, 5, sizeof(cl_uint), &start);
		errors[1] = clSetKernelArg(flow_kernel, 6, sizeof(cl_uint), &stop);

		// compute sizes
		size_t kernel_size_this_iteration = computeTotalSizeWithPadding(iteration_size);
		errors[2] = clEnqueueNDRangeKernel(queue, flow_kernel, 1, NULL, &kernel_size_this_iteration, local_work_size_1dim, 0, NULL, NULL);
		checkForCLErrors(errors, 3, source + STR_LOOP);

	}

	errors[0] = clReleaseKernel(flow_kernel);
	checkForCLErrors(errors, 1, source + STR_CLEANUP);
}

void topologicalElevationSort(
	const unsigned char *flow_directions,
	unsigned char *flow_dependencies,
	unsigned int *topological_sort,
	std::vector<cl_uint> *iteration_sizes)
{
	// The algorithm first scans through all the elements and add the peaks. Then it repeats the following procedure until no cells remains:

	/*
	 * Go through the added elements last iteration, keep track of how many cells are added this iteration.
	 * If any of the added cells' down_cells have no remaining dependencies,
	 * 	they are added to the current end of the topological sort
	 * 	their down_cell's dependencies are removed
	 * 	decrease number of remaining cells
	 * At the end of the iteration, record how many cells were added into the computation_order
	 */

	time_t start = time(0);
	cl_uchar *edges = flow_dependencies;
	std::cout << "Topological sort started..." << std::endl;
	iteration_sizes->clear();
	iteration_sizes->reserve(2000);
	cl_uint total_cells = head->rows * head->columns;
	cl_uint cells_added_last_iteration = 0; 	// This keeps track how many elements should be iterated over next iteration
	cl_uint next_sorted = 0; // This is the current index of the topological sort, where we are putting elements in

	// Cells_to_process gets reduced as more elements are processed, when cells_to_process is 0, the algorithm is finished
	cl_uint cells_to_process = total_cells;

	// Add the peaks to the array
	for (cl_uint cell_index = 0; cell_index < total_cells; cell_index++) {

		if (edges[cell_index] == 0) {
			topological_sort[next_sorted] = cell_index;

			next_sorted++;
			cells_to_process--;
			cells_added_last_iteration++;
		}
	}
	iteration_sizes->push_back(cells_added_last_iteration);

	// then during this iteration, loop through added items last iteration
	while (cells_to_process > 0) {

		cl_uint first_index_last_iteration = next_sorted - cells_added_last_iteration;
		cl_uint last_index_last_iteration = next_sorted;

		cells_added_last_iteration = 0;
		for (unsigned int cell_sort_index = first_index_last_iteration; cell_sort_index < last_index_last_iteration; cell_sort_index++) {

			cl_uint index = topological_sort[cell_sort_index];

			// Remove the dependencies from the cell
			for (int direction = DIRECTION_START; direction < DIRECTION_STOP; direction++) {
				unsigned char directions = flow_directions[index];
				if (checkBit(directions, direction) != 0) {
					cl_uint down_row = (index / head->columns) + CELL_OFFSETS[direction][ROW];
					cl_uint down_column = (index % head->columns) + CELL_OFFSETS[direction][COLUMN];

					if (onEdge(down_row, down_column)) {
						continue;
					}

					cl_uint down_index = down_row * head->columns + down_column;
					cl_uchar dependencies = edges[down_index];
					cl_int direction_to_this_cell = (direction < 4) ? direction + 4 : direction - 4;
					dependencies = clearBit(dependencies, direction_to_this_cell);
					edges[down_index] = dependencies;

					if (dependencies == 0) {
						topological_sort[next_sorted] = down_index;
						next_sorted++;
						cells_to_process--;
						cells_added_last_iteration++;
					}
				}
			}
		}

		iteration_sizes->push_back(cells_added_last_iteration);
	}

	time_t stop = time(0);
	time_t time_taken = (time_t) difftime(stop, start);

	// Output
	std::cout << std::endl;
	std::cout << "Topological Sort: " << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	std::cout << std::endl;
}

void topologicalAccumulation(
	cl_float *elevation_data,
	cl_float *anisotropic_accumulation_data,
	cl_float *d8_accumulation_data,
	cl_float *fd8_accumulation_data,
	cl_float *fdd8_accumulation_data,
	cl_float threshold,
	cl_float M,
	cl_float Q0,
	cl_float *vector_data)
{
	// Non desired accumulations should be NULL

	if (max_local_size == 0 || max_global_size == 0) {
		reportError("Max Size set to 0");
	}

	if (!elevation_data) {
		reportError("No elevation data specified for Topological Transfer");
	}

	if (!(d8_accumulation_data || fd8_accumulation_data || fdd8_accumulation_data || anisotropic_accumulation_data)) {
		reportError("No Algorithm specified for Topological Transfer");
	}

	if (threshold < FDD8_THRESHOLD_MIN || threshold > FDD8_THRESHOLD_MAX) {
		reportError("FDD8 Threshold is outside valid range");
	}

	// Check M & Q0 arguments
	if (M < ANISOTROPIC_M_MIN || M > ANISOTROPIC_M_MAX) {
		reportError("Anisotropic M factor is outside valid range");
	}

	if (Q0 < ANISOTROPIC_Q0_MIN || Q0 > ANISOTROPIC_Q0_MAX) {
		reportError("Anisotropic Q0 factor is outside valid range");
	}

	// Set work group sizes
	total_size = head->rows * head->columns;
	global_work_size_1dim[0] = computeTotalSizeWithPadding(total_size);
	local_work_size_1dim[0] = max_global_size;

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];
	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	cl_int errors[10] = {};
	std::string source ("Topological Transfer ");

	// Memory objects
	size_t float_buffer_size = total_size * sizeof(cl_float);
	size_t uchar_buffer_size = total_size * sizeof(cl_uchar);

	cl_mem accumulation_buffer = NULL;
	cl_mem aniso_normalization_buffer = NULL;
	cl_mem fd8_normalization_buffer = NULL;
	cl_mem d8_direction_buffer = NULL;
	cl_mem d8_dependency_buffer = NULL;
	cl_mem elevation_buffer = NULL;
	cl_mem vector_buffer = NULL;

	elevation_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, elevation_data, &errors[0]);
	accumulation_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[1]);
	cl_kernel dependency_kernel = clCreateKernel(program, STR_KERNEL_DEPEND, &errors[2]);
	checkForCLErrors(errors, 3, source);

	cl_event profiling_events[PROFILING_EVENTS];
	errors[0] = clEnqueueMarker(queue, &profiling_events[PROFILING_SETUP_START]);
	checkForCLErrors(errors, 1, source + STR_PROFILING + " " + STR_SETUP);

	// Calculate D8 Directions and Dependencies
	if (d8_accumulation_data || fdd8_accumulation_data) {
		cl_kernel d8_direction_kernel = clCreateKernel(program, STR_KERNEL_D8_DIRS, &errors[0]);

		d8_direction_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[0]);
		d8_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[1]);

		setDirectionKernelArgs(d8_direction_kernel, elevation_buffer, d8_direction_buffer, source);
		errors[3] = clEnqueueNDRangeKernel(queue, d8_direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

		setDependencyKernelArgs(dependency_kernel, d8_direction_buffer, d8_dependency_buffer, source);
		errors[4] = clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

		errors[5] = clReleaseKernel(d8_direction_kernel);
		checkForCLErrors(errors, 6, source + STR_DIRDEP);
	}

	// FD8 directions and dependencies are always calculated
	// FD8 is used for topological sort, since D8 dependencies will always be a strict subset of FD8 dependencies
	cl_kernel fd8_direction_kernel = clCreateKernel(program, STR_KERNEL_FD8_DIRS, &errors[0]);

	cl_mem fd8_direction_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[0]);
	cl_mem fd8_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[1]);

	setDirectionKernelArgs(fd8_direction_kernel, elevation_buffer, fd8_direction_buffer, source);
	errors[1] = clEnqueueNDRangeKernel(queue, fd8_direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);
	errors[2] = clReleaseKernel(fd8_direction_kernel);

	setDependencyKernelArgs(dependency_kernel, fd8_direction_buffer, fd8_dependency_buffer, source);
	errors[3] = clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);
	checkForCLErrors(errors, 4, source + STR_DIRDEP);

	// Calculate FD8 Normalization
	if (fd8_accumulation_data || fdd8_accumulation_data) {
		cl_kernel fd8_normalization_kernel = clCreateKernel(program, STR_KERNEL_FD8_NORMALIZATION, &errors[0]);
		fd8_normalization_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[1]);

		setFD8NormalizationKernelArgs(fd8_normalization_kernel, elevation_buffer, fd8_normalization_buffer, source);
		errors[2] = clEnqueueNDRangeKernel(queue, fd8_normalization_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);
		errors[3] = clReleaseKernel(fd8_normalization_kernel);
		checkForCLErrors(errors, 4, source + "FD8 " + STR_NORMALIZATION);
	}

	// Calculate Anisotropic Normalization
	if (anisotropic_accumulation_data) {
		cl_kernel aniso_normalization_kernel = clCreateKernel(program, STR_KERNEL_ANISO_NORMALIZATION, &errors[0]);
		vector_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, (void *)vector_data, &errors[1]);
		aniso_normalization_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[2]);

		setAnisotropicNormalizationKernelArgs(aniso_normalization_kernel, elevation_buffer, M, Q0, vector_buffer, aniso_normalization_buffer, source);
		errors[3] = clEnqueueNDRangeKernel(queue, aniso_normalization_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);
		errors[4] = clReleaseKernel(aniso_normalization_kernel);
		checkForCLErrors(errors, 5, source + "Anisotropic" + STR_NORMALIZATION);
	}

	// Sort using FD8 dependencies
	cl_uint *sorted_cells = new cl_uint[total_size];
	std::vector<cl_uint> iteration_sizes;

	// Does sort file exist?
	bool read_sort_from_file = false;
	if (!force_sort) {
		read_sort_from_file = readTopologicalSortFromFile(&sorted_cells, &iteration_sizes);
		if (!read_sort_from_file) {
			std::cout << "Could not find sort files for DEM, performing new sort" << std::endl;
		}
	}

	if (!read_sort_from_file ) {
		// Otherwise sort
		cl_uchar *fd8_directions = new cl_uchar[total_size];
		cl_uchar *fd8_dependencies = new cl_uchar[total_size];
		errors[0] = clEnqueueReadBuffer(queue, fd8_direction_buffer, CL_FALSE, 0, uchar_buffer_size, fd8_directions, 0, NULL, NULL);
		errors[1] = clEnqueueReadBuffer(queue, fd8_dependency_buffer, CL_TRUE, 0, uchar_buffer_size, fd8_dependencies, 0, NULL, NULL);

		topologicalElevationSort(fd8_directions, fd8_dependencies, sorted_cells, &iteration_sizes);

		if (save_sort && !force_sort) {
			writeTopologicalSortToFile(sorted_cells, iteration_sizes);
		}

		delete[] fd8_directions;
		delete[] fd8_dependencies;
	}

	cl_mem sorted_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, sorted_cells, &errors[2]);
	delete[] sorted_cells;

	// Topological setup complete here
	errors[3] = clEnqueueMarker(queue, &profiling_events[PROFILING_SETUP_STOP]);

	// Accumulation buffer will be reused for each algorithm, and needs to be reset at beginning of each
	cl_kernel fill_float_edge_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE, &errors[4]);
	setFillFloatEdgeKernelArgs(fill_float_edge_kernel, accumulation_buffer, ONE_PATTERN_FLOAT, source);
	checkForCLErrors(errors, 5, source + STR_SORT + STR_SETUP);

	// Timers
	time_t time_start;
	time_t time_stop;
	cl_double time_taken;

	// Perform Accumulation
	if (anisotropic_accumulation_data) {

		time_start = time(NULL);
		errors[0] = clEnqueueNDRangeKernel(queue, fill_float_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);
		topologicalAnisotropic(sorted_buffer, iteration_sizes, fd8_dependency_buffer, elevation_buffer, aniso_normalization_buffer, M, Q0, vector_buffer, accumulation_buffer, profiling_events);

		errors[1] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, anisotropic_accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
		time_stop = time(NULL);
		time_taken = difftime(time_stop, time_start);
		errors[2] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
		checkForCLErrors(errors, 3, source + STR_SORTANISOTROPIC);
		std::cout << "Time taken: " << time_taken << std::endl;
		if (performProfiling) {
			calculateProfilingTimes(profiling_events, STR_SORTANISOTROPIC);
		}
	}

	if (d8_accumulation_data) {

		time_start = time(NULL);
		errors[0] = clEnqueueNDRangeKernel(queue, fill_float_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);
		topologicalD8(sorted_buffer, iteration_sizes, d8_dependency_buffer, accumulation_buffer, profiling_events);

		errors[1] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, d8_accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
		time_stop = time(NULL);
		time_taken = difftime(time_stop, time_start);
		errors[2] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
		checkForCLErrors(errors, 3, source + STR_SORTD8);
		std::cout << "Time taken: " << time_taken << std::endl;
		if (performProfiling) {
			calculateProfilingTimes(profiling_events, STR_SORTD8);
		}
	}

	if (fd8_accumulation_data) {

		time_start = time(NULL);
		errors[0] = clEnqueueNDRangeKernel(queue, fill_float_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);
		topologicalFD8(sorted_buffer, iteration_sizes, fd8_dependency_buffer, elevation_buffer, fd8_normalization_buffer, accumulation_buffer, profiling_events);

		errors[1] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, fd8_accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
		time_stop = time(NULL);
		time_taken = difftime(time_stop, time_start);
		errors[2] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
		checkForCLErrors(errors, 3, source + STR_SORTFD8);
		std::cout << "Time taken: " << time_taken << std::endl;
		if (performProfiling) {
			calculateProfilingTimes(profiling_events, STR_SORTFD8);
		}
	}

	if (fdd8_accumulation_data) {

		time_start = time(NULL);
		errors[0] = clEnqueueNDRangeKernel(queue, fill_float_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);
		topologicalFDD8(sorted_buffer, iteration_sizes, d8_dependency_buffer, fd8_dependency_buffer, elevation_buffer, fd8_normalization_buffer, accumulation_buffer, threshold, profiling_events);

		errors[1] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, fdd8_accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
		time_stop = time(NULL);
		time_taken = difftime(time_stop, time_start);
		errors[2] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
		checkForCLErrors(errors, 3, source + STR_SORTFDD8);
		std::cout << "Time taken: " << time_taken << std::endl;
		if (performProfiling) {
			calculateProfilingTimes(profiling_events, STR_SORTFDD8);
		}
	}

	// Cleanup
	errors[0] = clReleaseMemObject(elevation_buffer);
	errors[1] = clReleaseMemObject(accumulation_buffer);
	if (d8_accumulation_data || fdd8_accumulation_data) {
		errors[0] = clReleaseMemObject(d8_direction_buffer);
		errors[1] = clReleaseMemObject(d8_dependency_buffer);
	}
	errors[2] = clReleaseMemObject(fd8_direction_buffer);
	errors[3] = clReleaseMemObject(fd8_dependency_buffer);
	errors[4] = clReleaseMemObject(sorted_buffer);
	if (fd8_accumulation_data || fdd8_accumulation_data) {
		errors[5] = clReleaseMemObject(fd8_normalization_buffer);
	}
	errors[6] = clReleaseKernel(dependency_kernel);
	errors[7] = clReleaseKernel(fill_float_edge_kernel);
	if (anisotropic_accumulation_data) {
		errors[8] = clReleaseMemObject(aniso_normalization_buffer);
		errors[9] = clReleaseMemObject(vector_buffer);
	}
	checkForCLErrors(errors, 10, source + STR_CLEANUP);
}
