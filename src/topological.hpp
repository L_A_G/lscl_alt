/*
 * topological.hpp
 *
 *  Created on: 19 Aug 2013
 *      Author: josten
 */

#ifndef TOPOLOGICAL_HPP_
#define TOPOLOGICAL_HPP_

void topologicalElevationSort(const unsigned char *flow_directions, unsigned char *flow_dependencies, unsigned int *topological_sort, std::vector<cl_uint> *iteration_sizes);

#endif /* TOPOLOGICAL_HPP_ */
