/*
 * transfer.cpp
 *
 *  Created on: 5 Jun 2013
 *      Author: josten
 */

#include <ctime>
#include <iostream>
#include <string>

#include "CL/cl.h"

#include "common.hpp"
#include "error.hpp"
#include "header.hpp"
#include "io.hpp"
#include "kernel_size.hpp"
#include "limits.hpp"
#include "strings.hpp"

// print options
extern bool printDirections;
extern bool printDependencies;
extern bool printNormalization;
extern bool printIndegree;

// OpenCL setup
extern cl_bool performProfiling;
extern cl_command_queue queue;
extern cl_context context;
extern cl_program program;
extern header *head;

extern size_t max_local_size;
extern size_t max_global_size;

// Byte patterns used for fill kernel
static cl_float ZERO_PATTERN_FLOAT = 0.0f;
static cl_uchar ZERO_PATTERN_UCHAR = 0;
static cl_float ONE_PATTERN_FLOAT = 1.0f;

void setConstantAnisotropicKernelArgs(cl_kernel flow_transfer_kernel, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_float M, cl_float Q_0, cl_mem vector_buffer, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source);
void setConstantD8KernelArgs(cl_kernel flow_transfer_kernel, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source);
void setConstantFD8KernelArgs(cl_kernel flow_transfer_kernel, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source);
void setConstantFDD8KernelArgs(cl_kernel flow_transfer_kernel, cl_mem elevation_buffer, cl_float threshold, cl_mem d8_dependency_buffer, cl_mem accumulation_buffer, cl_mem normalization_buffer, cl_mem device_run_again, std::string source);
void setConstantOrtegaRuedaKernelArgs(cl_kernel flow_transfer_kernel, cl_mem dependency_buffer, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source);
void setConstantZhanQinKernelArgs(cl_kernel zhan_qin_kernel, cl_mem elevation_buffer, cl_mem direction_buffer, cl_mem dependency_buffer, cl_mem normalization_buffer, cl_mem indegree_buffer, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source);
void setIndegreeKernelArgs(cl_kernel indegree_kernel, cl_mem dependency_buffer, cl_mem indegree_buffer, std::string source);

void writeFloatGPUBufferToFile(cl_mem buffer, std::string output_filename);
void writeIntegerGPUBufferToFile(cl_mem buffer, std::string output_filename);
void writeUnsignedCharGPUBufferToFile(cl_mem buffer, std::string output_filename);

void flowTransferAnisotropic(cl_float *elevation_data, cl_float M, cl_float Q_0, cl_float *vector_data, cl_float *accumulation_data) {

	std::cout << std::endl << "Starting Anisotropic..." << std::endl;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Set work group sizes
	size_t total_size = head->rows * head->columns;

	size_t global_work_size_1dim[1] = {computeTotalSizeWithPadding(total_size)};
	size_t local_work_size_1dim[1] = {max_global_size};

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];
	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	cl_int errors[8];
	std::string source = "FlowTransferAnisotropic ";

	// Create kernels
	cl_kernel normalization_kernel			= clCreateKernel(program, STR_KERNEL_ANISO_NORMALIZATION,	&errors[0]);
	cl_kernel dependency_kernel		= clCreateKernel(program, STR_KERNEL_DEPEND,				&errors[1]);
	cl_kernel direction_kernel		= clCreateKernel(program, STR_KERNEL_FD8_DIRS,				&errors[2]);
	cl_kernel fill_kernel			= clCreateKernel(program, STR_KERNEL_FILL_UCHAR,			&errors[3]);
	cl_kernel fill_edge_kernel		= clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE,		&errors[4]);
	cl_kernel flow_transfer_kernel	= clCreateKernel(program, STR_KERNEL_ANISO_ACCU,			&errors[5]);
	checkForCLErrors(errors, 6, source + STR_KERNELS);

	// Create memory objects
	size_t float_buffer_size	= total_size * sizeof(cl_float);
	size_t uchar_buffer_size	= total_size * sizeof(cl_uchar);

	cl_mem elevation_buffer		= clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, (void *)elevation_data, 	&errors[0]);
	cl_mem vector_buffer		= clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, (void *)vector_data,		&errors[1]);

	cl_mem accumulation_buffer		= clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[2]);
	cl_mem normalization_buffer		= clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[3]);
	cl_mem device_run_again			= clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_bool), 	NULL, &errors[4]);
	cl_mem direction_buffer			= clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[5]);
	cl_mem new_dependency_buffer	= clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[6]);
	cl_mem old_dependency_buffer	= clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[7]);
	checkForCLErrors(errors, 8, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	// Initialise buffers
	setFillUcharKernelArgs(fill_kernel, old_dependency_buffer, (cl_uint) total_size, ZERO_PATTERN_UCHAR, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);

	setFillFloatEdgeKernelArgs(fill_edge_kernel, accumulation_buffer, ONE_PATTERN_FLOAT, source);
	errors[1] = clEnqueueNDRangeKernel(queue, fill_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Calculate directions
	setDirectionKernelArgs(direction_kernel, elevation_buffer, direction_buffer, source);
	errors[2] = clEnqueueNDRangeKernel(queue, direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Calculate dependencies
	setDependencyKernelArgs(dependency_kernel, direction_buffer, new_dependency_buffer, source);
	errors[3] = clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Calculate Normalization
	setAnisotropicNormalizationKernelArgs(normalization_kernel, elevation_buffer, M, Q_0, vector_buffer, normalization_buffer, source);
	errors[4] = clEnqueueNDRangeKernel(queue, normalization_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_STOP]);
	checkForCLErrors(errors, 5, source + STR_SETUP);

	// Write out intermediate steps?
	if (printDirections) {
		writeUnsignedCharGPUBufferToFile(direction_buffer, STR_ANISO_TRANSFER_DIR_OUT);
	}

	if (printDependencies) {
		writeUnsignedCharGPUBufferToFile(new_dependency_buffer, STR_ANISO_TRANSFER_DEP_OUT);
	}

	if (printNormalization) {
		writeFloatGPUBufferToFile(normalization_buffer, STR_ANISO_TRANSFER_NORM_OUT);
	}

	// Calculate Flow
	setConstantAnisotropicKernelArgs(flow_transfer_kernel, elevation_buffer, normalization_buffer, M, Q_0, vector_buffer, accumulation_buffer, device_run_again, source);

	// Main loop
	int i = 0;
	cl_bool host_run_again = false;
	time_t start = time(0);
	errors[0] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 1, source + STR_SETUP);
	do {
		i++;

		// Swap the buffers
		cl_mem swap = old_dependency_buffer;
		old_dependency_buffer = new_dependency_buffer;
		new_dependency_buffer = swap;

		// Reset run variable
		cl_bool flow_false = false;
		errors[0] = clEnqueueWriteBuffer(queue, device_run_again, CL_FALSE, 0, sizeof(cl_bool), &flow_false, 0, NULL, NULL);

		// Run the kernel
		errors[1] = clSetKernelArg(flow_transfer_kernel, 0, sizeof(cl_mem), &old_dependency_buffer);
		errors[2] = clSetKernelArg(flow_transfer_kernel, 10, sizeof(cl_mem), &new_dependency_buffer);
		errors[3] = clEnqueueNDRangeKernel(queue, flow_transfer_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

		// Did anything change?
		errors[4] = clEnqueueReadBuffer(queue, device_run_again, CL_TRUE, 0, sizeof(cl_bool), &host_run_again, 0, NULL, NULL);

		checkForCLErrors(errors, 5, source + STR_LOOP);

	} while (host_run_again);

	errors[0] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[1] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 2, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "Flow Transfer Anisotropic: " << std::endl;
	std::cout << "Number of iterations: " << i << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(accumulation_buffer);
	errors[1] = clReleaseMemObject(normalization_buffer);
	errors[2] = clReleaseMemObject(device_run_again);
	errors[3] = clReleaseMemObject(direction_buffer);
	errors[4] = clReleaseMemObject(elevation_buffer);
	errors[5] = clReleaseMemObject(new_dependency_buffer);
	errors[6] = clReleaseMemObject(old_dependency_buffer);
	errors[7] = clReleaseMemObject(vector_buffer);
	checkForCLErrors(errors, 8, source + STR_CLEANUP + " " + STR_BUFFERS);

	errors[0] = clReleaseKernel(normalization_kernel);
	errors[1] = clReleaseKernel(dependency_kernel);
	errors[2] = clReleaseKernel(direction_kernel);
	errors[3] = clReleaseKernel(fill_edge_kernel);
	errors[4] = clReleaseKernel(fill_kernel);
	errors[5] = clReleaseKernel(flow_transfer_kernel);
	checkForCLErrors(errors, 6, source + STR_CLEANUP + " " + STR_KERNELS);
}

void flowTransferD8(cl_float *elevation_data, cl_float *accumulation_data) {

	std::cout << std::endl << "Starting D8..." << std::endl;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Set work group sizes
	size_t total_size = head->rows * head->columns;

	size_t global_work_size_1dim[1] = {computeTotalSizeWithPadding(total_size)};
	size_t local_work_size_1dim[1] = {max_global_size};

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];
	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	cl_int errors[6];
	std::string source = "FlowTransferD8 ";

	// Create kernels
	cl_kernel direction_kernel = clCreateKernel(program, STR_KERNEL_D8_DIRS, &errors[0]);
	cl_kernel dependency_kernel = clCreateKernel(program, STR_KERNEL_DEPEND, &errors[1]);
	cl_kernel flow_transfer_kernel = clCreateKernel(program, STR_KERNEL_D8_ACCU, &errors[2]);
	cl_kernel fill_kernel = clCreateKernel(program, STR_KERNEL_FILL_UCHAR, &errors[3]);
	cl_kernel fill_edge_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE, &errors[4]);
	checkForCLErrors(errors, 5, source + STR_KERNELS);

	// Create memory objects
	size_t float_buffer_size = total_size * sizeof(cl_float);
	size_t uchar_buffer_size = total_size * sizeof(cl_uchar);
	cl_mem elevation_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, (void *)elevation_data, &errors[0]);
	cl_mem direction_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[1]);
	cl_mem old_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[2]);
	cl_mem new_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[3]);
	cl_mem accumulation_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[4]);
	cl_mem device_run_again = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_bool), NULL, &errors[5]);
	checkForCLErrors(errors, 6, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	// Initialise old_dependency to 0, new will become initial dependencies. It's the other way around but they will be swapped in the main loop
	setFillUcharKernelArgs(fill_kernel, old_dependency_buffer, (cl_uint) total_size, ZERO_PATTERN_UCHAR, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);

	// Initialise accumulation buffer to zero, with no_data_val along edges
	setFillFloatEdgeKernelArgs(fill_edge_kernel, accumulation_buffer, ONE_PATTERN_FLOAT, source);
	errors[1] = clEnqueueNDRangeKernel(queue, fill_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Set direction kernel arguments and run it
	setDirectionKernelArgs(direction_kernel, elevation_buffer, direction_buffer, source);
	errors[2] = clEnqueueNDRangeKernel(queue, direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Set dependency kernel arguments and run it
	setDependencyKernelArgs(dependency_kernel, direction_buffer, new_dependency_buffer, source);
	errors[3] = clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_STOP]);

	checkForCLErrors(errors, 4, source + STR_SETUP);

	// Write out intermediate steps?
	if (printDirections) {
		writeUnsignedCharGPUBufferToFile(direction_buffer, STR_D8_TRANSFER_DIR_OUT);
	}

	if (printDependencies) {
		writeUnsignedCharGPUBufferToFile(new_dependency_buffer, STR_D8_TRANSFER_DEP_OUT);
	}

	// Calculate Flow
	setConstantD8KernelArgs(flow_transfer_kernel, accumulation_buffer, device_run_again, source);

	// Main loop
	int i = 0;
	cl_bool host_run_again = false;
	time_t start = time(0);

	errors[0] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 1, source + STR_SETUP);

	do {
		i++;

		// Swap the buffers
		cl_mem swap = old_dependency_buffer;
		old_dependency_buffer = new_dependency_buffer;
		new_dependency_buffer = swap;

		// Reset run variable
		cl_bool flow_false = false;
		errors[0] = clEnqueueWriteBuffer(queue, device_run_again, CL_FALSE, 0, sizeof(cl_bool), &flow_false, 0, NULL, NULL);

		// Run the kernel
		errors[1] = clSetKernelArg(flow_transfer_kernel, 0, sizeof(cl_mem), &old_dependency_buffer);
		errors[2] = clSetKernelArg(flow_transfer_kernel, 4, sizeof(cl_mem), &new_dependency_buffer);
		errors[3] = clEnqueueNDRangeKernel(queue, flow_transfer_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

		// Did anything change?
		errors[4] = clEnqueueReadBuffer(queue, device_run_again, CL_TRUE, 0, sizeof(cl_bool), &host_run_again, 0, NULL, NULL);

		checkForCLErrors(errors, 5, source + STR_LOOP);

	} while (host_run_again);

	errors[0] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[1] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 3, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "Flow Transfer D8: " << std::endl;
	std::cout << "Number of iterations: " << i << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(accumulation_buffer);
	errors[1] = clReleaseMemObject(device_run_again);
	errors[2] = clReleaseMemObject(direction_buffer);
	errors[3] = clReleaseMemObject(elevation_buffer);
	errors[4] = clReleaseMemObject(new_dependency_buffer);
	errors[5] = clReleaseMemObject(old_dependency_buffer);
	checkForCLErrors(errors, 6, source + STR_CLEANUP + " " + STR_BUFFERS);

	errors[0] = clReleaseKernel(dependency_kernel);
	errors[1] = clReleaseKernel(direction_kernel);
	errors[2] = clReleaseKernel(fill_kernel);
	errors[3] = clReleaseKernel(fill_edge_kernel);
	errors[4] = clReleaseKernel(flow_transfer_kernel);
	checkForCLErrors(errors, 5, source + STR_CLEANUP + " " + STR_KERNELS);
}

void flowTransferFD8(cl_float *elevation_data, cl_float *accumulation_data) {

	std::cout << std::endl << "Starting FD8..." << std::endl;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Set work group sizes
	size_t total_size = head->rows * head->columns;

	size_t global_work_size_1dim[1] = {computeTotalSizeWithPadding(total_size)};
	size_t local_work_size_1dim[1] = {max_global_size};

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];

	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	cl_int errors[8];
	std::string source = "FlowTransferFD8 ";

	// Create kernels
	cl_kernel dependency_kernel = clCreateKernel(program, STR_KERNEL_DEPEND, &errors[0]);
	cl_kernel direction_kernel = clCreateKernel(program, STR_KERNEL_FD8_DIRS, &errors[1]);
	cl_kernel fill_float_edge_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE, &errors[2]);
	cl_kernel fill_uchar_kernel = clCreateKernel(program, STR_KERNEL_FILL_UCHAR, &errors[3]);
	cl_kernel flow_transfer_kernel = clCreateKernel(program, STR_KERNEL_FD8_ACCU, &errors[4]);
	cl_kernel normalization_kernel = clCreateKernel(program, STR_KERNEL_FD8_NORMALIZATION, &errors[5]);
	checkForCLErrors(errors, 6, source + STR_KERNELS);

	// Create buffers
	size_t float_buffer_size = total_size * sizeof(cl_float);
	size_t uchar_buffer_size = total_size * sizeof(cl_uchar);
	cl_mem normalization_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[0]);
	cl_mem elevation_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, (void *)elevation_data, &errors[1]);
	cl_mem direction_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[2]);
	cl_mem old_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[3]);
	cl_mem new_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[4]);
	cl_mem accumulation_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[5]);
	cl_mem device_run_again = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_bool), NULL, &errors[6]);
	checkForCLErrors(errors, 7, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	// Initialise buffers
	setFillUcharKernelArgs(fill_uchar_kernel, old_dependency_buffer, (cl_uint) total_size, ZERO_PATTERN_UCHAR, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_uchar_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);

	setFillFloatEdgeKernelArgs(fill_float_edge_kernel, accumulation_buffer, ONE_PATTERN_FLOAT, source);
	errors[1] = clEnqueueNDRangeKernel(queue, fill_float_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Calculate directions
	setDirectionKernelArgs(direction_kernel, elevation_buffer, direction_buffer, source);
	errors[2] = clEnqueueNDRangeKernel(queue, direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Calculate dependencies
	setDependencyKernelArgs(dependency_kernel, direction_buffer, new_dependency_buffer, source);
	errors[3] = clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Calculate Normalization
	setFD8NormalizationKernelArgs(normalization_kernel, elevation_buffer, normalization_buffer, source);
	errors[4] = clEnqueueNDRangeKernel(queue, normalization_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_STOP]);
	checkForCLErrors(errors, 5, source + STR_SETUP);

	// Write out intermediate steps?
	if (printDirections) {
		writeUnsignedCharGPUBufferToFile(direction_buffer, STR_FD8_TRANSFER_DIR_OUT);
	}

	if (printDependencies) {
		writeUnsignedCharGPUBufferToFile(new_dependency_buffer, STR_FD8_TRANSFER_DEP_OUT);
	}

	if (printNormalization) {
		writeFloatGPUBufferToFile(normalization_buffer, STR_FD8_TRANSFER_NORM_OUT);
	}

	// Calculate Flow
	setConstantFD8KernelArgs(flow_transfer_kernel, elevation_buffer, normalization_buffer, accumulation_buffer, device_run_again, source);

	// Main loop
	int i = 0;
	cl_bool host_run_again = false;
	time_t start = time(0);
	errors[0] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 1, source + STR_SETUP);
	do {
		i++;

		// Swap the buffers
		cl_mem swap = old_dependency_buffer;
		old_dependency_buffer = new_dependency_buffer;
		new_dependency_buffer = swap;

		// Reset run variable
		cl_bool flow_false = false;
		errors[0] = clEnqueueWriteBuffer(queue, device_run_again, CL_FALSE, 0, sizeof(cl_bool), &flow_false, 0, NULL, NULL);

		// Run the kernel
		errors[1] = clSetKernelArg(flow_transfer_kernel, 0, sizeof(cl_mem), &old_dependency_buffer);
		errors[2] = clSetKernelArg(flow_transfer_kernel, 6, sizeof(cl_mem), &new_dependency_buffer);
		errors[3] = clEnqueueNDRangeKernel(queue, flow_transfer_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

		// Did anything change?
		errors[4] = clEnqueueReadBuffer(queue, device_run_again, CL_TRUE, 0, sizeof(cl_bool), &host_run_again, 0, NULL, NULL);

		checkForCLErrors(errors, 5, source + STR_LOOP);

	} while (host_run_again);

	errors[0] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[1] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 2, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "Flow Transfer FD8: " << std::endl;
	std::cout << "Number of iterations: " << i << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(accumulation_buffer);
	errors[1] = clReleaseMemObject(normalization_buffer);
	errors[2] = clReleaseMemObject(device_run_again);
	errors[3] = clReleaseMemObject(direction_buffer);
	errors[4] = clReleaseMemObject(elevation_buffer);
	errors[5] = clReleaseMemObject(new_dependency_buffer);
	errors[6] = clReleaseMemObject(old_dependency_buffer);
	checkForCLErrors(errors, 7, source + STR_CLEANUP + " " + STR_BUFFERS);

	errors[0] = clReleaseKernel(normalization_kernel);
	errors[1] = clReleaseKernel(dependency_kernel);
	errors[2] = clReleaseKernel(direction_kernel);
	errors[3] = clReleaseKernel(fill_float_edge_kernel);
	errors[4] = clReleaseKernel(fill_uchar_kernel);
	errors[5] = clReleaseKernel(flow_transfer_kernel);
	checkForCLErrors(errors, 6, source + STR_CLEANUP + " " + STR_KERNELS);
}

void flowTransferFDD8(cl_float *elevation_data, cl_float threshold, cl_float *accumulation_data) {

	std::cout << std::endl << "Starting FDD8..." << std::endl;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Set work group sizes
	size_t total_size = head->rows * head->columns;

	size_t global_work_size_1dim[1] = {computeTotalSizeWithPadding(total_size)};
	size_t local_work_size_1dim[1] = {max_global_size};

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];

	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	cl_int errors[9];
	std::string source = "FlowTransferFDD8 ";

	if (threshold < FDD8_THRESHOLD_MIN || threshold > FDD8_THRESHOLD_MAX) {
		reportError("FDD8 Threshold is outside valid range");
	}

	// Create kernels
	cl_kernel dependency_kernel = clCreateKernel(program, STR_KERNEL_DEPEND, &errors[0]);
	cl_kernel d8_direction_kernel = clCreateKernel(program, STR_KERNEL_D8_DIRS, &errors[1]);
	cl_kernel fd8_direction_kernel = clCreateKernel(program, STR_KERNEL_FD8_DIRS, &errors[2]);
	cl_kernel fill_float_edge_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE, &errors[3]);
	cl_kernel fill_uchar_kernel = clCreateKernel(program, STR_KERNEL_FILL_UCHAR, &errors[4]);
	cl_kernel flow_transfer_kernel = clCreateKernel(program, STR_KERNEL_FDD8_ACCU, &errors[5]);
	cl_kernel normalization_kernel = clCreateKernel(program, STR_KERNEL_FD8_NORMALIZATION, &errors[6]);
	checkForCLErrors(errors, 7, source + STR_KERNELS);

	// Create buffers
	size_t float_buffer_size = total_size * sizeof(cl_float);
	size_t uchar_buffer_size = total_size * sizeof(cl_uchar);

	cl_mem normalization_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[0]);
	cl_mem elevation_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, (void *)elevation_data, &errors[1]);
	cl_mem d8_direction_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[2]);
	cl_mem fd8_direction_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[3]);
	cl_mem d8_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[4]);
	cl_mem old_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[5]);
	cl_mem new_dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[6]);
	cl_mem accumulation_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[7]);
	cl_mem device_run_again = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_bool), NULL, &errors[8]);
	checkForCLErrors(errors, 9, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	// Initialise buffers
	setFillUcharKernelArgs(fill_uchar_kernel, old_dependency_buffer, (cl_uint) total_size, ZERO_PATTERN_UCHAR, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_uchar_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);

	setFillFloatEdgeKernelArgs(fill_float_edge_kernel, accumulation_buffer, ONE_PATTERN_FLOAT, source);
	errors[1] = clEnqueueNDRangeKernel(queue, fill_float_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Compute directions for D8 and FD8
	setDirectionKernelArgs(d8_direction_kernel, elevation_buffer, d8_direction_buffer, source);
	errors[2] = clEnqueueNDRangeKernel(queue, d8_direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	setDirectionKernelArgs(fd8_direction_kernel, elevation_buffer, fd8_direction_buffer, source);
	errors[3] = clEnqueueNDRangeKernel(queue, fd8_direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Compute dependencies
	setDependencyKernelArgs(dependency_kernel, d8_direction_buffer, d8_dependency_buffer, source);
	errors[4]= clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	setDependencyKernelArgs(dependency_kernel, fd8_direction_buffer, new_dependency_buffer, source);
	errors[5] = clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Compute Normalization
	setFD8NormalizationKernelArgs(normalization_kernel, elevation_buffer, normalization_buffer, source);
	errors[6] = clEnqueueNDRangeKernel(queue, normalization_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_STOP]);
	checkForCLErrors(errors, 7, source + STR_SETUP);

	// Write out intermediate steps?
	if (printDirections) {
		writeUnsignedCharGPUBufferToFile(d8_direction_buffer, STR_FDD8_D8_TRANSFER_DIR_OUT);
		writeUnsignedCharGPUBufferToFile(fd8_direction_buffer, STR_FDD8_FD8_TRANSFER_DIR_OUT);
	}

	if (printDependencies) {
		writeUnsignedCharGPUBufferToFile(d8_dependency_buffer, STR_FDD8_D8_TRANSFER_DEP_OUT);
		writeUnsignedCharGPUBufferToFile(new_dependency_buffer, STR_FDD8_FD8_TRANSFER_DEP_OUT);
	}

	if (printNormalization) {
		writeFloatGPUBufferToFile(normalization_buffer, STR_FDD8_TRANSFER_NORM_OUT);
	}

	// Calculate Flow
	setConstantFDD8KernelArgs(flow_transfer_kernel, elevation_buffer, threshold, d8_dependency_buffer, accumulation_buffer, normalization_buffer, device_run_again, source);

	// Main loop
	int i = 0;
	cl_bool host_run_again = false;
	time_t start = time(0);
	errors[0] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 1, source + STR_SETUP);
	do {
		i++;

		// Swap the buffers
		cl_mem swap = old_dependency_buffer;
		old_dependency_buffer = new_dependency_buffer;
		new_dependency_buffer = swap;

		// Reset run variable
		cl_bool flow_false = false;
		errors[0] = clEnqueueWriteBuffer(queue, device_run_again, CL_FALSE, 0, sizeof(cl_bool), &flow_false, 0, NULL, NULL);

		// Run the kernel
		errors[3] = clSetKernelArg(flow_transfer_kernel, 0, sizeof(cl_mem), &old_dependency_buffer);
		errors[4] = clSetKernelArg(flow_transfer_kernel, 8, sizeof(cl_mem), &new_dependency_buffer);
		errors[5] = clEnqueueNDRangeKernel(queue, flow_transfer_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

		// Did anything change?
		errors[6] = clEnqueueReadBuffer(queue, device_run_again, CL_TRUE, 0, sizeof(cl_bool), &host_run_again, 0, NULL, NULL);
		checkForCLErrors(errors, 7, source + STR_LOOP);

	} while (host_run_again);

	errors[0] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[1] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 2, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "Flow Transfer FDD8: " << std::endl;
	std::cout << "Number of iterations: " << i << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(accumulation_buffer);
	errors[1] = clReleaseMemObject(normalization_buffer);
	errors[2] = clReleaseMemObject(d8_dependency_buffer);
	errors[3] = clReleaseMemObject(d8_direction_buffer);
	errors[4] = clReleaseMemObject(device_run_again);
	errors[5] = clReleaseMemObject(fd8_direction_buffer);
	errors[6] = clReleaseMemObject(elevation_buffer);
	errors[7] = clReleaseMemObject(new_dependency_buffer);
	errors[8] = clReleaseMemObject(old_dependency_buffer);
	checkForCLErrors(errors, 9, source + STR_CLEANUP + " " + STR_BUFFERS);

	errors[0] = clReleaseKernel(normalization_kernel);
	errors[1] = clReleaseKernel(dependency_kernel);
	errors[2] = clReleaseKernel(d8_direction_kernel);
	errors[3] = clReleaseKernel(fd8_direction_kernel);
	errors[4] = clReleaseKernel(fill_float_edge_kernel);
	errors[5] = clReleaseKernel(fill_uchar_kernel);
	errors[6] = clReleaseKernel(flow_transfer_kernel);
	checkForCLErrors(errors, 7, source + STR_CLEANUP + " " + STR_KERNELS);
}

void ortegaRueda(cl_float *elevation_data, cl_float *accumulation_data) {
	std::cout << std::endl << "Starting Ortega Rueda Flow Transfer..." << std::endl;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Set work group sizes
	size_t total_size = head->rows * head->columns;

	size_t global_work_size_1dim[1] = {computeTotalSizeWithPadding(total_size)};
	size_t local_work_size_1dim[1] = {max_global_size};

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];

	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	cl_int errors[7];
	std::string source ("Ortega Rueda ");

	// Create kernels
	cl_kernel direction_kernel = clCreateKernel(program, STR_KERNEL_D8_DIRS, &errors[0]);
	cl_kernel dependency_kernel = clCreateKernel(program, STR_KERNEL_DEPEND, &errors[1]);
	cl_kernel flow_transfer_kernel = clCreateKernel(program, STR_KERNEL_ORTEGA, &errors[2]);
	cl_kernel fill_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT, &errors[3]);
	cl_kernel fill_edge_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE, &errors[4]);
	checkForCLErrors(errors, 5, source + STR_KERNELS);

	// Create memory objects
	size_t float_buffer_size = total_size * sizeof(cl_float);
	size_t uchar_buffer_size = total_size * sizeof(cl_uchar);
	cl_mem elevation_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, (void *)elevation_data, &errors[0]);
	cl_mem direction_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[1]);
	cl_mem dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[2]);
	cl_mem old_flow_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[3]);
	cl_mem new_flow_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[4]);
	cl_mem accumulation_buffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, float_buffer_size, NULL, &errors[5]);
	cl_mem device_run_again = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_bool), NULL, &errors[6]);
	checkForCLErrors(errors, 7, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	// Initialise new flow buffer to 1
	setFillFloatKernelArgs(fill_kernel, new_flow_buffer, (cl_uint) total_size, ONE_PATTERN_FLOAT, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);

	// Initialise old flow buffer to 0
	setFillFloatKernelArgs(fill_kernel, old_flow_buffer, (cl_uint) total_size, ZERO_PATTERN_FLOAT, source);
	errors[1] = clEnqueueNDRangeKernel(queue, fill_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, NULL);

	// Initialise accumulation buffer to zero, with no_data_val along edges
	setFillFloatEdgeKernelArgs(fill_edge_kernel, accumulation_buffer, ONE_PATTERN_FLOAT, source);
	errors[2] = clEnqueueNDRangeKernel(queue, fill_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Set direction kernel arguments and run it
	setDirectionKernelArgs(direction_kernel, elevation_buffer, direction_buffer, source);
	errors[3] = clEnqueueNDRangeKernel(queue, direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Set dependency kernel arguments and run it
	setDependencyKernelArgs(dependency_kernel, direction_buffer, dependency_buffer, source);
	errors[4] = clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_STOP]);
	checkForCLErrors(errors, 5, source + STR_SETUP);

	// Write out intermediate steps?
	if (printDirections) {
		writeUnsignedCharGPUBufferToFile(direction_buffer, STR_ORTEGA_DIR_OUT);
	}

	if (printDependencies) {
		writeUnsignedCharGPUBufferToFile(dependency_buffer, STR_ORTEGA_DEP_OUT);
	}

	// Calculate Flow
	setConstantOrtegaRuedaKernelArgs(flow_transfer_kernel, dependency_buffer, accumulation_buffer, device_run_again, source);

	// Main Loop
	cl_bool host_run_again = false;
	int i = 0;
	time_t start = time(0);
	errors[0] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 1, source + STR_SETUP);
	do {
		i++;

		// Swap the buffers
		cl_mem swap = old_flow_buffer;
		old_flow_buffer = new_flow_buffer;
		new_flow_buffer = swap;

		// Reset run variable
		cl_bool flow_false = false;
		errors[0] = clEnqueueWriteBuffer(queue, device_run_again, CL_FALSE, 0, sizeof(cl_bool), &flow_false, 0, NULL, NULL);

		// Run the kernel
		errors[1] = clSetKernelArg(flow_transfer_kernel, 0, sizeof(cl_mem), &old_flow_buffer);
		errors[2] = clSetKernelArg(flow_transfer_kernel, 5, sizeof(cl_mem), &new_flow_buffer);
		errors[3] = clEnqueueNDRangeKernel(queue, flow_transfer_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

		// Did anything change?
		errors[4] = clEnqueueReadBuffer(queue, device_run_again, CL_TRUE, 0, sizeof(cl_bool), &host_run_again, 0, NULL, NULL);
		checkForCLErrors(errors, 5, source + STR_LOOP);

	} while (host_run_again);

	errors[0] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[1] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 2, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "Ortega: " << std::endl;
	std::cout << "Number of iterations: " << i << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(accumulation_buffer);
	errors[1] = clReleaseMemObject(dependency_buffer);
	errors[2] = clReleaseMemObject(device_run_again);
	errors[3] = clReleaseMemObject(direction_buffer);
	errors[4] = clReleaseMemObject(elevation_buffer);
	errors[5] = clReleaseMemObject(new_flow_buffer);
	errors[6] = clReleaseMemObject(old_flow_buffer);
	checkForCLErrors(errors, 7, source + STR_CLEANUP + " " + STR_BUFFERS);

	errors[0] = clReleaseKernel(dependency_kernel);
	errors[1] = clReleaseKernel(direction_kernel);
	errors[2] = clReleaseKernel(fill_kernel);
	errors[3] = clReleaseKernel(fill_edge_kernel);
	errors[4] = clReleaseKernel(flow_transfer_kernel);
	checkForCLErrors(errors, 5, source + STR_CLEANUP + " " + STR_KERNELS);
}

void zhanQin(cl_float *elevation_data, cl_float *accumulation_data) {

	std::cout << std::endl << "Starting Zhan Qin Flow Transfer..." << std::endl;

	if (max_local_size == 0 || max_global_size == 0) {
		std::cerr << "ERROR: Max Size set to 0" << std::endl;
		exit(EXIT_FAILURE);
	}

	// Set work group sizes
	size_t total_size = head->rows * head->columns;

	size_t global_work_size_1dim[1] = {computeTotalSizeWithPadding(total_size)};
	size_t local_work_size_1dim[1] = {max_global_size};

	size_t global_work_size_2dim[2];
	size_t local_work_size_2dim[2];

	size_t global_size[2] = {head->rows, head->columns};
	compute2DimKernelSizes(global_size, global_work_size_2dim, local_work_size_2dim);

	cl_int errors[7];
	std::string source ("Zhan Qin ");

	// Create kernels
cl_kernel normalization_kernel = clCreateKernel(program, STR_KERNEL_FD8_NORMALIZATION, &errors[0]);
	cl_kernel direction_kernel = clCreateKernel(program, STR_KERNEL_FD8_DIRS, &errors[1]);
	cl_kernel dependency_kernel = clCreateKernel(program, STR_KERNEL_DEPEND, &errors[2]);
	cl_kernel indegree_kernel = clCreateKernel(program, STR_KERNEL_INDEGREE, &errors[3]);
	cl_kernel flow_kernel = clCreateKernel(program, STR_KERNEL_ZHANQIN, &errors[4]);
	cl_kernel fill_edge_kernel = clCreateKernel(program, STR_KERNEL_FILL_FLOAT_EDGE, &errors[5]);
	checkForCLErrors(errors, 6, source + STR_KERNELS);

	// Create memory objects
	size_t float_buffer_size = total_size * sizeof(cl_float);
	size_t integer_buffer_size = float_buffer_size;
	size_t uchar_buffer_size = total_size * sizeof(cl_uchar);
	cl_mem elevation_buffer = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, float_buffer_size, (void *)elevation_data, &errors[0]);
	cl_mem direction_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[1]);
	cl_mem dependency_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, uchar_buffer_size, NULL, &errors[2]);
	cl_mem normalization_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, float_buffer_size, NULL, &errors[3]);
	cl_mem indegree_buffer = clCreateBuffer(context, CL_MEM_READ_WRITE, integer_buffer_size, NULL, &errors[4]);
	cl_mem accumulation_buffer = clCreateBuffer(context, CL_MEM_WRITE_ONLY, float_buffer_size, NULL, &errors[5]);
	cl_mem device_run_again = clCreateBuffer(context, CL_MEM_WRITE_ONLY, sizeof(cl_bool), NULL, &errors[6]);
	checkForCLErrors(errors, 7, source + STR_BUFFERS);

	cl_event profiling_events[PROFILING_EVENTS];

	// Initialise buffers
	setFillFloatEdgeKernelArgs(fill_edge_kernel, accumulation_buffer, ONE_PATTERN_FLOAT, source);
	errors[0] = clEnqueueNDRangeKernel(queue, fill_edge_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_START]);

	// Compute Directions
	setDirectionKernelArgs(direction_kernel, elevation_buffer, direction_buffer, source);
	errors[1] = clEnqueueNDRangeKernel(queue, direction_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Compute Dependencies
	setDependencyKernelArgs(dependency_kernel, direction_buffer, dependency_buffer, source);
	errors[2] = clEnqueueNDRangeKernel(queue, dependency_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

	// Compute Indegree Matrix
	setIndegreeKernelArgs(indegree_kernel, dependency_buffer, indegree_buffer, source);
	errors[3] = clEnqueueNDRangeKernel(queue, indegree_kernel, 1, NULL, global_work_size_1dim, local_work_size_1dim, 0, NULL, NULL);

	// Compute Normalization
	setFD8NormalizationKernelArgs(normalization_kernel, elevation_buffer, normalization_buffer, source);
	errors[4] = clEnqueueNDRangeKernel(queue, normalization_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, &profiling_events[PROFILING_SETUP_STOP]);
	checkForCLErrors(errors, 5, source + STR_SETUP);

	// Write out intermediate steps?
	if (printDirections) {
		writeUnsignedCharGPUBufferToFile(direction_buffer, STR_ZHAN_DIR_OUT);
	}

	if (printDependencies) {
		writeUnsignedCharGPUBufferToFile(dependency_buffer, STR_ZHAN_DEP_OUT);
	}

	if (printIndegree) {
		writeIntegerGPUBufferToFile(indegree_buffer, STR_ZHAN_INDEGREE_OUT);
	}

	if (printNormalization) {
		writeFloatGPUBufferToFile(normalization_buffer, STR_ZHAN_NORM_OUT);
	}

	// Set Constant ZhanQin kernel arguments
	setConstantZhanQinKernelArgs(flow_kernel, elevation_buffer, direction_buffer, dependency_buffer, normalization_buffer, indegree_buffer, accumulation_buffer, device_run_again, source);

	// Main Loop
	cl_bool host_run_again = false;
	int i = 0;
	time_t start = time(0);
	errors[0] = clEnqueueMarker(queue, &profiling_events[PROFILING_EXECUTION_START]);
	checkForCLErrors(errors, 1, source + STR_SETUP);
	do {
		i++;

		// Reset run variable
		cl_bool flow_false = false;
		errors[0] = clEnqueueWriteBuffer(queue, device_run_again, CL_FALSE, 0, sizeof(cl_bool), &flow_false, 0, NULL, NULL);

		// Run the kernel
		errors[1] = clEnqueueNDRangeKernel(queue, flow_kernel, 2, NULL, global_work_size_2dim, local_work_size_2dim, 0, NULL, NULL);

		// Did anything change?
		errors[2] = clEnqueueReadBuffer(queue, device_run_again, CL_TRUE, 0, sizeof(cl_bool), &host_run_again, 0, NULL, NULL);
		checkForCLErrors(errors, 3, source + STR_LOOP);

	} while (host_run_again);

	errors[0] = clEnqueueReadBuffer(queue, accumulation_buffer, CL_TRUE, 0, float_buffer_size, accumulation_data, 0, NULL, &profiling_events[PROFILING_EXECUTION_STOP]);
	time_t stop = time(0);
	double time_taken = difftime(stop, start);
	errors[1] = clWaitForEvents(PROFILING_EVENTS, profiling_events);
	checkForCLErrors(errors, 2, source + STR_RESULTS);

	// Output
	std::cout << std::endl;
	std::cout << "Flow Transfer Zhan Qin: " << std::endl;
	std::cout << "Number of iterations: " << i << std::endl;
	std::cout << "Time taken in seconds: " << time_taken << std::endl;
	if (performProfiling) {
		calculateProfilingTimes(profiling_events, source);
	}
	std::cout << std::endl;

	// Cleanup
	errors[0] = clReleaseMemObject(accumulation_buffer);
	errors[1] = clReleaseMemObject(normalization_buffer);
	errors[2] = clReleaseMemObject(indegree_buffer);
	errors[3] = clReleaseMemObject(direction_buffer);
	errors[4] = clReleaseMemObject(device_run_again);
	errors[5] = clReleaseMemObject(dependency_buffer);
	errors[6] = clReleaseMemObject(elevation_buffer);
	checkForCLErrors(errors, 7, source + STR_CLEANUP + " " + STR_BUFFERS);

	errors[0] = clReleaseKernel(normalization_kernel);
	errors[1] = clReleaseKernel(dependency_kernel);
	errors[2] = clReleaseKernel(direction_kernel);
	errors[3] = clReleaseKernel(fill_edge_kernel);
	errors[4] = clReleaseKernel(indegree_kernel);
	errors[5] = clReleaseKernel(flow_kernel);
	checkForCLErrors(errors, 6, source + STR_CLEANUP + " " + STR_KERNELS);

}

void setConstantAnisotropicKernelArgs(cl_kernel flow_transfer_kernel, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_float M, cl_float Q_0, cl_mem vector_buffer, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source) {

	std::string out (source);
	out.append("AnisotropicTransfer Arg ");

	cl_int errors[10] = {};
	errors[0] = clSetKernelArg(flow_transfer_kernel, 1, sizeof(cl_mem), &elevation_buffer);
	errors[1] = clSetKernelArg(flow_transfer_kernel, 2, sizeof(cl_mem), &normalization_buffer);
	errors[2] = clSetKernelArg(flow_transfer_kernel, 3, sizeof(cl_uint), &head->rows);
	errors[3] = clSetKernelArg(flow_transfer_kernel, 4, sizeof(cl_uint), &head->columns);
	errors[4] = clSetKernelArg(flow_transfer_kernel, 5, sizeof(cl_float), &head->resolution);
	errors[5] = clSetKernelArg(flow_transfer_kernel, 6, sizeof(cl_float), &M);
	errors[6] = clSetKernelArg(flow_transfer_kernel, 7, sizeof(cl_float), &Q_0);
	errors[7] = clSetKernelArg(flow_transfer_kernel, 8, sizeof(cl_mem), &vector_buffer);
	errors[8] = clSetKernelArg(flow_transfer_kernel, 9, sizeof(cl_mem), &accumulation_buffer);
	errors[9] = clSetKernelArg(flow_transfer_kernel, 11, sizeof(cl_mem), &device_run_again);
	checkForCLErrors(errors, 10, out);
}

void setConstantD8KernelArgs(cl_kernel flow_transfer_kernel, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source) {

	std::string out (source);
	out.append("D8Transfer Arg ");

	cl_int errors[4] = {};
	errors[0] = clSetKernelArg(flow_transfer_kernel, 1, sizeof(cl_uint), &head->rows);
	errors[1] = clSetKernelArg(flow_transfer_kernel, 2, sizeof(cl_uint), &head->columns);
	errors[2] = clSetKernelArg(flow_transfer_kernel, 3, sizeof(cl_mem), &accumulation_buffer);
	errors[3] = clSetKernelArg(flow_transfer_kernel, 5, sizeof(cl_mem), &device_run_again);
	checkForCLErrors(errors, 4, out);
}

void setConstantFD8KernelArgs(cl_kernel flow_transfer_kernel, cl_mem elevation_buffer, cl_mem normalization_buffer, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source) {
	// Arguments 0 and 6 are not constant and should be set by caller

	std::string out (source);
	out.append("FD8Transfer Arg ");

	cl_int errors[6] = {};
	errors[0] = clSetKernelArg(flow_transfer_kernel, 1, sizeof(cl_mem), &elevation_buffer);
	errors[1] = clSetKernelArg(flow_transfer_kernel, 2, sizeof(cl_uint), &head->rows);
	errors[2] = clSetKernelArg(flow_transfer_kernel, 3, sizeof(cl_uint), &head->columns);
	errors[3] = clSetKernelArg(flow_transfer_kernel, 4, sizeof(cl_mem), &normalization_buffer);
	errors[4] = clSetKernelArg(flow_transfer_kernel, 5, sizeof(cl_mem), &accumulation_buffer);
	errors[5] = clSetKernelArg(flow_transfer_kernel, 7, sizeof(cl_mem), &device_run_again);
	checkForCLErrors(errors, 6, out);
}

void setConstantFDD8KernelArgs(cl_kernel flow_transfer_kernel, cl_mem elevation_buffer, cl_float threshold, cl_mem d8_dependency_buffer, cl_mem accumulation_buffer, cl_mem normalization_buffer, cl_mem device_run_again, std::string source) {
	// Arguments 0 and 8 are not constant and should be set by caller

	std::string out (source);
	out.append("FD8Transfer Arg ");

	cl_int errors[8] = {};
	errors[0] = clSetKernelArg(flow_transfer_kernel, 1, sizeof(cl_mem), &elevation_buffer);
	errors[1] = clSetKernelArg(flow_transfer_kernel, 2, sizeof(cl_uint), &head->rows);
	errors[2] = clSetKernelArg(flow_transfer_kernel, 3, sizeof(cl_uint), &head->columns);
	errors[3] = clSetKernelArg(flow_transfer_kernel, 4, sizeof(cl_float), &threshold);
	errors[4] = clSetKernelArg(flow_transfer_kernel, 5, sizeof(cl_mem), &d8_dependency_buffer);
	errors[5] = clSetKernelArg(flow_transfer_kernel, 6, sizeof(cl_mem), &accumulation_buffer);
	errors[6] = clSetKernelArg(flow_transfer_kernel, 7, sizeof(cl_mem), &normalization_buffer);
	errors[7] = clSetKernelArg(flow_transfer_kernel, 9, sizeof(cl_mem), &device_run_again);
	checkForCLErrors(errors, 8, out);
}

void setConstantOrtegaRuedaKernelArgs(cl_kernel flow_transfer_kernel, cl_mem dependency_buffer, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source) {

	std::string out (source);
	out.append("Ortega Rueda Arg ");

	cl_int errors[5] = {};
	errors[0] = clSetKernelArg(flow_transfer_kernel, 1, sizeof(cl_uint), &head->rows);
	errors[1] = clSetKernelArg(flow_transfer_kernel, 2, sizeof(cl_uint), &head->columns);
	errors[2] = clSetKernelArg(flow_transfer_kernel, 3, sizeof(cl_mem), &dependency_buffer);
	errors[3] = clSetKernelArg(flow_transfer_kernel, 4, sizeof(cl_mem), &accumulation_buffer);
	errors[4] = clSetKernelArg(flow_transfer_kernel, 6, sizeof(cl_mem), &device_run_again);
	checkForCLErrors(errors, 5, out);
}

void setConstantZhanQinKernelArgs(cl_kernel zhan_qin_kernel, cl_mem elevation_buffer, cl_mem direction_buffer, cl_mem dependency_buffer, cl_mem normalization_buffer, cl_mem indegree_buffer, cl_mem accumulation_buffer, cl_mem device_run_again, std::string source) {

	std::string out (source);
	out.append("Zhan Qin Arg ");

	cl_int errors[9] = {};
	errors[0] = clSetKernelArg(zhan_qin_kernel, 0, sizeof(cl_mem), &elevation_buffer);
	errors[1] = clSetKernelArg(zhan_qin_kernel, 1, sizeof(cl_mem), &direction_buffer);
	errors[2] = clSetKernelArg(zhan_qin_kernel, 2, sizeof(cl_mem), &dependency_buffer);
	errors[3] = clSetKernelArg(zhan_qin_kernel, 3, sizeof(cl_mem), &normalization_buffer);
	errors[4] = clSetKernelArg(zhan_qin_kernel, 4, sizeof(cl_mem), &indegree_buffer);
	errors[5] = clSetKernelArg(zhan_qin_kernel, 5, sizeof(cl_uint), &head->rows);
	errors[6] = clSetKernelArg(zhan_qin_kernel, 6, sizeof(cl_uint), &head->columns);
	errors[7] = clSetKernelArg(zhan_qin_kernel, 7, sizeof(cl_mem), &accumulation_buffer);
	errors[8] = clSetKernelArg(zhan_qin_kernel, 8, sizeof(cl_mem), &device_run_again);
	checkForCLErrors(errors, 9, out);
}

void setIndegreeKernelArgs(cl_kernel indegree_kernel, cl_mem dependency_buffer, cl_mem indegree_buffer, std::string source) {

	std::string out (source);
	out.append("Indegree Arg ");

	cl_int errors[3] = {};
	errors[0] = clSetKernelArg(indegree_kernel, 0, sizeof(cl_mem), &dependency_buffer);
	cl_uint total_cells = head->rows * head->columns;
	errors[1] = clSetKernelArg(indegree_kernel, 1, sizeof(cl_uint), &total_cells);
	errors[2] = clSetKernelArg(indegree_kernel, 2, sizeof(cl_mem), &indegree_buffer);
	checkForCLErrors(errors, 3, out);
}

void writeIntegerGPUBufferToFile(cl_mem buffer, std::string output_filename) {
	cl_uint total_size = head->rows * head->columns;
	cl_int *cpu_buffer = new cl_int[total_size];
	cl_int errors[1] = {};
	errors[0] = clEnqueueReadBuffer(queue, buffer, CL_TRUE, 0, total_size * sizeof(cl_int), cpu_buffer, 0, NULL, NULL);
	std::string message (STR_ERROR_WRITING + output_filename);
	checkForCLErrors(errors, 1, message);
	writeIntegerGridCheck(output_filename, cpu_buffer);
	delete [] cpu_buffer;
}

void writeFloatGPUBufferToFile(cl_mem buffer, std::string output_filename) {
	cl_uint total_size = head->rows * head->columns;
	cl_float *cpu_buffer = new cl_float[total_size];
	cl_int errors[1] = {};
	errors[0] = clEnqueueReadBuffer(queue, buffer, CL_TRUE, 0, total_size * sizeof(cl_float), cpu_buffer, 0, NULL, NULL);
	std::string message (STR_ERROR_WRITING + output_filename);
	checkForCLErrors(errors, 1, message);
	writeDEMToFile(output_filename, cpu_buffer);
	delete [] cpu_buffer;
}

void writeUnsignedCharGPUBufferToFile(cl_mem buffer, std::string output_filename) {
	cl_uint total_size = head->rows * head->columns;
	cl_uchar *cpu_buffer = new cl_uchar[total_size];
	cl_int errors[1] = {};
	errors[0] = clEnqueueReadBuffer(queue, buffer, CL_TRUE, 0, total_size * sizeof(cl_uchar), cpu_buffer, 0, NULL, NULL);
	std::string message (STR_ERROR_WRITING + output_filename);
	checkForCLErrors(errors, 1, message);
	writeUnsignedGridCheck(output_filename, cpu_buffer);
	delete [] cpu_buffer;
}
